<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class No_generator extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function digit_count($length, $value)
	{
		while (strlen($value) < $length)
			$value = '0' . $value;
		return $value;
	}

	// public function generateNoAntrian($frontID)
	// {
	// 	$no_antrian = $frontID;
	// 	// echo $no_antrian;
	// 	// die;
	// 	$date = date('Y-m-d');
	// 	$data = Modules::run('database/get', array(
	// 		'table' => 'antrian',
	// 		'like' => array(
	// 			array('no_antrian', $no_antrian),
	// 			array('createddate', $date),
	// 		),
	// 		'orderby' => 'id desc'
	// 	));

	// 	$seq = 1;
	// 	if (!empty($data)) {
	// 		$data = $data->row_array();
	// 		$seq = str_replace($no_antrian, '', $data['no_antrian']);
	// 		$seq = intval($seq) + 1;
	// 	}

	// 	$seq = $this->digit_count(3, $seq);
	// 	$no_antrian .= $seq;
	// 	return $no_antrian;
	// }
	public function generateNoAntrian()
	{
		$no_antrian = "";
		// echo $no_antrian;
		// die;
		$date = date('Y-m-d');
		$data = Modules::run('database/get', array(
			'table' => 't_antri',
			'like' => array(
				array('no_antri', $no_antrian),
				array('tgl', $date),
			),
			'orderby' => 'no_antri desc'
		));

		$seq = 1;
		if (!empty($data)) {
			$data = $data->row_array();
			$seq = str_replace($no_antrian, '', $data['no_antri']);
			$seq = intval($seq) + 1;
		}

		$seq = $this->digit_count(3, $seq);
		$no_antrian .= $seq;
		return $no_antrian;
	}
}

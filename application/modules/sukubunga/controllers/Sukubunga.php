<?php

class Sukubunga extends MX_Controller {

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
		$this->load->model('storage/t_simpanan');
		$this->load->model('storage/t_ket_sukubunga');
	}

	public function getModuleName()
	{
		return 'sukubunga';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/sukubunga.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 't_simpanan';
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Suku Bunga";
		$data['title_content'] = 'Data Suku Bunga';
		$content = $this->getData();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);

		// echo '<pre>';
		// print_r($data['pagination']);die;
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		echo Modules::run('template', $data);
	}

	public function getTotalData($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('u.jenis', $keyword),
				array('u.nama', $keyword),
				array('u.bunga', $keyword),
				array('u.ket', $keyword),
				array('tm.no_amandemen', $keyword),
			);
		}
		$total = Modules::run('database/count_all', array(
			'table' => $this->getTableName() . ' u',
			'field' => array('u.*', 'tm.no_amandemen'),
			'join' => array(
				array('t_amandemen tm', 'tm.id = u.t_amandemen', 'left')
			),
			'like' => $like,
			'is_or_like' => true,
			'where' => 'u.deleted = 0'
		));

		return $total;
	}

	public function getData($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('u.jenis', $keyword),
				array('u.nama', $keyword),
				array('u.bunga', $keyword),
				array('u.ket', $keyword),
				array('tm.no_amandemen', $keyword),
			);
		}
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' u',
			'field' => array('u.*', 'tm.no_amandemen'),
			'join' => array(
				array('t_amandemen tm', 'tm.id = u.t_amandemen', 'left')
			),
			'like' => $like,
			'is_or_like' => true,
			'limit' => $this->limit,
			'offset' => $this->last_no,
			'where' => 'u.deleted = 0'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalData($keyword)
		);
	}

	public function getDetailData($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' u',
			'where' => "u.id = '" . $id . "'"
		));

		return $data->row_array();
	}

	public function getListPegawai()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pegawai p',
			'field' => array('p.*', 'u.hak_akses as hak_akses'),
			'join' => array(
				array('user u', 'p.id = u.pegawai', 'left')
			)
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				if ($value['hak_akses'] == '') {
					array_push($result, $value);
				}
			}
		}


		return $result;
	}

	public function getListHakAkses()
	{
		$data = Modules::run('database/get', array(
			'table' => 'hak_akses'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Suku Bunga";
		$data['title_content'] = 'Tambah Jenis Suku Bunga';
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = T_simpanan::findOrFail($id)->toArray();
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Suku Bunga";
		$data['title_content'] = 'Ubah Suku Bunga';
		$data['list_jenis'] = T_ket_sukubunga::where('deleted', '=', 0)->get()->toArray();
		// echo '<pre>';
		// print_r($data);die;
		echo Modules::run('template', $data);
	}

	public function detail($id)
	{
		$data = T_simpanan::findOrFail($id)->toArray();
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Suku Bunga";
		$data['title_content'] = 'Detail Suku Bunga';
		echo Modules::run('template', $data);
	}

	public function getPostDataHeader($value)
	{
		$data['jenis'] = $value->jenis;
		$data['ket'] = $value->jenis_suku;
		$data['nama'] = $value->nama;
		$data['bunga'] = $value->bunga;
		return $data;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));
		$id = $this->input->post('id');
		
		$is_valid = false;
		$this->db->trans_begin();
		try {
			$push = array();
			$push['no_amandemen'] = $data->no_ubah;
			$t_amandemen = Modules::run('database/_insert', 't_amandemen', $push);
			
			$post_simpanan = $this->getPostDataHeader($data);
			// echo '<pre>';
			// print_r($post_user);
			// die;
			$post_simpanan['t_amandemen'] = $t_amandemen;
			if ($id == '') {
				$user = Modules::run('database/_insert', $this->getTableName(), $post_simpanan);
			} else {
				//update
				// Modules::run('database/_update', $this->getTableName(), $post_simpanan, array('id' => $id));
				Modules::run('database/_update', $this->getTableName(), $post_simpanan, array('ket' => $data->jenis_suku));
			}
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Pertanyaan";
		$data['title_content'] = 'Data Pertanyaan';
		$content = $this->getData($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function setUserAktifNonAktif($id, $is_active)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', 'user', array('is_active' => $is_active), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', $this->getTableName(), array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}
}

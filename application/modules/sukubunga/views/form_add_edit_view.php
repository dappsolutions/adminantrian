<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Suku Bunga</u>
					</div>
				</div>
				<hr />

				<div class="row">
					<div class='col-md-3'>
						Jenis
					</div>
					<div class='col-md-3'>
						<input id="jenis" class="form-control required" error="Jenis" value="<?php echo isset($jenis) ? $jenis : '' ?>"/>
					</div>
				</div>
				<br />
			
				<div class="row">
					<div class='col-md-3'>
						Nama
					</div>
					<div class='col-md-3'>
						<input id="nama" class="form-control required" error="Nama" value="<?php echo isset($nama) ? $nama : '' ?>"/>
					</div>
				</div>
				<br />
				
				<div class="row">
					<div class='col-md-3'>
						Bunga (%)
					</div>
					<div class='col-md-3'>
						<input id="bunga" class="form-control required" error="Bunga" value="<?php echo isset($bunga) ? $bunga : '' ?>"/>
					</div>
				</div>
				<br />
			
				<div class="row">
					<div class='col-md-3'>
						Jenis Suku Bunga
					</div>
					<div class='col-md-3'>
						<select name="" class="form-control" id="jenis-suku">
							<?php foreach ($list_jenis as $key => $value) {?>
								<option value="<?php echo $value['ket'] ?>" <?php echo $value['ket'] == $ket ? 'selected' : '' ?>><?php echo $value['ket'] ?></option>
							<?php }?>
						</select>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3'>
						No Perubahan
					</div>
					<div class='col-md-3'>
						<input id="no_ubah" class="form-control required" error="No Perubahan" value="<?php echo isset($no_amandemen) ? $no_amandemen : '' ?>"/>
					</div>
				</div>
				<br />
				<hr />

				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-success" onclick="Sukubunga.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
						&nbsp;
						<button id="" class="btn btn-baru" onclick="Sukubunga.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

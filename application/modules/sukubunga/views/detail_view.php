<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Jenis Suku Bunga</u>
					</div>
				</div>
				<hr />

				<div class="row">
					<div class='col-md-3'>
						Jenis 
					</div>
					<div class='col-md-3'>
						<b><?php echo $jenis ?></b>
					</div>
				</div>
				<br />
				
				<div class="row">
					<div class='col-md-3'>
						Nama
					</div>
					<div class='col-md-3'>
						<b><?php echo $nama ?></b>
					</div>
				</div>
				<br />
			
				<div class="row">
					<div class='col-md-3'>
						Bunga
					</div>
					<div class='col-md-3'>
						<b><?php echo $bunga ?></b>
					</div>
				</div>
				<br />
				
				<div class="row">
					<div class='col-md-3'>
						Jenis Suku Bunga
					</div>
					<div class='col-md-3'>
						<b><?php echo $ket ?></b>
					</div>
				</div>
				<br />

				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-danger-baru" onclick="Sukubunga.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

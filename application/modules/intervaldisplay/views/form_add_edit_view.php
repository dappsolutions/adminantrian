<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Interval</u>
					</div>
				</div>
				<hr />

				<div class="row">
					<div class='col-md-3'>
						Waktu
					</div>
					<div class='col-md-3'>
						<input id="waktu" class="form-control required" error="Waktu" value="<?php echo isset($waktu) ? $waktu : '' ?>"/>
					</div>
				</div>
				<br />
				<hr />

				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-success" onclick="Intervaldisplay.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
						&nbsp;
						<button id="" class="btn btn-baru" onclick="Intervaldisplay.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Interval Display</u>
					</div>
				</div>
				<hr />

				<div class="row">
					<div class='col-md-3'>
						Waktu
					</div>
					<div class='col-md-3'>
						<b><?php echo $waktu ?></b>
					</div>
				</div>
				<br />

				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-danger-baru" onclick="Intervaldisplay.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type="hidden" value='<?php echo $data_kepuasan ?>' id="data_kepuasan" class="form-control" />


<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary padding-16">
					<div class="box-header with-border">
						<i class="fa fa-bar-chart-o"></i>

						<h3 class="box-title">Grafik Kepuasan</h3>
						<br />

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
							</button>
							<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body chart-responsive">
						<div class="row">
							<div class="col-md-3">
								<select class="form-control col-sm-3" error="Tahun" id="tahun" onchange="GrafikKepuasan.changeYear(this)">
									<?php if (!empty($list_year)) { ?>
										<?php foreach ($list_year as $value) { ?>
											<?php $selected = $value == $date ? 'selected' : '' ?>
											<option <?php echo $selected ?> value="<?php echo $value ?>"><?php echo $value ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3">
								<select class="form-control col-sm-3" error="Bulan" id="month" onchange="GrafikKepuasan.changeMonth(this)">
									<option value="">Pilih Bulan</option>
									<?php if (!empty($list_month)) { ?>
										<?php foreach ($list_month as $key => $value) { ?>
											<?php $selected = $key == $month ? 'selected' : '' ?>
											<option <?php echo $selected ?> value="<?php echo $key ?>"><?php echo $value ?></option>
										<?php } ?>
									<?php } ?>
								</select>
							</div>
							<div class="col-md-3">
								<!-- <button class="btn btn-success" onclick="GrafikKepuasan.setGrafikByDate(this)">TAMPILKAN</button> -->
							</div>
						</div>
						<br />
						<div class="row">
							<div class="col-md-12">
								<div id="bar-chart" style="height: 300px;"></div>
							</div>
						</div>
						<br />
						<br />
						<br />
					</div>
					<!-- /.box-body-->
				</div>
			</div>
		</div>
	</div>
</div>

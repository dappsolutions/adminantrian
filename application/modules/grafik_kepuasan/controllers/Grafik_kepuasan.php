<?php

class Grafik_kepuasan extends MX_Controller
{

	public $hak_akses;
	public $user;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		// echo '<pre>';
		// print_r($_SESSION);die;
		$this->hak_akses = $this->session->userdata('hak_akses');
		$this->user = $this->session->userdata('user_id');
	}

	public function getModuleName()
	{
		return 'grafik_kepuasan';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<link rel="stylesheet" href="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.css">',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/raphael/raphael.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.min.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/grafik_kepuasan.js"></script>',
		);

		return $data;
	}

	public function index()
	{
		$data['view_file'] = 'index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Grafik Kepuasan";
		$data['title_content'] = 'Grafik Kepuasan';
		$data['pengajuan'] = array();
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		$data['list_year'] = $this->getYear();
		$data['list_month'] = $this->getMonth();
		$data['date'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];

		$month = "";
		if (isset($_GET['month'])) {
			$data['date'] = $_GET['year'] . '-' . $_GET['month'];
			$month = $_GET['month'];
		}
		$data['month'] = $month;
		$data['data_kepuasan'] = $this->getDataKepuasanGrafik($data['date'], $month);
		echo Modules::run('template', $data);
	}

	public function grafik()
	{
		echo $this->load->view('grafik_chart');
	}

	public function getYear()
	{
		$year = date('Y');
		$data = array();
		for ($year = intval(date('Y')); $year < intval(date('Y')) + 3; $year++) {
			$data[$year] = $year;
		}

		return $data;
	}

	public function getMonth()
	{

		$data['01'] = "Januari";
		$data['02'] = "Februari";
		$data['03'] = "Maret";
		$data['04'] = "April";
		$data['05'] = "Mei";
		$data['06'] = "Juni";
		$data['07'] = "Juli";
		$data['08'] = "Agustus";
		$data['09'] = "September";
		$data['10'] = "Oktober";
		$data['11'] = "November";
		$data['12'] = "Desember";
		return $data;
	}


	public function getListPenilaian()
	{
		$data = Modules::run('database/get', array(
			'table' => 'kategori_penilaian kp',
			'field' => array(
				'kp.*',
			),
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}


	public function getHasilPenilaian($kepuasan, $rata2)
	{
		$hasil = "";
		if (!empty($kepuasan)) {
			foreach ($kepuasan as $key => $value) {
				list($awal, $akhir) = explode('-', $value['range']);
				if ($rata2 >= $awal && $rata2 <= $akhir) {
					$hasil = $value['kategori'];
				}
			}
		}

		return $hasil;
	}

	public function getGrafikPerMonth($date, $kategori_nilai)
	{
		$data = Modules::run('database/get', array(
			'table' => 'customer_nilai cn',
			'field' => array(
				'cn.*',
				'c.nama',
				'a.no_antrian',
				'kp.kategori',
				'kp.nilai',
			),
			'join' => array(
				array('antrian a', 'a.id = cn.antrian'),
				array('customer c', 'c.id = a.customer'),
				array('kategori_penilaian kp', 'kp.id = cn.kategori_penilaian'),
			),
			'like' => array(
				array('cn.createddate', $date)
			),
			'orderby' => 'cn.createddate desc',
			'limit' => 100000
		));


		$result = array();
		if (!empty($data)) {
			$temp = array();
			foreach ($data->result_array() as $key => $value) {
				if (!in_array($value['antrian'], $temp)) {
					$data_nilai = array();
					$total_nilai = 0;
					foreach ($data->result_array() as $k => $v) {
						if ($v['antrian'] == $value['antrian']) {
							$total_nilai += $v['nilai'];
							array_push($data_nilai, $v);
						}
					}
					$value['data_nilai'] = $data_nilai;
					$value['rata_rata'] = $total_nilai / count($data_nilai);
					$value['hasil'] = $this->getHasilPenilaian($kategori_nilai, $value['rata_rata']);
					array_push($result, $value);
					$temp[] = $value['antrian'];
				}
			}
		}


		//tidak puas
		$data_total['tgl_1_tp'] = 0;
		$data_total['tgl_2_tp'] = 0;
		$data_total['tgl_3_tp'] = 0;
		$data_total['tgl_4_tp'] = 0;
		$data_total['tgl_5_tp'] = 0;
		$data_total['tgl_6_tp'] = 0;
		$data_total['tgl_7_tp'] = 0;
		$data_total['tgl_8_tp'] = 0;
		$data_total['tgl_9_tp'] = 0;
		$data_total['tgl_10_tp'] = 0;
		$data_total['tgl_11_tp'] = 0;
		$data_total['tgl_12_tp'] = 0;
		$data_total['tgl_13_tp'] = 0;
		$data_total['tgl_14_tp'] = 0;
		$data_total['tgl_15_tp'] = 0;
		$data_total['tgl_16_tp'] = 0;
		$data_total['tgl_17_tp'] = 0;
		$data_total['tgl_18_tp'] = 0;
		$data_total['tgl_19_tp'] = 0;
		$data_total['tgl_20_tp'] = 0;
		$data_total['tgl_21_tp'] = 0;
		$data_total['tgl_22_tp'] = 0;
		$data_total['tgl_23_tp'] = 0;
		$data_total['tgl_24_tp'] = 0;
		$data_total['tgl_25_tp'] = 0;
		$data_total['tgl_26_tp'] = 0;
		$data_total['tgl_27_tp'] = 0;
		$data_total['tgl_28_tp'] = 0;
		$data_total['tgl_29_tp'] = 0;
		$data_total['tgl_30_tp'] = 0;
		$data_total['tgl_31_tp'] = 0;

		//kurang puas
		$data_total['tgl_1_kp'] = 0;
		$data_total['tgl_2_kp'] = 0;
		$data_total['tgl_3_kp'] = 0;
		$data_total['tgl_4_kp'] = 0;
		$data_total['tgl_5_kp'] = 0;
		$data_total['tgl_6_kp'] = 0;
		$data_total['tgl_7_kp'] = 0;
		$data_total['tgl_8_kp'] = 0;
		$data_total['tgl_9_kp'] = 0;
		$data_total['tgl_10_kp'] = 0;
		$data_total['tgl_11_kp'] = 0;
		$data_total['tgl_12_kp'] = 0;
		$data_total['tgl_13_kp'] = 0;
		$data_total['tgl_14_kp'] = 0;
		$data_total['tgl_15_kp'] = 0;
		$data_total['tgl_16_kp'] = 0;
		$data_total['tgl_17_kp'] = 0;
		$data_total['tgl_18_kp'] = 0;
		$data_total['tgl_19_kp'] = 0;
		$data_total['tgl_20_kp'] = 0;
		$data_total['tgl_21_kp'] = 0;
		$data_total['tgl_22_kp'] = 0;
		$data_total['tgl_23_kp'] = 0;
		$data_total['tgl_24_kp'] = 0;
		$data_total['tgl_25_kp'] = 0;
		$data_total['tgl_26_kp'] = 0;
		$data_total['tgl_27_kp'] = 0;
		$data_total['tgl_28_kp'] = 0;
		$data_total['tgl_29_kp'] = 0;
		$data_total['tgl_30_kp'] = 0;
		$data_total['tgl_31_kp'] = 0;


		//puas
		$data_total['tgl_1_p'] = 0;
		$data_total['tgl_2_p'] = 0;
		$data_total['tgl_3_p'] = 0;
		$data_total['tgl_4_p'] = 0;
		$data_total['tgl_5_p'] = 0;
		$data_total['tgl_6_p'] = 0;
		$data_total['tgl_7_p'] = 0;
		$data_total['tgl_8_p'] = 0;
		$data_total['tgl_9_p'] = 0;
		$data_total['tgl_10_p'] = 0;
		$data_total['tgl_11_p'] = 0;
		$data_total['tgl_12_p'] = 0;
		$data_total['tgl_13_p'] = 0;
		$data_total['tgl_14_p'] = 0;
		$data_total['tgl_15_p'] = 0;
		$data_total['tgl_16_p'] = 0;
		$data_total['tgl_17_p'] = 0;
		$data_total['tgl_18_p'] = 0;
		$data_total['tgl_19_p'] = 0;
		$data_total['tgl_20_p'] = 0;
		$data_total['tgl_21_p'] = 0;
		$data_total['tgl_22_p'] = 0;
		$data_total['tgl_23_p'] = 0;
		$data_total['tgl_24_p'] = 0;
		$data_total['tgl_25_p'] = 0;
		$data_total['tgl_26_p'] = 0;
		$data_total['tgl_27_p'] = 0;
		$data_total['tgl_28_p'] = 0;
		$data_total['tgl_29_p'] = 0;
		$data_total['tgl_30_p'] = 0;
		$data_total['tgl_31_p'] = 0;

		//sangat puas
		$data_total['tgl_1_sp'] = 0;
		$data_total['tgl_2_sp'] = 0;
		$data_total['tgl_3_sp'] = 0;
		$data_total['tgl_4_sp'] = 0;
		$data_total['tgl_5_sp'] = 0;
		$data_total['tgl_6_sp'] = 0;
		$data_total['tgl_7_sp'] = 0;
		$data_total['tgl_8_sp'] = 0;
		$data_total['tgl_9_sp'] = 0;
		$data_total['tgl_10_sp'] = 0;
		$data_total['tgl_11_sp'] = 0;
		$data_total['tgl_12_sp'] = 0;
		$data_total['tgl_13_sp'] = 0;
		$data_total['tgl_14_sp'] = 0;
		$data_total['tgl_15_sp'] = 0;
		$data_total['tgl_16_sp'] = 0;
		$data_total['tgl_17_sp'] = 0;
		$data_total['tgl_18_sp'] = 0;
		$data_total['tgl_19_sp'] = 0;
		$data_total['tgl_20_sp'] = 0;
		$data_total['tgl_21_sp'] = 0;
		$data_total['tgl_22_sp'] = 0;
		$data_total['tgl_23_sp'] = 0;
		$data_total['tgl_24_sp'] = 0;
		$data_total['tgl_25_sp'] = 0;
		$data_total['tgl_26_sp'] = 0;
		$data_total['tgl_27_sp'] = 0;
		$data_total['tgl_28_sp'] = 0;
		$data_total['tgl_29_sp'] = 0;
		$data_total['tgl_30_sp'] = 0;
		$data_total['tgl_31_sp'] = 0;

		if (!empty($result)) {
			foreach ($result as $value) {
				$kepuasan_date = date('d', strtotime($value['createddate']));
				for ($i = 1; $i < 32; $i++) {
					$tgl = $i < 10 ? '0' . $i : $i;
					if ($tgl == $kepuasan_date) {
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$data_total['tgl_' . $i . '_tp'] += 1;
								break;
							case 'Kurang Puas':
								$data_total['tgl_' . $i . '_kp'] += 1;
								break;
							case 'Puas':
								$data_total['tgl_' . $i . '_p'] += 1;
								break;
							case 'Sangat Puas':
								$data_total['tgl_' . $i . '_sp'] += 1;
								break;
							default:
								# code...
								break;
						}
					}
				}
			}
		}


		$result = array();

		for ($i = 1; $i < 32; $i++) {
			$kepuasan_date = $i < 10 ? '0' . $i : $i;
			array_push($result, array(
				'y' => $date . '-' . $kepuasan_date,
				'a' => $data_total['tgl_' . $i . '_tp'],
				'b' => $data_total['tgl_' . $i . '_kp'],
				'c' => $data_total['tgl_' . $i . '_p'],
				'd' => $data_total['tgl_' . $i . '_sp'],
			));
		}

		return json_encode($result);
	}

	public function insertKepuasan()
	{
		$data = Modules::run('database/get', array(
			'table' => 't_ikm',
			'limit' => 100000
		));

		$this->db->trans_begin();
		try {
			//code...
			if (!empty($data)) {
				foreach ($data->result_array() as $key => $value) {
					//insert into customer
					$push = array();
					$push['no_ktp'] = $value['no_identitas'];
					$push['nama'] = $value['nama'];
					$push['alamat'] = $value['alamat'];
					$push['no_tlp'] = $value['tlp'];
					$customer = Modules::run('database/_insert', 'customer', $push);

					//insert antrian
					$push = array();
					$push['customer'] = $customer;
					$push['no_antrian'] = $value['no_antri'];
					$antrian = Modules::run('database/_insert', 'antrian', $push);

					//insert antrian status
					$push = array();
					$push['antrian'] = $antrian;
					$push['status'] = 'DONE';
					Modules::run('database/_insert', 'antrian_status', $push);

					//insert customer nilai
					for ($i = 1; $i < 10; $i++) {
						$push = array();
						$push['antrian'] = $antrian;
						$push['questions_customer'] = $i;
						if ($value['t' . $i] >= 25 && $value['t' . $i] <= 49) {
							$push['kategori_penilaian'] = 1;
						}
						if ($value['t' . $i] >= 50 && $value['t' . $i] <= 74) {
							$push['kategori_penilaian'] = 2;
						}
						if ($value['t' . $i] >= 75 && $value['t' . $i] <= 99) {
							$push['kategori_penilaian'] = 3;
						}
						if ($value['t' . $i] >= 100 && $value['t' . $i] <= 100) {
							$push['kategori_penilaian'] = 4;
						}
						$push['createddate'] = $value['tgl'];
						$this->db->insert('customer_nilai', $push);
					}
				}
			}
			$this->db->trans_commit();
		} catch (\Exception $th) {
			//throw $th;
			$this->db->trans_rollback();
		}
	}


	public function getDataKepuasanGrafik($date = '', $month = '')
	{
		$kategori_nilai = $this->getListPenilaian();
		if ($date == '') {
			$date = date('Y');
		}

		if ($month != '') {
			$result = $this->getGrafikPerMonth($date, $kategori_nilai);
			return $result;
		}

		// echo $date;
		// die;

		$data = Modules::run('database/get', array(
			'table' => 'customer_nilai cn',
			'field' => array(
				'cn.*',
				'c.nama',
				'a.no_antrian',
				'kp.kategori',
				'kp.nilai',
			),
			'join' => array(
				array('antrian a', 'a.id = cn.antrian'),
				array('customer c', 'c.id = a.customer'),
				array('kategori_penilaian kp', 'kp.id = cn.kategori_penilaian'),
			),
			'like' => array(
				array('cn.createddate', $date)
			),
			'orderby' => 'cn.createddate desc',
			'limit' => 100000
		));

		// echo '<pre>';
		// print_r($data->row_array());
		// die;

		$result = array();
		if (!empty($data)) {
			$temp = array();
			foreach ($data->result_array() as $key => $value) {
				if (!in_array($value['antrian'], $temp)) {
					$data_nilai = array();
					$total_nilai = 0;
					foreach ($data->result_array() as $k => $v) {
						if ($v['antrian'] == $value['antrian']) {
							$total_nilai += $v['nilai'];
							array_push($data_nilai, $v);
						}
					}
					$value['data_nilai'] = $data_nilai;
					$value['rata_rata'] = $total_nilai / count($data_nilai);
					$value['hasil'] = $this->getHasilPenilaian($kategori_nilai, $value['rata_rata']);
					array_push($result, $value);
					$temp[] = $value['antrian'];
				}
			}
		}

		// echo '<pre>';
		// print_r($result);
		// die;

		// $result = array();
		//Tidak puas
		$totat_jan_tp = 0;
		$totat_feb_tp = 0;
		$totat_mar_tp = 0;
		$totat_apr_tp = 0;
		$totat_mei_tp = 0;
		$totat_jun_tp = 0;
		$totat_jul_tp = 0;
		$totat_ags_tp = 0;
		$totat_sep_tp = 0;
		$totat_okt_tp = 0;
		$totat_nov_tp = 0;
		$totat_des_tp = 0;

		//kurang puas
		$totat_jan_kp = 0;
		$totat_feb_kp = 0;
		$totat_mar_kp = 0;
		$totat_apr_kp = 0;
		$totat_mei_kp = 0;
		$totat_jun_kp = 0;
		$totat_jul_kp = 0;
		$totat_ags_kp = 0;
		$totat_sep_kp = 0;
		$totat_okt_kp = 0;
		$totat_nov_kp = 0;
		$totat_des_kp = 0;

		//puas
		$totat_jan_p = 0;
		$totat_feb_p = 0;
		$totat_mar_p = 0;
		$totat_apr_p = 0;
		$totat_mei_p = 0;
		$totat_jun_p = 0;
		$totat_jul_p = 0;
		$totat_ags_p = 0;
		$totat_sep_p = 0;
		$totat_okt_p = 0;
		$totat_nov_p = 0;
		$totat_des_p = 0;

		//Sangat puas
		$totat_jan_sp = 0;
		$totat_feb_sp = 0;
		$totat_mar_sp = 0;
		$totat_apr_sp = 0;
		$totat_mei_sp = 0;
		$totat_jun_sp = 0;
		$totat_jul_sp = 0;
		$totat_ags_sp = 0;
		$totat_sep_sp = 0;
		$totat_okt_sp = 0;
		$totat_nov_sp = 0;
		$totat_des_sp = 0;

		if (!empty($result)) {
			foreach ($result as $value) {
				$kepuasan_date = date('m', strtotime($value['createddate']));
				switch ($kepuasan_date) {
					case "01":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_jan_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_jan_kp += 1;
								break;
							case 'Puas':
								$totat_jan_p += 1;
								break;
							case 'Sangat Puas':
								$totat_jan_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "02":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_feb_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_feb_kp += 1;
								break;
							case 'Puas':
								$totat_feb_p += 1;
								break;
							case 'Sangat Puas':
								$totat_feb_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "03":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_mar_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_mar_kp += 1;
								break;
							case 'Puas':
								$totat_mar_p += 1;
								break;
							case 'Sangat Puas':
								$totat_mar_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "04":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_apr_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_apr_kp += 1;
								break;
							case 'Puas':
								$totat_apr_p += 1;
								break;
							case 'Sangat Puas':
								$totat_apr_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "05":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_mei_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_mei_kp += 1;
								break;
							case 'Puas':
								$totat_mei_p += 1;
								break;
							case 'Sangat Puas':
								$totat_mei_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "06":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_jun_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_jun_kp += 1;
								break;
							case 'Puas':
								$totat_jun_p += 1;
								break;
							case 'Sangat Puas':
								$totat_jun_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "07":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_jul_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_jul_kp += 1;
								break;
							case 'Puas':
								$totat_jul_p += 1;
								break;
							case 'Sangat Puas':
								$totat_jul_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "08":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_ags_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_ags_kp += 1;
								break;
							case 'Puas':
								$totat_ags_p += 1;
								break;
							case 'Sangat Puas':
								$totat_ags_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "09":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_sep_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_sep_kp += 1;
								break;
							case 'Puas':
								$totat_sep_p += 1;
								break;
							case 'Sangat Puas':
								$totat_sep_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "10":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_okt_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_okt_kp += 1;
								break;
							case 'Puas':
								$totat_okt_p += 1;
								break;
							case 'Sangat Puas':
								$totat_okt_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "11":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_nov_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_nov_kp += 1;
								break;
							case 'Puas':
								$totat_nov_p += 1;
								break;
							case 'Sangat Puas':
								$totat_nov_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;
					case "12":
						switch ($value['hasil']) {
							case 'Tidak Puas':
								$totat_des_tp += 1;
								break;
							case 'Kurang Puas':
								$totat_des_kp += 1;
								break;
							case 'Puas':
								$totat_des_p += 1;
								break;
							case 'Sangat Puas':
								$totat_des_sp += 1;
								break;
							default:
								# code...
								break;
						}
						break;

					default:
						break;
				}
			}
		}

		$result = array();

		for ($i = 1; $i < 13; $i++) {
			$kepuasan_date = $i < 10 ? '0' . $i : $i;
			switch ($kepuasan_date) {
				case "01":
					array_push($result, array(
						'y' => 'Jan',
						'a' => $totat_jan_tp,
						'b' => $totat_jan_kp,
						'c' => $totat_jan_p,
						'd' => $totat_jan_sp,
					));
					break;
				case "02":
					array_push($result, array(
						'y' => 'Feb',
						'a' => $totat_feb_tp,
						'b' => $totat_feb_kp,
						'c' => $totat_feb_p,
						'd' => $totat_feb_sp,
					));
					break;
				case "03":
					array_push($result, array(
						'y' => 'Mar',
						'a' => $totat_mar_tp,
						'b' => $totat_mar_kp,
						'c' => $totat_mar_p,
						'd' => $totat_mar_sp,
					));
					break;
				case "04":
					array_push($result, array(
						'y' => 'Apr',
						'a' => $totat_apr_tp,
						'b' => $totat_apr_kp,
						'c' => $totat_apr_p,
						'd' => $totat_apr_sp,
					));
					break;
				case "05":
					array_push($result, array(
						'y' => 'Mei',
						'a' => $totat_mei_tp,
						'b' => $totat_mei_kp,
						'c' => $totat_mei_p,
						'd' => $totat_mei_sp,
					));
					break;
				case "06":
					array_push($result, array(
						'y' => 'Jun',
						'a' => $totat_jun_tp,
						'b' => $totat_jun_kp,
						'c' => $totat_jun_p,
						'd' => $totat_jun_sp,
					));
					break;
				case "07":
					array_push($result, array(
						'y' => 'Jul',
						'a' => $totat_jul_tp,
						'b' => $totat_jul_kp,
						'c' => $totat_jul_p,
						'd' => $totat_jul_sp,
					));
					break;
				case "08":
					array_push($result, array(
						'y' => 'Ags',
						'a' => $totat_ags_tp,
						'b' => $totat_ags_kp,
						'c' => $totat_ags_p,
						'd' => $totat_ags_sp,
					));
					break;
				case "09":
					array_push($result, array(
						'y' => 'Sep',
						'a' => $totat_sep_tp,
						'b' => $totat_sep_kp,
						'c' => $totat_sep_p,
						'd' => $totat_sep_sp,
					));
					break;
				case "10":
					array_push($result, array(
						'y' => 'Okt',
						'a' => $totat_okt_tp,
						'b' => $totat_okt_kp,
						'c' => $totat_okt_p,
						'd' => $totat_okt_sp,
					));
					break;
				case "11":
					array_push($result, array(
						'y' => 'Nov',
						'a' => $totat_nov_tp,
						'b' => $totat_nov_kp,
						'c' => $totat_nov_p,
						'd' => $totat_nov_sp,
					));
					break;
				case "12":
					array_push($result, array(
						'y' => 'Des',
						'a' => $totat_des_tp,
						'b' => $totat_des_kp,
						'c' => $totat_des_p,
						'd' => $totat_des_sp,
					));
					break;

				default:
					break;
			}
		}

		// echo '<pre>';
		// print_r($result);
		// die;
		return json_encode($result);
	}
}

<?php

class Report_kepuasan extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function getModuleName()
	{
		return 'report_kepuasan';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/raphael/raphael.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.min.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/report_kepuasan.js"></script>',
		);

		return $data;
	}

	public function index()
	{
		$data['view_file'] = 'v_index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Laporan Kepuasan";
		$data['title_content'] = 'Laporan Kepuasan';
		$data['date_now'] = Modules::run('helper/getIndoDate', date('Y-m-d'), false);
		$kepuasan = $this->getListPenilaian();
		$data['data'] = $this->getDataLaporanKepuasan($kepuasan);
		echo Modules::run('template', $data);
	}


	public function getListPenilaian()
	{
		$data = Modules::run('database/get', array(
			'table' => 'kategori_penilaian kp',
			'field' => array(
				'kp.*',
			),
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}


	public function getDataLaporanKepuasan($kategori_nilai)
	{
		$data = Modules::run('database/get', array(
			'table' => 'customer_nilai cn',
			'field' => array(
				'cn.*',
				'c.nama',
				'a.no_antrian',
				'kp.kategori',
				'kp.nilai',
			),
			'join' => array(
				array('antrian a', 'a.id = cn.antrian'),
				array('customer c', 'c.id = a.customer'),
				array('kategori_penilaian kp', 'kp.id = cn.kategori_penilaian'),
			),
			// 'where' => 'cn.antrian = 641',
			'orderby' => 'cn.createddate desc',
			'limit' => 100000
		));

		// echo '<pre>';
		// print_r($data->result_array());
		// die;
		$result = array();
		if (!empty($data)) {
			$temp = array();
			$data_result = $data->result_array();
			foreach ($data_result as $key => $value) {
				if (!in_array($value['antrian'], $temp)) {
					$data_nilai = array();
					$total_nilai = 0;
					foreach ($data_result as $v_data) {
						if (trim($v_data['antrian']) == trim($value['antrian'])) {
							$total_nilai += $v_data['nilai'];
							array_push($data_nilai, $v_data);
						}
					}
					$value['data_nilai'] = $data_nilai;
					$value['rata_rata'] = $total_nilai / count($data_nilai);
					$value['hasil'] = $this->getHasilPenilaian($kategori_nilai, $value['rata_rata']);
					array_push($result, $value);
					$temp[] = $value['antrian'];
				}
			}
		}

		// echo '<pre>';
		// print_r($result);
		// die;
		// '<pre>';
		// print_r($result);
		// die;


		return $result;
	}

	public function getHasilPenilaian($kepuasan, $rata2)
	{
		$hasil = "";
		if (!empty($kepuasan)) {
			foreach ($kepuasan as $key => $value) {
				list($awal, $akhir) = explode('-', $value['range']);
				if ($rata2 >= $awal && $rata2 <= $akhir) {
					$hasil = $value['kategori'];
				}
			}
		}

		return $hasil;
	}

	public function getDataAntrianProses($limit = "2")
	{
		$filter = strtolower($this->session->userdata('hak_akses')) == 'teller' ? 'A' : 'B';
		$data = Modules::run('database/get', array(
			'table' => 'antrian a',
			'field' => array(
				'a.*',
				'c.nama',
				'ist.status'
			),
			'join' => array(
				array('customer c', 'c.id = a.customer'),
				array('(select max(id) id, antrian from antrian_status group by antrian) iss', 'iss.antrian = a.id'),
				array('antrian_status ist', 'ist.id = iss.id'),
			),
			'where' => "ist.status = 'WAIT' and a.deleted = 0",
			'like' => array(
				array('a.no_antrian', $filter)
			),
			'inside_brackets' => true,
			'limit' => $limit,
			'orderby' => 'a.id asc'
		));


		$result = array();
		$dilter_data = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				if ($value['status'] == 'WAIT') {
					$value['status'] = 'MENUNGGU ANTRIAN';
				}

				$jenis_no = strpos($value['no_antrian'], $filter);
				if ($jenis_no === false) {
					// array_push($result, $value);
				} else {
					array_push($result, $value);
				}
			}
		}

		return $result;
	}

	public function prosesNextAntrian()
	{
		$data = $_POST;
		$is_valid = false;
		$this->db->trans_begin();
		try {
			$push = array();
			$push['antrian'] = $data['current_antrian'];
			$push['status'] = 'DONE';
			Modules::run('database/_insert', 'antrian_status', $push);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array(
			'is_valid' => $is_valid
		));
	}
}

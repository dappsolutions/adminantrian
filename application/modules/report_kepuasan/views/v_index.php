<?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
	<div class="content">
		<div class="animated fadeIn">
			<div class="box padding-16">
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-10">
							<div class="box-title">
								<i class="mdi mdi-chart-bar mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
							</div>
						</div>
						<div class="col-sm-2 text-right"></div>
					</div>
				</div>
				<div class="box-body">

					<div class='row'>
						<div class='col-md-12'>
							<div class='table-responsive'>
								<table class="table table-bordered">
									<thead>
										<tr class="bg-primary">
											<th>Tanggal Kedatangan</th>
											<th>No Antrian</th>
											<th>Nama Pelanggan</th>
											<th>Pertanyaan 1</th>
											<th>Pertanyaan 2</th>
											<th>Pertanyaan 3</th>
											<th>Pertanyaan 4</th>
											<th>Pertanyaan 5</th>
											<th>Pertanyaan 6</th>
											<th>Pertanyaan 7</th>
											<th>Pertanyaan 8</th>
											<th>Pertanyaan 9</th>
											<th>Rata Rata</th>
											<th>Hasil</th>
										</tr>
									</thead>
									<tbody>
										<?php if (!empty($data)) { ?>
											<?php $no = 1; ?>
											<?php foreach ($data as $value) { ?>
												<tr>
													<td><?php echo $value['createddate'] ?></td>
													<td><?php echo $value['no_antrian'] ?></td>
													<td><?php echo $value['nama'] ?></td>
													<?php if (!empty($value['data_nilai'])) { ?>
														<?php foreach ($value['data_nilai'] as $k => $v) { ?>
															<td class="text-center"><?php echo $v['nilai'] ?></td>
														<?php } ?>
													<?php } else { ?>
														<td colspan="11" class="text-center">Tidak ada data</td>
													<?php } ?>
													<td class="text-center"><?php echo number_format($value['rata_rata'], 2) ?></td>
													<td class="text-center"><?php echo $value['hasil'] ?></td>
												</tr>
											<?php } ?>
										<?php } else { ?>
											<tr>
												<td colspan="14" class="text-center">Tidak ada data ditemukan</td>
											</tr>
										<?php } ?>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

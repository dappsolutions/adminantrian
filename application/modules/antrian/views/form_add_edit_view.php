<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>'/>
<div class="content">
 <div class="animated fadeIn">
  <div class="box padding-16">
   <div class="box-body box-block">   
    <div class="row">
     <div class='col-md-3 text-bold'>
      Hak Akses
     </div>
     <div class='col-md-3'>
      <select id="akses"  class="form-control required" error="Hak Akses">
       <?php if (!empty($list_akses)) { ?>
        <?php foreach ($list_akses as $value) { ?>       
         <?php
         $selected = "";
         if (isset($hak_akses)) {
          $selected = $hak_akses == $value['hak_akses'] ? 'selected' : '';
         }
         ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['hak_akses'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Pegawai
     </div>
     <div class='col-md-3'>
      <select id="pegawai" class="form-control required" error="Pegawai">
       <option value="-">Pilih Pegawai</option>
       <?php if (!empty($list_pegawai)) { ?>
        <?php foreach ($list_pegawai as $value) { ?>       
         <?php
         $selected = "";
         if (isset($pegawai)) {
          $selected = $pegawai == $value['id'] ? 'selected' : '';
         }
         ?>
         <option <?php echo $selected ?> value="<?php echo $value['id'] ?>"><?php echo $value['nama'] ?></option>
        <?php } ?>
       <?php } ?>
      </select>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Username
     </div>
     <div class='col-md-3'>
      <input type='text' name='' id='username' class='form-control required' 
             value='<?php echo isset($username) ? $username : '' ?>' error="Username"/>
     </div>     
    </div>
    <br/>

    <div class="row">
     <div class='col-md-3 text-bold'>
      Password
     </div>
     <div class='col-md-3'>
      <input type='password' name='' id='password' class='form-control required' 
             value='<?php echo isset($password) ? $password : '' ?>' error="Password"/>
     </div>     
    </div>
    <br/>

    <hr/>
    <div class='row'>
     <div class='col-md-12 text-right'>
      <button id="" class="btn btn-success" onclick="Pengguna.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
      &nbsp;
      <button id="" class="btn btn-baru" onclick="Pengguna.back()">Kembali</button>
     </div>
    </div>
   </div>
  </div>
 </div>
</div>

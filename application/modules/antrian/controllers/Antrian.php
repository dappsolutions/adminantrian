<?php

class Antrian extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'antrian';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/antrian.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 'antrian';
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Antrian";
		$data['title_content'] = 'Data Antrian';
		$content = $this->getData();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function getTotalData($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('a.no_antrian', $keyword),
				array('c.nama', $keyword),
			);
		}
		$total = Modules::run('database/count_all', array(
			'table' => 'antrian a',
			'field' => array(
				'a.*',
				'c.nama',
				'ist.status'
			),
			'join' => array(
				array('customer c', 'c.id = a.customer'),
				array('(select max(id) id, antrian from antrian_status group by antrian) iss', 'iss.antrian = a.id'),
				array('antrian_status ist', 'ist.id = iss.id'),
			),
			'like' => $like,
			'is_or_like' => true,
			'where' => "a.deleted = 0"
		));

		return $total;
	}

	public function getData($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('a.no_antrian', $keyword),
				array('c.nama', $keyword),
			);
		}
		$data = Modules::run('database/get', array(
			'table' => 'antrian a',
			'field' => array(
				'a.*',
				'c.nama',
				'ist.status'
			),
			'join' => array(
				array('customer c', 'c.id = a.customer'),
				array('(select max(id) id, antrian from antrian_status group by antrian) iss', 'iss.antrian = a.id'),
				array('antrian_status ist', 'ist.id = iss.id'),
			),
			'like' => $like,
			'is_or_like' => true,
			'limit' => $this->limit,
			'offset' => $this->last_no,
			'where' => "a.deleted = 0",
			"orderby" => "a.id asc"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalData($keyword)
		);
	}

	public function getDetailData($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' kr',
			'field' => array('kr.*', 'p.hak_akses'),
			'join' => array(
				array('priveledge p', 'kr.priveledge = p.id')
			),
			'where' => "kr.id = '" . $id . "'"
		));

		return $data->row_array();
	}

	public function getListPriveledge()
	{
		$data = Modules::run('database/get', array(
			'table' => 'priveledge',
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function getListPegawai()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pegawai',
			'where' => "deleted = 0"
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah Pengguna";
		$data['title_content'] = 'Tambah Pengguna';
		$data['list_akses'] = $this->getListPriveledge();
		$data['list_pegawai'] = $this->getListPegawai();
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = $this->getDetailData($id);
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Pengguna";
		$data['title_content'] = 'Ubah Pengguna';
		$data['list_akses'] = $this->getListPriveledge();
		echo Modules::run('template', $data);
	}

	public function detail($id)
	{
		$data = $this->getDetailData($id);
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Pengguna";
		$data['title_content'] = 'Detail Pengguna';
		echo Modules::run('template', $data);
	}

	public function getPostDataHeader($value)
	{
		$data['username'] = $value->username;
		$data['password'] = md5($value->password);
		$data['priveledge'] = $value->priveledge;
		if ($value->pegawai != '-') {
			$data['pegawai'] = $value->pegawai;
		}
		return $data;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));

		$id = $this->input->post('id');
		$is_valid = false;
		$pengguna = $id;
		$this->db->trans_begin();
		try {
			$post_user = $this->getPostDataHeader($data);
			if ($id == '') {
				$pengguna = Modules::run('database/_insert', $this->getTableName(), $post_user);
			} else {
				//update
				Modules::run('database/_update', $this->getTableName(), $post_user, array('id' => $id));
			}
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'pengguna' => $pengguna));
	}

	public function delete($id)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', 'user', array('deleted' => true), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Antrian";
		$data['title_content'] = 'Data Antrian';
		$content = $this->getData($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}
}

<div class="row">
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-aqua">
			<div class="inner">
				<h5>&nbsp;</h5>

				<p>Total Antrian</p>
			</div>
			<div class="icon">
				<i class="ion ion-person"></i>
			</div>
			<a href="<?php echo base_url() . 'faktur_pelanggan' ?>" class="small-box-footer"> Antrian <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-green">
			<div class="inner">
				<h5>&nbsp;</h5>

				<p>Total Customer</p>
			</div>
			<div class="icon">
				<i class="ion ion-stats-bars"></i>
			</div>
			<a href="<?php echo base_url() . 'faktur_pelanggan' ?>" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-yellow">
			<div class="inner">
				<h5>&nbsp;</h5>

				<p>Total Antrian Selesai</p>
			</div>
			<div class="icon">
				<i class="ion ion-android-menu"></i>
			</div>
			<a href="<?php echo base_url()  ?>" class="small-box-footer"> <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
	<div class="col-lg-3 col-xs-6">
		<!-- small box -->
		<div class="small-box bg-red">
			<div class="inner">
				<h5>&nbsp;</h5>

				<p>Total Antrian Menunggu</p>
			</div>
			<div class="icon">
				<i class="ion ion-pie-graph"></i>
			</div>
			<a href="<?php echo base_url() . 'pelanggan' ?>" class="small-box-footer">Customer <i class="fa fa-arrow-circle-right"></i></a>
		</div>
	</div>
	<!-- ./col -->
</div>
<div class="row">
	<div class="col-md-12 text-center">
		<div class="">
			<div class="row">
				<div class="col-md-6">
					<div class="box padding-16">
						<div class="box-header with-border">
							<div class="row">
								<div class="col-md-10">
									<div class="box-title">
										<i class="mdi mdi-chart-bar mdi-18px"></i><strong class="card-title"> FORM <?php echo strtoupper($this->session->userdata('hak_akses')) ?></strong>
									</div>
								</div>
								<div class="col-sm-2 text-right"></div>
							</div>
						</div>
						<div class="box-body">
							<div class="row">
								<div class="col-md-12 text-center">
									<div class="table-responsive">
										<table class="table">
											<tbody>
												<tr class="bg-primary">
													<td colspan="3">
														<h3><?php echo ucfirst($this->session->userdata('username')) ?></h3>
													</td>
												</tr>
												<tr>
													<td colspan="3">
														<?php if (empty($data_antrian_proses)) { ?>
															<h3>Current Number -</h3>
															<h3>Waiting Number -</h3>
														<?php } else { ?>
															<?php if (count($data_antrian_proses) > 1) { ?>
																<h3>Current Number <b class="text-success" data_id="<?php echo $data_antrian_proses[0]['id'] ?>" id="current-number"><?php echo $data_antrian_proses[0]['no_antrian'] ?></b></h3>
																<h3>Waiting Number <b class="text-warning"><?php echo $data_antrian_proses[1]['no_antrian'] ?></b></h3>
															<?php } else { ?>
																<h3>Current Number <b class="text-success" data_id="<?php echo $data_antrian_proses[0]['id'] ?>" id="current-number"><?php echo $data_antrian_proses[0]['no_antrian'] ?></b></h3>
																<h3>Waiting Number - </h3>
															<?php } ?>
														<?php } ?>
													</td>
												</tr>
												<tr>
													<td colspan="3">
														<input type="text" class="form-control" id="input-antrian" onkeyup="Dashboard.inputAntrian(this, event)">
													</td>
												</tr>
												<tr>
													<td>
														<table class="">
															<tr>
																<td><button class="btn btn-success" onclick="Dashboard.addToAntrian(this)">1</button></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><button class="btn btn-success" onclick="Dashboard.addToAntrian(this)">2</button></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><button class="btn btn-success" onclick="Dashboard.addToAntrian(this)">3</button></td>
															</tr>
															<tr>
																<td colspan="10">&nbsp;</td>
															</tr>
															<tr>
																<td><button class="btn btn-success" onclick="Dashboard.addToAntrian(this)">4</button></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><button class="btn btn-success" onclick="Dashboard.addToAntrian(this)">5</button></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><button class="btn btn-success" onclick="Dashboard.addToAntrian(this)">6</button></td>
															</tr>
															<tr>
																<td colspan="10">&nbsp;</td>
															</tr>
															<tr>
																<td><button class="btn btn-success btn-block" onclick="Dashboard.addToAntrian(this)">7</button></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><button class="btn btn-success btn-block" onclick="Dashboard.addToAntrian(this)">8</button></td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td>&nbsp;</td>
																<td><button class="btn btn-success btn-block" onclick="Dashboard.addToAntrian(this)">9</button></td>
															</tr>
															<tr>
																<td colspan="10">&nbsp;</td>
															</tr>
															<tr>
																<td colspan="10"><button class="btn btn-success btn-block" onclick="Dashboard.addToAntrian(this)">0</button></td>
															</tr>
															<tr>
																<td colspan="10">&nbsp;</td>
															</tr>
															<tr>
																<td colspan="10"><?php echo ucfirst($this->session->userdata('username')) ?></td>
															</tr>
															<tr>
																<td colspan="10"><?php echo date('Y-m-d H:i:s') ?></td>
															</tr>
														</table>
													</td>
													<td>&nbsp;</td>
													<td class="text-right">
														<table class="table">
															<tr>
																<td><button class="btn btn-success btn-block" onclick="Dashboard.prosesNextAntrian(this)">Next&nbsp;&nbsp;</button></td>
																<td>&nbsp;</td>
																<td><button class="btn btn-success btn-block">Repeat</button></td>
															</tr>
															<tr>
																<td colspan="3">&nbsp;</td>
															</tr>
															<tr>
																<td colspan="3"><button class="btn btn-success btn-block">Direct&nbsp;&nbsp;</button></td>
															</tr>
															<tr>
																<td colspan="3">&nbsp;</td>
															</tr>
															<tr>
																<td colspan="3"><button class="btn btn-success btn-block">Transfer&nbsp;&nbsp;</button></td>
															</tr>
														</table>
													</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>


				<div class="col-md-6">
					<div class="box padding-16">
						<div class="box-body">

							<div class='row'>
								<div class='col-md-12'>
									<h4><u>Top 5 Antrian</u></h4>
									<br />
									<div class='table-responsive'>
										<table class="table table-bordered">
											<thead>
												<tr class="bg-primary">
													<th>No </th>
													<th>No Antrian</th>
													<th>Nama Pelanggan</th>
													<th>Status</th>
												</tr>
											</thead>
											<tbody>
												<?php if (!empty($data_antrian)) { ?>
													<?php $no = 1; ?>
													<?php foreach ($data_antrian as $value) { ?>
														<tr>
															<td><?php echo $no++ ?></td>
															<td><?php echo $value['no_antrian'] ?></td>
															<td><?php echo $value['nama'] ?></td>
															<td class="bg-danger"><?php echo $value['status'] ?></td>
														</tr>
													<?php } ?>
												<?php } else { ?>
													<tr>
														<td colspan="6" class="text-center">Tidak ada data ditemukan</td>
													</tr>
												<?php } ?>

											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
	<?php echo $this->load->view('top_dashboard', array(), true); ?>
<?php } ?>

<?php if ($this->session->userdata('hak_akses') == 'teller' || $this->session->userdata('hak_akses') == 'customerservice') { ?>
	<?php echo $this->load->view('form_teller_cs') ?>
<?php } ?>


<?php if ($this->session->userdata('hak_akses') == 'superadmin') { ?>
	<div class="content">
		<div class="animated fadeIn">
			<div class="box padding-16">
				<div class="box-header with-border">
					<div class="row">
						<div class="col-md-10">
							<div class="box-title">
								<i class="mdi mdi-chart-bar mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
							</div>
						</div>
						<div class="col-sm-2 text-right"></div>
					</div>
				</div>
				<div class="box-body">

					<div class='row'>
						<div class='col-md-12'>
							<h4><u>Top 5 Antrian</u></h4>
							<br />
							<div class='table-responsive'>
								<table class="table table-bordered">
									<thead>
										<tr class="bg-primary">
											<th>No</th>
											<th>No Antrian</th>
											<th>Nama Pelanggan</th>
											<th>Status</th>
										</tr>
									</thead>
									<tbody>
										<?php if (!empty($data_antrian)) { ?>
											<?php $no = 1; ?>
											<?php foreach ($data_antrian as $value) { ?>
												<tr>
													<td><?php echo $no++ ?></td>
													<td><?php echo $value['no_antrian'] ?></td>
													<td><?php echo $value['nama'] ?></td>
													<td class="bg-danger"><?php echo $value['status'] ?></td>
												</tr>
											<?php } ?>
										<?php } else { ?>
											<tr>
												<td colspan="6" class="text-center">Tidak ada data ditemukan</td>
											</tr>
										<?php } ?>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>

<?php

class Dashboard extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function getModuleName()
	{
		return 'dashboard';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/raphael/raphael.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.min.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/dashboard.js"></script>',
		);

		return $data;
	}

	public function index()
	{
		$data['view_file'] = 'v_index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Dashboard";
		$data['title_content'] = 'Dashboard';
		// $data['data_antrian'] = $this->getTotalAntrianMenunggu(5);
		// $data['data_antrian_proses'] = $this->getDataAntrianProses(2);

		$data['date_now'] = Modules::run('helper/getIndoDate', date('Y-m-d'), false);
		echo Modules::run('template', $data);
	}


	public function getTotalAntrianMenunggu($limit = "1000")
	{
		$data = Modules::run('database/get', array(
			'table' => 'antrian a',
			'field' => array(
				'a.*',
				'c.nama',
				'ist.status'
			),
			'join' => array(
				array('customer c', 'c.id = a.customer'),
				array('(select max(id) id, antrian from antrian_status group by antrian) iss', 'iss.antrian = a.id'),
				array('antrian_status ist', 'ist.id = iss.id'),
			),
			'where' => "ist.status = 'WAIT' and a.deleted = 0",
			'limit' => $limit,
			'orderby' => 'a.id desc'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				if ($value['status'] == 'WAIT') {
					$value['status'] = 'MENUNGGU ANTRIAN';
				}
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getDataAntrianProses($limit = "2")
	{
		$filter = strtolower($this->session->userdata('hak_akses')) == 'teller' ? 'A' : 'B';
		$data = Modules::run('database/get', array(
			'table' => 'antrian a',
			'field' => array(
				'a.*',
				'c.nama',
				'ist.status'
			),
			'join' => array(
				array('customer c', 'c.id = a.customer'),
				array('(select max(id) id, antrian from antrian_status group by antrian) iss', 'iss.antrian = a.id'),
				array('antrian_status ist', 'ist.id = iss.id'),
			),
			'where' => "ist.status = 'WAIT' and a.deleted = 0",
			'like' => array(
				array('a.no_antrian', $filter)
			),
			'inside_brackets' => true,
			'limit' => $limit,
			'orderby' => 'a.id asc'
		));


		$result = array();
		$dilter_data = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				if ($value['status'] == 'WAIT') {
					$value['status'] = 'MENUNGGU ANTRIAN';
				}

				$jenis_no = strpos($value['no_antrian'], $filter);
				if ($jenis_no === false) {
					// array_push($result, $value);
				} else {
					array_push($result, $value);
				}
			}
		}

		return $result;
	}

	public function prosesNextAntrian()
	{
		$data = $_POST;
		$is_valid = false;
		$this->db->trans_begin();
		try {
			$push = array();
			$push['antrian'] = $data['current_antrian'];
			$push['status'] = 'DONE';
			Modules::run('database/_insert', 'antrian_status', $push);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array(
			'is_valid' => $is_valid
		));
	}
}

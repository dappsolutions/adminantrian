<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Eloquent\Model as Eloquent;
class T_kurs extends Eloquent
{
    protected $table = 't_kurs';
    protected $primaryKey = 'nox';
}

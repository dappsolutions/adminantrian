<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Pegawai</u>
					</div>
				</div>
				<hr />

				<div class="row">
					<div class='col-md-3'>
						Hak Akses
					</div>
					<div class='col-md-3'>
						<?php echo $hak_akses ?>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3'>
						Username
					</div>
					<div class='col-md-3'>
						<?php echo $username ?>
					</div>
				</div>
				<br />

				<div class='row'>
					<div class='col-md-3'>
						Password
					</div>
					<div class='col-md-3'>
						<?php echo $password ?>
					</div>
				</div>
				<br />

				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-danger-baru" onclick="User.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data User</u>
					</div>
				</div>
				<hr />


				<div class="row">
					<div class='col-md-3'>
						Hak Akses
					</div>
					<div class='col-md-3'>
						<select id="hak_akses" error="Hak Akses" class="form-control required">
							<?php if (!empty($list_hak_akses)) { ?>
								<?php foreach ($list_hak_akses as $v_hak) { ?>
									<?php
									$selected = "";
									if (isset($hak_akses)) {
										if ($v_hak['id'] == $hak_akses) {
											$selected = 'selected';
										}
									}
									?>
									<option <?php echo $selected ?> value="<?php echo $v_hak['id'] ?>"><?php echo $v_hak['hak_akses'] ?></option>
								<?php } ?>
							<?php } else { ?>
								<option value="">Tidak Ada Hak Akses</option>
							<?php } ?>
						</select>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3'>
						Username
					</div>
					<div class='col-md-3'>
						<input type='text' name='' id='username' class='form-control required' value='<?php echo isset($username) ? $username : '' ?>' error="Username" />
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-3'>
						Password
					</div>
					<div class='col-md-3'>
						<input type='text' name='' id='password' class='form-control required' value='<?php echo isset($password) ? $password : '' ?>' error="Password" />
					</div>
				</div>
				<br />
				<hr />

				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-success" onclick="User.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
						&nbsp;
						<button id="" class="btn btn-baru" onclick="User.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php

if (!defined('BASEPATH'))
 exit('No direct script access allowed');

class Pagination extends MX_Controller {

 public function get_pagination($url, $uri, $total_rows, $limit, $last_no) {
  $this->load->library('pagination');
  $config["base_url"] = base_url() . $url;
  $config['first_url'] = '1';
  $config['uri_segment'] = $uri;
  $config["total_rows"] = $total_rows;
  $config["per_page"] = $limit;
  $config['use_page_numbers'] = true;
  
  $config['cur_tag_open'] = "<li class='page-item active'>";
  $config['cur_tag_close'] = "</li>";
  $config['prev_tag_open'] = "<li class='page-item'>";
  $config['prev_tag_close'] = "</li>";
  $config['num_tag_open'] = "<li class='page-item'>";
  $config['num_tag_close'] = "</li>";
  $config['next_tag_open'] = "<li class='page-item'>";
  $config['next_tag_close'] = "</li>";
  $config['first_tag_open'] = "<li class='page-item'>";
  $config['first_tag_close'] = "</li>";
  $config['last_tag_open'] = "<li class='page-item'>";
  $config['last_tag_close'] = "</li>";
  $config['full_tag_open'] = '<div class="col-md-12 text-right"><nav aria-label="Page navigation example"><ul class="pagination justify-content-end">';
  $config['full_tag_close'] = '</ul></nav></div>';
  $this->pagination->initialize($config);
  return array(
   'links' => $this->pagination->create_links(),
   'last_no' => $last_no,
   'total_rows' => $total_rows
  );
 }

 public function get_limit() {
  return 1;
 }

 public function get_custom_limit($limit = 10) {
  return $limit;
 }


 public function generate($paging_data = array(), $all_data = array()){
  $page_active = isset($_GET['page']) ? $_GET['page'] : 1;
  $limit = isset($_GET['limit']) ? $_GET['limit'] : 2;
  // $paginate = Production_forecast::paginate($limit);
  $paginate = $paging_data;
  $links = $paginate->toArray()['links'];
  // $data_all = Production_forecast::all();
  $data_all = $all_data;
  $data_active = array();
  if($page_active == 1){
      $data_active = $data_all->take($limit);
  }else{
      $next = $page_active -1;
      $data_active = $data_all->skip($limit*$next)->take($limit);
  }
  
  return array(
      'data'=> $data_active->toArray(),
      'links'=> $links
  );
}

}

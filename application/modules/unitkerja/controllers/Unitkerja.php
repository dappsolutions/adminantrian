<?php

class Unitkerja extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
		$this->load->model('storage/t_data');
	}

	public function getModuleName()
	{
		return 'unitkerja';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/unitkerja.js"></script>'
		);

		return $data;
	}

	public function getTableName()
	{
		return 't_data';
	}

	public function index()
	{
		$this->segment = 3;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;

		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Pertanyaan";
		$data['title_content'] = 'Data Pertanyaan';
		$content = $this->getData();
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);

		$data['hak_akses'] = $this->session->userdata('hak_akses');
		echo Modules::run('template', $data);
	}

	public function getTotalData($keyword = '')
	{
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('u.uker', $keyword),
				array('u.almt', $keyword),
				array('u.tlp', $keyword),
				array('u.link', $keyword),
			);
		}
		$total = Modules::run('database/count_all', array(
			'table' => $this->getTableName() . ' u',
			'field' => array('u.*'),
			'like' => $like,
			'is_or_like' => true,
		));

		return $total;
	}

	public function getData($keyword = '')
	{
		// $data = T_data::paginate(1);
		// echo '<pre>';
		// print_r($data->toArray());die;
		$like = array();
		if ($keyword != '') {
			$like = array(
				array('u.uker', $keyword),
				array('u.almt', $keyword),
				array('u.tlp', $keyword),
				array('u.link', $keyword),
			);
		}
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' u',
			'field' => array('u.*'),
			'like' => $like,
			'is_or_like' => true,
			'limit' => $this->limit,
			'offset' => $this->last_no,
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		return array(
			'data' => $result,
			'total_rows' => $this->getTotalData($keyword)
		);
	}

	public function getDetailData($id)
	{
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' u',
			'where' => "u.id = '" . $id . "'"
		));

		return $data->row_array();
	}

	public function getListPegawai()
	{
		$data = Modules::run('database/get', array(
			'table' => 'pegawai p',
			'field' => array('p.*', 'u.hak_akses as hak_akses'),
			'join' => array(
				array('user u', 'p.id = u.pegawai', 'left')
			)
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				if ($value['hak_akses'] == '') {
					array_push($result, $value);
				}
			}
		}


		return $result;
	}

	public function getListHakAkses()
	{
		$data = Modules::run('database/get', array(
			'table' => 'hak_akses'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}


		return $result;
	}

	public function add()
	{
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Tambah User";
		$data['title_content'] = 'Tambah User';
		// $data['list_pegawai'] = $this->getListPegawai();
		$data['list_hak_akses'] = $this->getListHakAkses();
		echo Modules::run('template', $data);
	}

	public function ubah($id)
	{
		$data = T_data::findOrFail($id)->toArray();
		$data['view_file'] = 'form_add_edit_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Ubah Unit Kerja";
		$data['title_content'] = 'Ubah Unit Kerja';
		$data['hak_akses'] = $this->session->userdata('hak_akses');
		echo Modules::run('template', $data);
	}

	public function detail($id)
	{
		$data = T_data::findOrFail($id)->toArray();
		$data['view_file'] = 'detail_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Detail Pertanyaan";
		$data['title_content'] = 'Detail Pertanyaan';
		echo Modules::run('template', $data);
	}

	public function getPostDataHeader($value)
	{
		$data['uker'] = $value->unit_kerja;
		$data['almt'] = $value->alamat;
		return $data;
	}

	public function simpan()
	{
		$data = json_decode($this->input->post('data'));
		$id = $this->input->post('id');


		// echo '<pre>';
		// print_r($data);die;
		$is_valid = false;
		$this->db->trans_begin();
		try {
			$post_user = $this->getPostDataHeader($data);
			// echo '<pre>';
			// print_r($post_user);
			// die;
			if ($id == '') {
				$user = Modules::run('database/_insert', $this->getTableName(), $post_user);
			} else {
				//update
				Modules::run('database/_update', $this->getTableName(), $post_user, array('id' => $id));
			}
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function search($keyword)
	{
		$this->segment = 4;
		$this->page = $this->uri->segment($this->segment) ?
			$this->uri->segment($this->segment) - 1 : 0;
		$this->last_no = $this->page * $this->limit;
		$keyword = urldecode($keyword);

		$data['keyword'] = $keyword;
		$data['view_file'] = 'index_view';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = "Data Pertanyaan";
		$data['title_content'] = 'Data Pertanyaan';
		$content = $this->getData($keyword);
		$data['content'] = $content['data'];
		$total_rows = $content['total_rows'];
		$data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
		echo Modules::run('template', $data);
	}

	public function setUserAktifNonAktif($id, $is_active)
	{
		$is_valid = false;
		$this->db->trans_begin();
		try {
			Modules::run('database/_update', 'user', array('is_active' => $is_active), array('id' => $id));
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}
}

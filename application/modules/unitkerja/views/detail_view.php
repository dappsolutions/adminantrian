<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Unit Kerja</u>
					</div>
				</div>
				<hr />

				<div class="row">
					<div class='col-md-3'>
						Unit Kerja
					</div>
					<div class='col-md-3'>
						<b><?php echo $uker ?></b>
					</div>
				</div>
				<br />
				
				<div class="row">
					<div class='col-md-3'>
						Alamat
					</div>
					<div class='col-md-3'>
						<b><?php echo $almt ?></b>
					</div>
				</div>
				<br />

				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-danger-baru" onclick="Unitkerja.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />
<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Unit Kerja</u>
					</div>
				</div>
				<hr />

				<div class="row">
					<div class='col-md-3'>
						Unit Kerja
					</div>
					<div class='col-md-3'>
						<input <?php echo $hak_akses == 'superadmin' ? '' : 'disabled' ?> id="unit_kerja" class="form-control required" error="Unit Kerja" value="<?php echo isset($uker) ? $uker : '' ?>" />
					</div>
				</div>
				<br />
				
				<div class="row">
					<div class='col-md-3'>
						Alamat
					</div>
					<div class='col-md-3'>
						<input <?php echo $hak_akses == 'superadmin' ? '' : 'disabled' ?> id="alamat" class="form-control required" error="Alamat" value="<?php echo isset($almt) ? $almt : '' ?>" />
					</div>
				</div>
				<br />
				<hr />

				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-success" onclick="Unitkerja.simpan('<?php echo isset($id) ? $id : '' ?>')">Simpan</button>
						&nbsp;
						<button id="" class="btn btn-baru" onclick="Unitkerja.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

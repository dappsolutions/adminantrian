<?php

class Send_notif extends MX_Controller {

 public $segment;
 public $limit;
 public $page;
 public $last_no;

 public function __construct() {
  parent::__construct();
  $this->limit = 10;
 }

 public function getModuleName() {
  return 'send_notif';
 }

 public function getHeaderJSandCSS() {
  $data = array(
      '<script src="' . base_url() . 'assets/js/bootbox.js"></script>',
      '<script src="' . base_url() . 'assets/js/controllers/send_notif.js"></script>'
  );

  return $data;
 }

 public function getTableName() {
  return 'invoice';
 }

 public function index() {
  $this->segment = 3;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;

  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Faktur Reminder";
  $data['title_content'] = 'Data Faktur Reminder';
  $content = $this->getDataFaktur();
//  echo '<pre>';
//  print_r($content);die;
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  $data['jatuh_tempo'] = $this->getReminderJatuhTempo();

  echo Modules::run('template', $data);
 }

 public function getTotalDataFaktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('phr.no_invoice', $keyword),
   );
  }
  $total = Modules::run('database/count_all', array(
              'table' => $this->getTableName() . ' i',
              'field' => "i.no_faktur  
	, jp.jenis
	, i.tanggal_faktur
	, i.tanggal_bayar
	, isa.status
 , p.nama as customer",
              'join' => array(
                  array('jenis_pembayaran jp', 'jp.id = i.jenis_pembayaran'),
                  array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
                  array('invoice_status isa', 'isa.id = iss.id'),
                  array('pembeli p', 'p.id = i.pembeli'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "i.deleted = 0 and isa.status = 'DRAFT' and jp.jenis = 'KREDIT'",
  ));

  return $total;
 }

 public function getDataFaktur($keyword = '') {
  $like = array();
  if ($keyword != '') {
   $like = array(
       array('i.no_faktur', $keyword),
   );
  }
  $data = Modules::run('database/get', array(
              'table' => $this->getTableName() . ' i',
              'field' => "i.no_faktur  
	, jp.jenis
	, i.tanggal_faktur
	, i.tanggal_bayar
	, isa.status
 , p.nama as customer
 , p.no_hp
	, p.email
 , i.id
 , i.total",
              'join' => array(
                  array('jenis_pembayaran jp', 'jp.id = i.jenis_pembayaran'),
                  array('(select max(id) id, invoice from invoice_status group by invoice) iss', 'iss.invoice = i.id'),
                  array('invoice_status isa', 'isa.id = iss.id'),
                  array('pembeli p', 'p.id = i.pembeli'),
              ),
              'like' => $like,
              'is_or_like' => true,
              'where' => "i.deleted = 0 and isa.status = 'DRAFT' and jp.jenis = 'KREDIT'",
              'limit' => $this->limit,
              'offset' => $this->last_no,
  ));

  $result = array();
  if (!empty($data)) {
   foreach ($data->result_array() as $value) {
    array_push($result, $value);
   }
  }

  return array(
      'data' => $result,
      'total_rows' => $this->getTotalDataFaktur($keyword)
  );
 }

 public function getDataTotalJumlahReminder($pr) {
  $total = Modules::run('database/count_all', array(
              'table' => 'data_notifikasi_tagihan',
              'where' => array('pembayaran_product' => $pr)
  ));

  return $total;
 }

 public function getDataAngsuranTerakhir($pr) {
  $data = Modules::run('database/get', array(
              'table' => 'pembayaran_has_angsuran',
              'where' => array('pembayaran_product' => $pr),
              'orderby' => 'id desc',
              'limit' => 1
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function getDataPeriodeAngsuran($phr) {
  $data = Modules::run('database/get', array(
              'table' => 'pembeli_has_angsuran pha',
              'field' => array('pha.*', 'a.ansuran as total_angsuran'),
              'join' => array(
                  array('product_has_harga_angsuran rhha', 'pha.product_has_harga_angsuran = rhha.id'),
                  array('ansuran a', 'rhha.ansuran = a.id')
              ),
              'where' => array('pha.pembeli_has_product' => $phr)
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function getTotalAngsuran($id) {
  $total = Modules::run('database/count_all', array(
              'table' => 'pembayaran_has_angsuran',
              'where' => array('pembayaran_product' => $id)
  ));

  return $total;
 }

 public function getReminderJatuhTempo() {
  $data = Modules::run('database/get', array(
              'table' => 'notif_jatuh_tempo',
  ));

  $result = array();
  if (!empty($data)) {
   $result = $data->row_array();
  }


  return $result;
 }

 public function sendView($pr = '') {
  $data['pembayaran_product'] = $pr;
  $data['email'] = $this->input->post('email');
  $data['no_faktur'] = $this->input->post('no_faktur');
  $data['no_hp'] = str_replace('+', '', $this->input->post('no_hp'));
  $data['title_content'] = "Reminderikasi";

  echo $this->load->view('form_notifikasi', $data, true);
 }

 public function kirimReminder() {
  $data = json_decode($this->input->post('data'));
  $no_faktur = $this->input->post('no_faktur');
//  echo '<pre>';
//  print_r($no_faktur);die;
  $is_valid = false;
  $message = "";
  $this->db->trans_begin();
  try {
   $post_notif['message'] = $data->pesan;
   $post_notif['no_faktur'] = $no_faktur;
   $post_notif['jenis'] = 'FAKTUR';
   $post_notif['status'] = 1;
   Modules::run('database/_insert', 'data_notifikasi_tagihan', $post_notif);
   $this->db->trans_commit();
   $is_valid = true;

   $status_email = Modules::run("email/send_email", $data->email, $data->pesan);
   if ($status_email == "failed") {
    $message = "Email Gagal, Tidak Terkirim</br/>";
    $is_valid = false;
   }
//   $status_sms = Modules::run("sms/send_sms", $data->no_hp, $data->pesan);
//   if ($status_sms == 'failed') {
//    $message .= "Sms Gagal, Tidak Terkirim";
//    $is_valid = false;
//   }
  } catch (Exception $ex) {
   $this->db->trans_rollback();
  }

  echo json_encode(array('is_valid' => $is_valid, 'message' => $message));
 }

 public function search($keyword) {
  $this->segment = 4;
  $this->page = $this->uri->segment($this->segment) ?
          $this->uri->segment($this->segment) - 1 : 0;
  $this->last_no = $this->page * $this->limit;
  $keyword = urldecode($keyword);

  $data['keyword'] = $keyword;
  $data['view_file'] = 'index_view';
  $data['header_data'] = $this->getHeaderJSandCSS();
  $data['module'] = $this->getModuleName();
  $data['title'] = "Data Faktur";
  $data['title_content'] = 'Data Faktur';
  $content = $this->getDataFaktur($keyword);
  $data['content'] = $content['data'];
  $total_rows = $content['total_rows'];
  $data['pagination'] = Modules::run('pagination/get_pagination', $this->getModuleName() . '/index/', $this->segment, $total_rows, $this->limit, $this->last_no);
  echo Modules::run('template', $data);
 }

}

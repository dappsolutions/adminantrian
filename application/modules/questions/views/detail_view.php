<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Pertanyaan</u>
					</div>
				</div>
				<hr />

				<div class="row">
					<div class='col-md-3'>
						Pertanyaan
					</div>
					<div class='col-md-3'>
						<?php echo $pertanyaan ?>
					</div>
				</div>
				<br />

				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-danger-baru" onclick="Questions.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

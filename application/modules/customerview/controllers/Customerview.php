<?php
// ini_set("memory_limit","4096M");

class Customerview extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");

		//storage
		$this->load->model('storage/user');
		$this->load->model('storage/t_shutdown');
	}

	public function getModuleName()
	{
		return 'customerview';
	}

	public function getHeaderJSandCSS()
	{
		$data = array(
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/raphael/raphael.min.js"></script>',
			'<script src="' . base_url() . 'assets/admin_lte/bower_components/morris.js/morris.min.js"></script>',
			'<script src="' . base_url() . 'assets/js/controllers/customerview.js"></script>',
		);

		return $data;
	}

	public function index()
	{
		$data['view_file'] = 'v_index';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = 'Silakan Memilih Antrian Dengan Cara Menekan Tombol "TELLER" dan "CS"';
		$data['title_content'] = 'Daftar Antrian';
		$data['antrian_data'] = $this->getTotalAntrianMenunggu(5);
		// echo '<pre>';
		// print_r($data['summary_penjualan']);
		// die;
		$data['date_now'] = Modules::run('helper/getIndoDate', date('Y-m-d'), false);
		$data['openview'] = true;
		echo Modules::run('template', $data);
	}

	public function penilaianview()
	{
		$data['view_file'] = 'page_penilaian';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = 'Isi Kepuasan Pelanggan';
		$data['title_content'] = 'Isi Kepuasan Pelanggan';
		$data['openview'] = true;
		$data['pertanyaan'] = $this->getDataPertanyaan();
		$data['penilaian'] = $this->getListPenilaian();
		// echo '<pre>';
		// print_r($data);
		// die;
		echo Modules::run('template', $data);
	}

	public function getDataPertanyaan()
	{
		$data = Modules::run('database/get', array(
			'table' => 'questions_customer p',
			'where' => 'p.deleted = 0'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function getFormCustomer()
	{
		$data = $_POST;
		echo $this->load->view('form_customer', $data, true);
	}

	public function getListPenilaian()
	{
		$data = Modules::run('database/get', array(
			'table' => 'kategori_penilaian kp',
			'field' => array(
				'kp.*',
			),
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}


	public function getFormCustomerPenilaian()
	{
		$data = $_POST;
		$data['list_penilaian'] = $this->getListPenilaian();
		echo $this->load->view('form_penilaian', $data, true);
	}

	public function getPostDataInput($params)
	{
		$data['nama'] = $params['nama'];
		$data['no_ktp'] = $params['ktp'];
		$data['no_tlp'] = $params['no_tlp'];
		$data['alamat'] = $params['alamat'];
		$data['keperluan'] = $params['keperluan'];
		return $data;
	}

	public function getTotalAntrianMenunggu($limit = "1000")
	{
		$data = Modules::run('database/get', array(
			'table' => 'antrian a',
			'field' => array(
				'a.*',
				'c.nama',
				'ist.status'
			),
			'join' => array(
				array('customer c', 'c.id = a.customer'),
				array('(select max(id) id, antrian from antrian_status group by antrian) iss', 'iss.antrian = a.id'),
				array('antrian_status ist', 'ist.id = iss.id'),
			),
			'where' => "ist.status = 'WAIT' and a.deleted = 0",
			'limit' => $limit,
			'orderby' => 'a.id desc'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				if ($value['status'] == 'WAIT') {
					$value['status'] = 'MENUNGGU ANTRIAN';
				}
				array_push($result, $value);
			}
		}

		return $result;
	}

	public function cetakFormulirAntrian()
	{
		$no_antrian = $_GET['no_antrian'];
		$data['no_antrian'] = $no_antrian;
		$data['jenis'] = $_GET['jenis'];
		$this->generateBarcode($no_antrian);
		$data['total_antrian'] = count($this->getTotalAntrianMenunggu());
		echo $this->load->view('cetak_antrian', $data, true);
	}

	public function prosesSimpan()
	{
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);
		// die;
		$frontId = "";
		if (strtolower($data['jenis']) == 'teller') {
			$frontId = "A";
		} else {
			$frontId = "B";
		}

		$no_antrian = Modules::run('no_generator/generateNoAntrian', $frontId);
		$is_valid = false;
		$this->db->trans_begin();
		try {
			$push = array();
			$push = $this->getPostDataInput($data['data']);
			$customer = Modules::run('database/_insert', 'customer', $push);


			$push = array();
			$push['no_antrian'] = $no_antrian;
			$push['customer'] = $customer;
			$antrian = Modules::run('database/_insert', 'antrian', $push);

			$push = array();
			$push['antrian'] = $antrian;
			$push['status'] = 'WAIT';
			Modules::run('database/_insert', 'antrian_status', $push);
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array(
			'is_valid' => $is_valid,
			"no_antrian" => $no_antrian
		));
	}

	public function prosesSimpanPenilaian()
	{
		$data = $_POST;
		// echo '<pre>';
		// print_r($data);
		// die;
		$is_valid = false;
		$this->db->trans_begin();
		try {
			if (!empty($data['questions'])) {
				foreach ($data['questions'] as $key => $value) {
					$push = array();
					$push['antrian'] = $data['antrian'];
					$push['questions_customer'] = $value['questions'];
					$push['kategori_penilaian'] = $value['kategori'] == '' ? 4 : $value['kategori'];
					Modules::run('database/_insert', 'customer_nilai', $push);
				}
			}
			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array(
			'is_valid' => $is_valid,
		));
	}


	public function getListSukuBunga()
	{
		$data = Modules::run('database/get', array(
			'table' => 'suku_bunga a',
			'field' => array(
				'a.*'
			),
			'where' => "a.deleted = 0",
			'limit' => 8,
			'orderby' => 'a.id desc'
		));

		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		$content['data'] = $result;
		echo $this->load->view('list_suku_bunga', $content, true);
	}


	public function getDataAntrianProsesTeller($limit = "1")
	{
		$filter = 'A';
		$data = Modules::run('database/get', array(
			'table' => 'antrian a',
			'field' => array(
				'a.*',
				'c.nama',
				'ist.status'
			),
			'join' => array(
				array('customer c', 'c.id = a.customer'),
				array('(select max(id) id, antrian from antrian_status group by antrian) iss', 'iss.antrian = a.id'),
				array('antrian_status ist', 'ist.id = iss.id'),
			),
			'where' => "ist.status = 'WAIT' and a.deleted = 0",
			'like' => array(
				array('a.no_antrian', $filter)
			),
			'inside_brackets' => true,
			'limit' => $limit,
			'orderby' => 'a.id asc'
		));


		$result = array();
		$dilter_data = array();
		if (!empty($data)) {
			$result = $data->row_array();
		}

		echo json_encode($result);
	}

	public function getDataAntrianProsesCs($limit = "1")
	{
		$filter = 'B';
		$data = Modules::run('database/get', array(
			'table' => 'antrian a',
			'field' => array(
				'a.*',
				'c.nama',
				'ist.status'
			),
			'join' => array(
				array('customer c', 'c.id = a.customer'),
				array('(select max(id) id, antrian from antrian_status group by antrian) iss', 'iss.antrian = a.id'),
				array('antrian_status ist', 'ist.id = iss.id'),
			),
			'where' => "ist.status = 'WAIT' and a.deleted = 0",
			'like' => array(
				array('a.no_antrian', $filter)
			),
			'inside_brackets' => true,
			'limit' => $limit,
			'orderby' => 'a.id asc'
		));


		$result = array();
		$dilter_data = array();
		if (!empty($data)) {
			$result = $data->row_array();
		}

		echo json_encode($result);
	}

	public function generateBarcode($no_antrian)
	{
		$this->load->library('zend');
		$this->zend->load('Zend/Barcode');

		$image_resource = Zend_Barcode::factory('code128', 'image', array('text' => $no_antrian), array())->draw();
		$image_name     = $no_antrian . '.jpg';
		$image_dir      = './files/berkas/images/barcode/'; // penyimpanan file barcode
		imagejpeg($image_resource, $image_dir . $image_name);
	}

	public function getDataCustomer()
	{
		$no_antrian = $_POST['no_antrian'];
		$data = Modules::run('database/get', array(
			'table' => 'antrian a',
			'field' => array(
				'a.*',
				'c.nama',
				'ist.status',
				'c.no_ktp',
				'c.alamat',
				'c.no_tlp',
				'c.keperluan',
			),
			'join' => array(
				array('customer c', 'c.id = a.customer'),
				array('(select max(id) id, antrian from antrian_status group by antrian) iss', 'iss.antrian = a.id'),
				array('antrian_status ist', 'ist.id = iss.id'),
			),
			'where' => "a.deleted = 0 and a.no_antrian = '" . $no_antrian . "'",
			'inside_brackets' => true,
			'limit' => '1',
			'orderby' => 'a.id desc'
		));

		// echo '<pre>';
		// echo $this->db->last_query();
		// die;

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
		}

		echo json_encode($result);
	}

	public function show()
	{
		$data['view_file'] = 'pengaduancustomer';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = '-';
		$data['title_content'] = '-';
		echo $this->load->view('pengaduancustomer', $data, true);
	}

	public function getInfoUnitKerja($link = "SID"){
		$data = $this->db->from('t_data')->where('link', $link)->limit(1)->get();
		return $data->row_array();
	}

	public function getInfoDisplayActive(){
		$data = $this->db
		->select([
			'td.*',
			'ts.ket'
		])
		->from('t_display as td')
		->join('t_simpanan ts', 'ts.display_no = td.bungax')
		->where('td.published', 1)->get();
		return $data->row_array();
	}
	
	public function getInfoDetailBungaX($bunga = ""){
		$data = $this->db->from('t_simpanan')->where('display_no', $bunga)->get();
		return $data->result_array();
	}
	
	public function getInfoKurs($kurs = ""){
		$sql = "SELECT * FROM t_kurs
		where kurs_no = '".$kurs."'
		ORDER BY RAND()
		LIMIT 3";
		// $data = $this->db->from('t_kurs')->where('kurs_no', $kurs)->limit(3)->get();
		$data = $this->db->query($sql);
		return $data->result_array();
	}
	
	public function getInfoVideo(){
		$data = $this->db->from('t_video')->get();
		return $data->row_array();
	}
	
	public function getIntervalWaktuUpdate(){
		$data = $this->db->from('t_interval')->get();
		return $data->row_array();
	}

	public function getIntervalWaktuUpdateInterval(){
		$data = $this->db->from('t_interval_info')->get();
		return $data->row_array();
	}
	
	public function showKurs($tipe = 4, $link = "SID")
	{

		$data['unit_kerja'] = $this->getInfoUnitKerja($link);
		$data['display'] = $this->getInfoDisplayActive();
		$data['bungax'] = $this->getInfoDetailBungaX($data['display']['bungax']);
		$data['kurs'] = $this->getInfoKurs($data['display']['kursx']);
		$data['video'] = $this->getInfoVideo();
		$data['interval'] = $this->getIntervalWaktuUpdate();
		$data['interval_info'] = $this->getIntervalWaktuUpdateInterval();
		// echo '<pre>';
		// print_r($data);die;
		$data['view_file'] = 'kurst-first';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = '-';
		$data['title_content'] = '-';
		$view_file = 'kurs_first';
		$data['display_index'] = 2;
		switch ($tipe) {
			case '1':
				$view_file = 'kurs_first';
				break;
			case '2':
				$view_file = 'kurs_second';
				break;
			case '3':
				$view_file = 'kurs_third';
				break;
			case '4':
				// $view_file = 'kurs_fourth';
				$view_file = 'kurs_fourth_new';
				break;

			default:
				# code...
				break;
		}
		echo $this->load->view($view_file, $data, true);
	}


	public function getDataKurs()
	{

		require_once APPPATH . '/libraries/simple_html_dom.php'; //php 7		
		$base = 'https://eform.bri.co.id/info/kurs';
		// $base = 'https://google.com';

		echo phpinfo();die;

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_URL, $base);
		curl_setopt($curl, CURLOPT_REFERER, $base);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		$str = curl_exec($curl);
		curl_close($curl);

		// Create a DOM object
		$bri_data = new simple_html_dom();		
		// Load HTML from a string
		$bri_data->load($str);
		$link_data = $bri_data->find("table.cf");

		echo '<pre>';
		print_r($link_data);die;
	}


	public function forminput()
	{
		$data['view_file'] = 'form_pengaduan';
		$data['header_data'] = $this->getHeaderJSandCSS();
		$data['module'] = $this->getModuleName();
		$data['title'] = '-';
		$data['title_content'] = '-';
		$data['jenis'] = $_GET['jenis'];
		echo $this->load->view('form_pengaduan', $data, true);
	}

	public function simpanData()
	{
		$no_antrian = Modules::run('no_generator/generateNoAntrian');
		$data = $_POST;

		// echo '<pre>';
		// print_r($data);
		// die;
		$is_valid = false;
		$this->db->trans_begin();
		try {

			$push = array();
			$push['tgl'] = date('Y-m-d');
			$push['msk'] = date('H:i:s');
			$push['no_antri'] = $no_antrian;
			$push['loket'] = ucfirst($data['jenis']);
			$this->db->insert('t_antri', $push);

			//insert customer

			$push = array();
			$push['tgl'] = date('Y-m-d');
			$push['no_antri'] = $no_antrian;
			$push['loket'] = ucfirst($data['jenis']);
			$push['identitas'] = $data['identitas'];
			$push['no_identitas'] = $data['no_identitas'];
			$push['nama'] = $data['nama'];
			$push['alamat'] = $data['alamat'];
			$push['umur'] = $data['umur'];
			$push['tlp'] = $data['no_hp'];
			$push['jk'] = $data['jk'];
			$push['pendidikan'] = $data['pendidikan'];
			$push['pekerjaan'] = $data['pekerjaan'];
			$push['t1'] = 0;
			$push['t2'] = 0;
			$push['t3'] = 0;
			$push['t4'] = 0;
			$push['t5'] = 0;
			$push['t6'] = 0;
			$push['t7'] = 0;
			$push['t8'] = 0;
			$push['t9'] = 0;
			$this->db->insert('t_ikm', $push);


			$this->db->trans_commit();
			$is_valid = true;
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array(
			'is_valid' => $is_valid
		));
	}

	public function getChangeVideo(){
		$data['video'] = $this->getInfoVideo();	
		echo json_encode($data);
	}

	public function getSukuBungaDislpay($no = 1){
		$data = $this->db->from('t_simpanan as ts')
		->join('t_amandemen ta', 'ta.id = ts.t_amandemen', 'left')
		->where('ts.display_no', $no)->get();
		$result = array();
		if(!empty($data->result_array())){
			foreach ($data->result_array() as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}


	public function cekStatusShutdown(){
		$data = T_shutdown::all();
		$status = $data->toArray()[0]['matikan'];
		$id = $data->toArray()[0]['id'];
		return array(
			'status'=> $status,
			'id'=> $id
		);
	}

	public function getDataDetailSukuBunga(){
		$index = isset($_POST['display_index']) ? $_POST['display_index'] : 1;
		$data['bungax'] = $this->getSukuBungaDislpay($index);
		$data['display_index'] = $index;
		$data['title'] = strtoupper($data['bungax'][0]['ket']);
		$data['no_amandemen'] = strtoupper($data['bungax'][0]['no_amandemen']);
		$data['kurs'] = $this->getInfoKurs(1);
		$data['status_shut'] = $this->cekStatusShutdown()['status'];
		$data['status_shutid'] = $this->cekStatusShutdown()['id'];
		// echo '<pre>';
		// print_r($data);die;
		$view = $this->load->view('content_detail_sukubunga', $data, true);
		$view_kurs = $this->load->view('content_detail_kurs', $data, true);
		$view_head_sukubunga = $this->load->view('header_sukubunga', $data, true);
		echo json_encode(array(
			'data'=>$data,
			'view'=> $view,
			'view_kurs'=> $view_kurs,
			'view_head_sukubunga'=> $view_head_sukubunga,
			'display_index'=> $data['display_index'],
			'title'=> $data['title'],
			'no_amandemen'=> $data['no_amandemen'],
			'status_shut'=> $data['status_shut'],
			'status_shutid'=> $data['status_shutid']
		));
	}

	public function tes(){
		$data = User::all();
		echo '<pre>';
		print_r($data->toArray());
	}
}

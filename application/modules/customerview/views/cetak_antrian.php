<center>
	<table class="header_logo">
		<tbody>
			<tr>
				<td>
					<div class="div_label_logo">
						<img src="<?php echo base_url() ?>assets/images/logo_bri.png" alt="" width="120" height="80">
					</div>
				</td>
				<td class="right_align" style="font-weight: bold;">
					SELAMAT DATANG DI BRI <br> KCP NGUNUT
				</td>
			</tr>
		</tbody>
	</table>
	<table class="header_st">
		<thead>
			<tr>
				<td style="padding-top:5px;" colspan="6">
					<!-- FAKTUR PENJUALAN -->
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>
					<div class="">
						<span style="margin-left: 50px;"><?php echo date('D') ?>, <?php echo date('d-M-Y') ?> </span><span style="margin-left: 70px;"><?php echo date('H:i:s') ?></span>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div style="text-align: center;">
						<p>No. Antrian Anda</p>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div style="text-align: center;">
						<h2 style="font-size: 50px;"><?php echo $no_antrian ?></h2>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div style="text-align: center;">
						<h2><?php echo $jenis ?></h2>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div style="text-align: center;">
						Yang Sedang Menunggu : <?php echo $total_antrian ?> Org
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div style="text-align: center;">
						Melayani Dengan Setulus Hati
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="" style="text-align: center;">
					<img src="<?php echo base_url() ?>files/berkas/images/barcode/<?php echo $no_antrian ?>.jpg" />
				</td>
			</tr>
			<tr>
				<td>
					<div style="text-align: center;">
						<div style="text-align: center;">
							<center>
								<div class="barcode-code128" style="text-align: center;"></div>
							</center>
						</div>
					</div>
					</div>
				</td>
			</tr>
		</tbody>
	</table>
</center>
<center>
	<table>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	<table>
		<tr>
			<td>
				<p id="prnt" style="display:none;">
					<button type="button" class="btn btn-primary" onclick="window.print();">
						PRINT
					</button>
				</p>
			</td>
		</tr>

	</table>
</center>

<!-- <link rel='shortcut icon' href="{base_url('assets/images/wonokoyogroup.jpg')}"> -->

<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url() ?>assets/js/tools/barcodegenerator.js"></script>
<script src="<?php echo base_url() ?>assets/js/tools/jquery-barcode.js"></script>
<!-- <script src="<?php echo base_url() ?>assets/js/tools/barcodeupc.js"></script> -->
<script type="text/javascript">
	$(function() {
		// generateBarcode();
		// $(".barcode-code128").barcode(
		// 	"<?php echo $no_antrian ?>", // Value barcode (dependent on the type of barcode)
		// 	"code128" // type (string)
		// );
		$('#prnt').show();
		// setTimeout(function() {
		// 	window.print();
		// }, 1000);
	});
</script>
<style media="all">
	body {
		font-family: Verdana, Geneva, sans-serif;
	}

	div.page-break {
		/*padding: 10px;*/
		/*margin-left:-12px;*/
	}

	table.header_logo {
		width: 350px;
		border: 1px solid #ccc;
		padding: 6px;
	}

	table.header_logo td.td_img {
		width: 40px;
		padding-right: 10px;
	}

	table.header_logo td div.div_label_logo {
		font-weight: bold;
		font-size: 11px;
		padding: 1px;
	}

	table.header_st {
		/* border: 1px solid #000; */
		/* border-collapse: collapse; */
		width: 350px;
		font-size: 12px;
		margin-top: -20px;
		border: 1px solid #ccc;
		padding: 6px;
	}

	table.header_st td {
		border: 0px solid #000;
		/* border-collapse: collapse; */
		/*padding: 0px 5px;*/
		font-size: 12px;
	}

	table.header_st td ul {
		list-style-type: none;
		/*padding-left: 0px;*/
	}

	table.header_st td ul li span.left_label {
		width: 140px;
		float: left;
	}

	table.header_st td ul li span.center_label {
		width: 10px;
		float: left;
	}

	table.header_st td ul li span.right_label {
		width: 200px;
		float: left;
	}

	table.header_st thead td {
		font-weight: bold;
		text-align: center;
		padding-bottom: 10px;
		font-size: 14px;
	}

	table.header_st td.ttd_header {
		text-align: center;
		width: 15%;
	}

	table.header_st td.ttd_header-right {
		text-align: left;
		width: 15%;
	}

	table.header_st div.ttd {
		padding-top: 5px;
		padding-bottom: 35px;
	}

	table.header_st span.ttd_person {
		padding-left: 100px;
	}

	table.detail_formula {
		width: 100%;
		/*margin-top: 5px;*/
		font-size: 12px;
	}

	table.detail_formula th {
		padding: 2px 5px 2px 5px;
		border-top: 1px solid #000;
		border-bottom: 1px solid #000;
		font-size: 12px;
	}

	table.detail_formula thead th {
		font-weight: bold;
		text-align: center;
		/*padding: 5px;*/
		font-size: 12px;
	}

	table.detail_formula tbody td {
		font-size: 12px;
		border-right: 0 !important;
		border-left: 0 !important;
		border-bottom: 0 !important;
		border-top: 0 !important;
		border-style: hidden !important;
	}

	.center_align {
		text-align: center;
	}

	.left_align {
		text-align: left;
	}

	.right_align {
		text-align: right;
	}

	#btn_table {
		top: 45%;
		position: fixed;
		z-index: 999;
	}

	.btn-primary {
		background-color: #006DCC;
		background-image: linear-gradient(to bottom, #0088CC, #0044CC);
		background-repeat: repeat-x;
		border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
		color: #FFFFFF;
		text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.25);
	}

	.btn {
		-moz-border-bottom-colors: none;
		-moz-border-left-colors: none;
		-moz-border-right-colors: none;
		-moz-border-top-colors: none;
		border-image: none;
		border-radius: 4px;
		border-style: solid;
		border-width: 1px;
		box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
		cursor: pointer;
		display: inline-block;
		font-size: 14px;
		line-height: 20px;
		margin-bottom: 0;
		padding: 4px 12px;
		text-align: center;
		vertical-align: middle;
		font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
		font-weight: normal;
	}
</style>
<style media="screen">
</style>
<style media="print">
	body {
		font-family: Verdana, Geneva, sans-serif;
	}

	/* @media print{
		html, body{
			width:215mm;
			height: 100mm;
		}
	} */

	table.header_logo {
		width: 100%;
	}

	table.header_logo td.td_img {
		width: 40px;
		padding-right: 5px;
	}

	table.header_logo td div.div_label_logo {
		font-weight: bold;
		font-size: 11px;
		padding: 1px;
	}

	table.header_st {
		/* border: 1px solid #000;
		border-collapse: collapse; */
		width: 100%;
		font-size: 12px;
		margin-top: -20px;
		/*margin-left:-100px;*/
	}

	table.header_st td {
		/* border: 0px solid #000;
		border-collapse: collapse; */
		/*padding: 0px 5px;*/
		font-size: 12px;
	}

	table.header_st td ul {
		list-style-type: none;
		/*padding-left: 0px;*/
	}

	table.header_st td ul li span.left_label {
		width: 140px;
		float: left;
	}

	table.header_st td ul li span.center_label {
		width: 10px;
		float: left;
	}

	table.header_st td ul li span.right_label {
		width: 200px;
		float: left;
	}

	table.header_st thead td {
		font-weight: bold;
		text-align: center;
		padding-bottom: 10px;
		font-size: 12px;
	}

	table.header_st td.ttd_header {
		text-align: center;
		width: 15%;
	}

	table.header_st td.ttd_header-right {
		text-align: left;
		width: 25%;
	}

	table.header_st div.ttd {
		padding-top: 5px;
		padding-bottom: 35px;
	}

	table.header_st span.ttd_person {
		padding-left: 50px;
	}

	table.detail_formula {
		width: 100%;
		margin-top: 5px;
		font-size: 12px;
	}

	table.detail_formula th {
		padding: 2px 5px 2px 5px;
		font-size: 12px;
	}

	table.detail_formula thead th {
		font-weight: bold;
		text-align: center;
		border-top: 1px solid #000;
		border-bottom: 1px solid #000;
		padding: 5px;
		font-size: 12px;
	}

	table.detail_formula tbody td {
		font-size: 10px;
		/*font-weight: bold;*/
		border-right: 0 !important;
		border-left: 0 !important;
		border-bottom: 0 !important;
		border-top: 0 !important;
		border-style: hidden !important;
	}

	.center_align {
		text-align: center;
	}

	.left_align {
		text-align: left;
	}

	.right_align {
		text-align: right;
	}

	#balik {
		display: none;
	}

	#save {
		display: none;
	}

	#prnt {
		display: none;
	}

	.barcodeTarget {
		-moz-transform: rotate(-90.0deg);
		-o-transform: rotate(-90.0deg);
		-webkit-transform: rotate(-90.0deg);
		-filter: progid: DXImageTransform.Microsoft.BasicImage(0.083);
		-ms-filter: "progid: DXImageTransform.Microsoft.BasicImage(0.083)";
		transform: rotate(-90.0deg);
	}

	@page {
		size: auto;
		margin: 0;
	}
</style>

<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />

<div class="content">
	<div class="animated fadeIn">
		<div class="padding-16">
			<div class="">
				<h4>Silakan Isi Form Berikut, untuk proses selanjutnya..</h4>
				<hr>

				<div class="row">
					<div class='col-md-6 text-bold'>
						Jenis Pengajuan
					</div>
					<div class='col-md-6'>
						<input type='text' readonly name='' id='jenis' class='form-control' value='<?php echo isset($jenis) ? $jenis : '' ?>' />
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						Nama
					</div>
					<div class='col-md-6'>
						<input type='text' name='' id='nama' class='form-control required' value='<?php echo isset($nama) ? $nama : '' ?>' error="Nama" placeholder="Nama" />
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						KTP (Kartu Indentitas)
					</div>
					<div class='col-md-6'>
						<input type='text' name='' id='ktp' class='form-control required' value='<?php echo isset($ktp) ? $ktp : '' ?>' error="KTP" placeholder="KTP (Kartu Indentitas)" />
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						No. Telp
					</div>
					<div class='col-md-6'>
						<input type='text' name='' id='no_tlp' class='form-control required' value='<?php echo isset($no_tlp) ? $no_tlp : '' ?>' error="No Telp" placeholder="No. Telp" />
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						Alamat
					</div>
					<div class='col-md-6'>
						<textarea id="alamat" class="form-control required" error="Alamat"></textarea>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						Keperluan
					</div>
					<div class='col-md-6'>
						<textarea id="keperluan" class="form-control required" error="Keperluan"></textarea>
					</div>
				</div>
				<br />

				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-success" onclick="CustomerView.prosesSimpan(this)">Proses</button>
						&nbsp;
						<button id="" class="btn btn-danger" onclick="CustomerView.batal()">Batal</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

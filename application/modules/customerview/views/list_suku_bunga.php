<h3 class="heading mb-sm-5 mb-4">Suku Bunga <strong>Simpanan</strong></h3>
<hr>

<?php if (!empty($data)) { ?>
	<?php foreach ($data as $key => $value) { ?>
		<div class="row">
			<div class="col-md-8">
				<a href="#" class="btn btn-banner1 my-3"><?php echo $value['keterangan'] ?></a>
			</div>
			<div class="col-md-4">
				<a href="#" class="btn btn-banner1 my-3"><?php echo $value['persentase'] ?> %</a>
			</div>
		</div>
	<?php } ?>
<?php } ?>
<hr>

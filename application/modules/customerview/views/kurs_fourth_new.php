<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="UTF-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0">
 <title>KURS SUKU BUNGA</title>
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">

 <style>
  /* html{
    height: 100%;
    box-sizing: border-box;
  }


  *,
  *:before,
  *:after {
    box-sizing: inherit;
  } */

  body{
   background-color: #00529c;
   color:white;
   /* margin:0;
   position: relative; */
  }

  .container-data{
   display: flex;
   flex-direction : column;
   align-items: stretch;
   flex-wrap: wrap;
   min-height: 100vh;
  }

  #logo-header{
    display:flex;
    flex-direction: row;
    margin:0 auto;
  }

  .title-customer{
   /* flex-grow: 10; */
   /* font-stretch: expanded; */
   letter-spacing: 2px;
   font-size:42px;
   text-align:center;
   margin-top:-10px;
   font-weight:bold;
  }
 
  .title-customer-dua{
   /* flex-grow: 10; */
   /* font-stretch: expanded; */
   letter-spacing: 2px;
   font-size:58px;
   font-weight:bold;
   text-align:center;
   margin-top:-10px;
  }

  .content-baris{
   margin-top:-14px;
  }
  .baris{
    background-color: white;
  }

  .video-content{
   border: 4px solid white;
   border-radius: 3px;
   height: 20%;
   margin-left: 8px;
   margin-right: 8px;
   position:relative;
   margin-top:-5px;
  }

  #video{
   width: 100%;height: 428px;
  }

  .ket_title{
   font-size:42px;
   align-items:stretch;
   text-align:center;
   background-color: white;
   border-bottom: 1px solid #ccc;
   color:#00529c;
   margin-top:2px;
   font-weight:bold;   
   border-radius:3px;
  }
 
  .ket_title_kurs{
   font-size:42px;
   align-items:stretch;
   text-align:center;
   background-color: white;
   border-bottom: 1px solid #ccc;
   color:#00529c;
   margin-top:1px;
   font-weight:bold;
   border-radius:3px;
  }

  .content-info-bunga{
    display:flex;
    margin-top:3px;
    margin-top:-3px;
    flex-direction:row;    
  }

  .content-info-bunga > div{
   font-size: 33px;
   color:#00529c;
   padding:3px;
   background-color: white;
   font-weight:bold;
  }

  .content-list-bunga{
   display:flex;
   flex-direction: row;
   border-bottom: 1px solid #ccc;
   justify-content: space-around;
  }

  .content-list-bunga{
   margin-top:3px;
   font-size:33px;
   padding:3px;
   font-weight:bold;
  }


  .content-info-kurs{
   font-size:33px;
   color:#00529c;
   display:flex;
   flex-direction:row;
   margin-top:-6px;
   font-weight:bold;
   background-color: white;
  }
 
  .content-kurs{
   display:flex;
   flex-direction: column;
   border-bottom: 1px solid #ccc;
  }

  .content-list-kurs{
   display:flex;
   flex-direction:row;
  }

  .content-list-kurs > div{
   font-size:33px;
   font-weight:bold;
  }

  .content-data-footer{
   display:flex;
   flex-direction:row;
  }

  .content-data-footer > div{
   font-size:30px;
  }

  .content-footer{
    right: 0;
    bottom: 0;
    left: 0;
    margin:0;
  }

  #img-footer{
    width:100%;
  }
 </style>
</head>
<body>
 <div class="container-data">
 <div id="logo-header">
  <div class="img-header" style="flex-grow:4;align-self:flex-start;">
    <img src="<?php echo base_url() ?>assets/images/BRI.png" width="75" height="75" alt="">
  </div>
  <div class="title-customer" style="flex-grow:6;">
   <?php echo strtoupper($unit_kerja['uker']) ?>
   <br>
   <?php echo strtoupper($unit_kerja['almt']) ?>    
  </div>
 </div>
  <div class="content-baris">
   <hr class="baris">
  </div>
  <div class="title-customer-dua">
   SUKU BUNGA SIMPANAN
  </div>

  <div class="video-content">
   <video autoplay id="video" src="<?php echo base_url() ?>files/berkas/video/<?php echo $video['video'] ?>" width="800" height="600" loop muted  style="">
   Your browser does not support the video tag
   </video>
  </div>

  <div style="padding:6px;">
    <p id="no-surat">No Surat : -</p>
  </div>

  <div class="ket_title">
   <?php echo $display['ket'] ?>
  </div>
  <div class="content-info-bunga">
   <div style="flex-grow:5;text-align:center;">
     NOMINAL 
   </div>
   <div style="flex-grow:5;text-align:center;">
    % / TAHUN 
   </div>
  </div>

  <div class="content-detail-bunga">
   <?php if(!empty($bungax)){ ?>
     <?php foreach ($bungax as $key => $value) {?>
				  <div class="content-list-bunga">
        <div style="flex-grow:5;">
         <?php echo $value['nama'] ?>
        </div>
        <div style="flex-grow:5;text-align:right;">
         <?php echo $value['bunga'] ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <hr style="background-color: white;">
      </div>
     <?php } ?>
			<?php } ?>
  </div>


  <div class="ket_title_kurs">
   KURS
  </div>
  <div class="content-info-kurs">
    <div style="flex-grow:3;text-align:center;">
      MATA UANG 
    </div>
    <div style="flex-grow:4;text-align:center;">
      JUAL 
    </div>
    <div style="flex-grow:3;text-align:center;">
     BELI
    </div>
  </div>

  <div class="content-kurs">
    <?php if(!empty($kurs)){ ?>
				<?php foreach ($kurs as $key => $value) {?>
						<div class="content-list-kurs">
						<div style="flex-grow:2;text-align:left;">
							<?php echo $value['negara'] ?>
						</div>
						<div style="flex-grow:4;text-align:right;">
							<?php echo str_replace('.', ',', $value['jual']) ?>
						</div>
						<div style="flex-grow:4;text-align:right;">
							<?php echo str_replace('.', ',', $value['beli']) ?>
						</div>
					</div>
					<hr style="background-color: white;">
				<?php } ?>
			<?php } ?>
  </div>

 </div>

 <footer class="page-footer">
  <div class="footer-copyright">
    <!-- <div style="padding: 16px;color:white;"> -->
    <!-- <div class="content-data-footer">
        <div style="flex-grow:5;">
        <label for="" id="jam"></label>
        <label for="" id="">:</label>
        <label for="" id="menit"></label>
        <label for="" id="">:</label>
        <label for="" id="detik"></label>
        </div>
        <div style="flex-grow:5;text-align:right;">
          <?php echo date('d F Y') ?></h3>
        </div>
    </div> -->
    <img id="img-footer" src="<?php echo base_url() ?>assets/images/footer_bri_dua.png" alt="">
  <!-- </div> -->
  </div>
  </footer>
 <input type="hidden" id="display_index" value="<?php echo $display_index ?>">

 <script src="<?php echo base_url() ?>assets/js/url.js"></script>
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>


 <script>
 function waktu() {
			var waktu = new Date();
			// setTimeout("waktu()", 1000);
			// document.getElementById("jam").innerHTML = waktu.getHours();
			// document.getElementById("menit").innerHTML = waktu.getMinutes();
			// document.getElementById("detik").innerHTML = waktu.getSeconds();
		}
		

		function setAnimate(){
			// $(`.title-customer`).fadeOut('slow');
			// $(`.title-customer`).fadeIn('slow');
   
			// $(`.title-customer-dua`).fadeOut('slow');
			// $(`.title-customer-dua`).fadeIn('slow');

			// $(`.ket_title`).fadeOut('slow');
			// $(`.ket_title`).fadeIn('slow');

			$(`.content-detail-bunga`).fadeOut('slow');
			$(`.content-detail-bunga`).fadeIn('slow');


			$(`div.content-title`).fadeOut('slow');
			$(`div.content-title`).fadeIn('slow');
			$(`div.content-kurs`).fadeOut('slow');
			$(`div.content-kurs`).fadeIn('slow');
		}

  $(function(){
   var video = $('#video');
			window.setTimeout("waktu()", 2000);
			video[0].muted = false;


   setAnimate();


   //interval kalo
			window.setInterval(function() {
				//sd
          
					$.ajax({
						type: 'POST',
						// data: params,
						dataType: 'json',
						url: url.base_url("customerview") + "getChangeVideo",
						error: function () {
							toastr.error("Program Error");
						},

						beforeSend: function () {
							// message.loadingProses("Proses Pengambilan Formulir...");
						},

						success: function (resp) {
							//add
								var video = $('#video');
								let link_url = `<?php echo base_url() ?>files/berkas/video/${resp.video.video}`;
								// console.log(link_url);
								video.attr('src', link_url);
						}
					});
			}, <?php echo $interval['waktu'] ?>);



      window.setInterval(() => {
        let params = {};
        params.display_index = $(`input#display_index`).val();
        // console.log('display', );
        $.ajax({
						type: 'POST',
						data: params,
						dataType: 'json',
						url: url.base_url("customerview") + "getDataDetailSukuBunga",
						error: function () {
							toastr.error("Program Error");
						},

						beforeSend: function () {
							// message.loadingProses("Proses Pengambilan Formulir...");
						},

						success: function (resp) {
							//add
              // console.log(resp.data);sdasd
              // console.log();
              $(`div.content-detail-bunga`).html(resp.view);
              // console.log('tes', resp);
              let display = 2;
              if(resp.display_index == 2){
                display = 3;
              }
              if(resp.display_index == 3){
                display = 4;
              }
              if(resp.display_index == 4){
                display = 1;
              }
              if(resp.display_index == 1){
                display = 2;
              }
              $(`input#display_index`).val(display);
              $(`div.ket_title`).html(resp.title);
              $(`div.content-kurs`).html(resp.view_kurs);
              $(`div.content-info-bunga`).html(resp.view_head_sukubunga);
              $(`p#no-surat`).text(`No Surat : ${resp.no_amandemen}`);
              setAnimate();

              if(resp.status_shut == '0'){
                 //hidupkan
                  $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    url: url.base_url('shutdown') + "hidupkan/" + resp.status_shutid,
                    error: function () {
                      toastr.error("Program Error");
                    },

                    beforeSend: function () {
                      // message.loadingProses("Proses Pengambilan Formulir...");
                    },

                    success: function (resp) {
                      <?php //exec ('shutdown -s -t 0') ?>
                      window.location.href = url.base_url('shutdown') + "matikanpc";
                    }
                  });
              }
						}
					});
      }, <?php echo $interval_info['interval'] ?>);
  });
 </script>
</body>
</html>
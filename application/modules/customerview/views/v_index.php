<!-- <div class="row">
	<div class="col-md-12">
		<h4>Silakan Memilih Antrian Dengan Cara Menekan Tombol "TELLER" dan "CS"</h4>
	</div>
</div> -->
<div class="row" style="margin-left: 3px;margin-right: 3px;">
	<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-red"><i jenis="TELLER" onclick="CustomerView.getFormCustomer(this)" class="fa fa-user"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">TELLER</span>
				<span class="info-box-number">0</span>
			</div>
			<!-- /.info-box-content -->
		</div>
		<!-- /.info-box -->
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-yellow"><i jenis="CUSTOMER SERVICE" onclick="CustomerView.getFormCustomer(this)" class="fa fa-user"></i></span>

			<div class="info-box-content">
				<span class="info-box-text">CS</span>
				<span class="info-box-number">0</span>
			</div>
			<!-- /.info-box-content -->
		</div>
		<!-- /.info-box -->
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12">
		<div class="info-box">
			<span class="info-box-icon bg-blue">
				<!-- <i jenis="PENILAIAN" onclick="CustomerView.getFormCustomerPenilaian(this)" class="fa fa-star"></i> -->
				<i jenis="PENILAIAN" onclick="CustomerView.gotoPagePenilaian(this)" class="fa fa-star"></i>
			</span>

			<div class="info-box-content">
				<span class="info-box-text">PENILAIAN</span>
				<span class="info-box-number">0</span>
			</div>
			<!-- /.info-box-content -->
		</div>
		<!-- /.info-box -->
	</div>
</div>
<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-header with-border">
				<div class="row">
					<div class="col-md-10">
						<div class="box-title">
							<i class="mdi mdi-chart-bar mdi-18px"></i><strong class="card-title"><?php echo isset($title_content) ? $title_content : '' ?></strong>
						</div>
					</div>
					<div class="col-sm-2 text-right"></div>
				</div>
			</div>
			<div class="box-body">
				<div class='row'>
					<div class='col-md-12'>
						<h4><u>Top 5 Antrian</u></h4>
						<br />
						<div class='table-responsive'>
							<table class="table table-bordered">
								<thead>
									<tr class="bg-primary">
										<th>No</th>
										<th>No Antrian</th>
										<th>Nama Pelanggan</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($antrian_data)) { ?>
										<?php $no = 1; ?>
										<?php foreach ($antrian_data as $value) { ?>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $value['no_antrian'] ?></td>
												<td><?php echo $value['nama'] ?></td>
												<td class="bg-danger"><?php echo $value['status'] ?></td>
											</tr>
										<?php } ?>
									<?php } else { ?>
										<tr>
											<td colspan="6" class="text-center">Tidak ada data ditemukan</td>
										</tr>
									<?php } ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

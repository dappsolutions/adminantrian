<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>POLSEK PENGADUAN DAN PELAYANAN</title>
	<!-- Tell the browser to be responsive to screen width -->
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>assets/images/logo_polda.png" />
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/dist/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/square/blue.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main_app.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
</head>

<body class="hold-transition login-page" style="background-color: #259dd4;">
	<div class="container">
		<br>
		<div class="row">
			<div class="col-md-3 text-right">
				<img src="<?php echo base_url() ?>assets/images/logo_polri.png" alt="" height="100" width="150">
			</div>
			<div class="col-md-6 text-center">
				<h3 style="color:yellow;font-weight:bold;">SENTRA PENGADUAN PELAYANAN TERPADU</h3>
				<h1 style="color:white;font-weight: bold;">POLRES TRENGGALEK</h1>
			</div>
			<div class="col-md-3 text-left">
				<img src="<?php echo base_url() ?>assets/images/logo_polda.png" alt="" height="100" width="200">
			</div>
		</div>
		<br>
		<br>
		<div class="row">
			<div class="col-md-12 text-center">
				<h3 style="color:yellow;font-weight: bold;">SILAKAN MEMILIH ANTRIAN</h3>
			</div>
		</div>
		<br>
		<div class="box padding-16">
			<div class="box-body">
				<div class="row">
					<div class="col-md-9 text-center">
						<div class="row">
							<div class="col-md-6 text-left">
								<button class="btn btn-danger" jenis="pengaduan" onclick="gotoForm(this)">
									<h1>PENGADUAN</h1>
								</button>
								<br>
								<br>
								<div style="margin-left: 12px;">
									<table>
										<tr>
											<th>- Pengaduan</th>
										</tr>
										<tr>
											<th>- Penganiayaan</th>
										</tr>
										<tr>
											<th>- Penipuan</th>
										</tr>
										<tr>
											<th>- Perzinahan</th>
										</tr>
										<tr>
											<th>- Perbuatan Cabul</th>
										</tr>
										<tr>
											<th>- Tindak Pidana Lainnya</th>
										</tr>
									</table>
								</div>
							</div>
							<div class="col-md-6 text-left">
								<button class="btn btn-danger" jenis="pelayanan" onclick="gotoForm(this)">
									<h1>PELAYANAN</h1>
								</button>
								<br>
								<br>
								<div style="margin-left: 12px;">
									<table>
										<tr>
											<th>- Laporan Kehilangan Benda</th>
										</tr>
										<tr>
											<th>- Kehilangan SIM/KTP/ATM</th>
										</tr>
										<tr>
											<th>- Kehilangan Surat Tanah/Surat Nikah</th>
										</tr>
										<tr>
											<th>- Kehilangan BPKB/STNK/Ijazah</th>
										</tr>
										<tr>
											<th>- Laporan Orang Hilang</th>
										</tr>
										<tr>
											<th>- Laporan Kehilangan Lainnya</th>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-3 text-left">
						<img src="<?php echo base_url() ?>assets/images/logo_polisi.png" alt="" width="200" height="200">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<marquee behavior="" direction="">
					<h1 style="color: yellow;font-weight: bold;">KEPOLISIAN TERPADU TRENGGALEK</h1>
				</marquee>
				<div style="margin-top: -12px;text-align: right;">
					<h1 style="text-shadow: 2px 2px #dd4b39;color:white;">
						<label for="" id="jam"></label>:<label for="" id="menit"></label>:<label for="" id="detik"></label>
					</h1>
				</div>
				<br>
			</div>
		</div>
		</section>
	</div>

	<!-- jQuery 3 -->
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/validation.js"></script>
	<script src="<?php echo base_url() ?>assets/js/url.js"></script>
	<script src="<?php echo base_url() ?>assets/js/message.js"></script>
	<script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
	<script>
		function waktu() {
			var waktu = new Date();
			setTimeout("waktu()", 1000);
			document.getElementById("jam").innerHTML = waktu.getHours();
			document.getElementById("menit").innerHTML = waktu.getMinutes();
			document.getElementById("detik").innerHTML = waktu.getSeconds();
		}

		function gotoForm(elm) {
			var jenis = $(elm).attr('jenis');
			window.location.href = url.base_url("customerview") + "forminput?jenis=" + jenis;
		}

		$(function() {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' /* optional */
			});


			window.setTimeout("waktu()", 1000);
		});
	</script>
</body>

</html>

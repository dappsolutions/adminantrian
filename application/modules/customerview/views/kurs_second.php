<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Kurs</title>


	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">
</head>

<body style="background-color: #00529c;">
	<div style="top: 0px;">
		<div class="content-header" style="color: white;">
			<div style="padding: 16px;top:0px;">
				<div class="row">
					<div class="col-md-2 text-right" style="">
						&nbsp;
						<!-- <img src="<?php echo base_url() ?>assets/images/logo-bri.png" alt="logo-bri" width="100" height="80"> -->
					</div>
					<div class="col-md-8 text-center">
						<h1 style="font-weight: bold;">KANTOR CABANG BRI SIDOARJO</h1>
						<h2 style="font-weight: bold;">Jl. A Yani 105 - Sidoarjo</h2>
						<hr>
					</div>
					<div class="col-md-2 text-left">
						&nbsp;
						<!-- <img src="<?php echo base_url() ?>assets/images/logo-bri.png" alt="logo-bri" width="100" height="80"> -->
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<h2 style="font-weight: bold;">SUKU BUNGA SIMPANAN</h2>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="content-title" style="background-color: white;border-bottom: 1px solid #ccc;color:#00529c">
			<div class="row">
				<div class="col-md-12 text-center">
					<h1 style="font-weight: bold;">BRITAMA</h1>
				</div>
			</div>
		</div>
		<div class="content-title" style="background-color: white;color: #00529c;">
			<div class="row">
				<div class="col-md-7 text-center">
					<h2 style="font-weight: bold;">NOMINAL</h2>
				</div>
				<div class="col-md-5 text-center">
					<h2 style="font-weight: bold;">% / Tahun</h2>
				</div>
			</div>
		</div>
		<br>
		<div class="content-kurs" style="padding: 16px;color: white;">
			<div class="row">
				<div class="col-md-7 text-left">
					<h2>0 s/d 500.000</h2>
				</div>
				<div class="col-md-5 text-center">
					<h2>1.25</h2>
				</div>
			</div>
			<hr style="background-color: white;">
			<div class="row">
				<div class="col-md-7 text-left">
					<h2>500.000 s/d 5JT</h2>
				</div>
				<div class="col-md-5 text-center">
					<h2>1.25</h2>
				</div>
			</div>
			<hr style="background-color: white;">
			<div class="row">
				<div class="col-md-7 text-left">
					<h2>> 5JT s/d 50JT</h2>
				</div>
				<div class="col-md-5 text-center">
					<h2>1.25</h2>
				</div>
			</div>
			<hr style="background-color: white;">
			<div class="row">
				<div class="col-md-7 text-left">
					<h2>> 50JT s/d 100JT</h2>
				</div>
				<div class="col-md-5 text-center">
					<h2>1.25</h2>
				</div>
			</div>
			<hr style="background-color: white;">
			<div class="row">
				<div class="col-md-7 text-left">
					<h2>> 100JT s/d 1M</h2>
				</div>
				<div class="col-md-5 text-center">
					<h2>1.25</h2>
				</div>
			</div>
			<hr style="background-color: white;">
			<div class="row">
				<div class="col-md-7 text-left">
					<h2>> 1 Milyar</h2>
				</div>
				<div class="col-md-5 text-center">
					<h2>1.25</h2>
				</div>
			</div>
		</div>
		<br>
		<div class="content-title" style="background-color: white;border-bottom: 1px solid #ccc;color: #00529c;">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2 style="font-weight: bold;">KURS</h2>
				</div>
			</div>
		</div>
		<div class="content-title" style="background-color: white;color: #00529c;">
			<div class="row">
				<div class="col-md-7 text-center">
					<h2 style="font-weight: bold;">JUAL</h2>
				</div>
				<div class="col-md-5 text-center">
					<h2 style="font-weight: bold;">BELI</h2>
				</div>
			</div>
		</div>
		<br>
		<div class="content-kurs" style="padding: 16px;color: white;">
			<div class="row">
				<div class="col-md-4 text-left">
					<h2>EUR</h2>
				</div>
				<div class="col-md-4 text-center">
					<h2>16,289.55</h2>
				</div>
				<div class="col-md-4 text-center">
					<h2>15,65.10</h2>
				</div>
			</div>
			<hr style="background-color: white;">
			<div class="row">
				<div class="col-md-4 text-left">
					<h2>USD</h2>
				</div>
				<div class="col-md-4 text-center">
					<h2>14,4558.00</h2>
				</div>
				<div class="col-md-4 text-center">
					<h2>13,945.00</h2>
				</div>
			</div>
			<hr style="background-color: white;">
			<div class="row">
				<div class="col-md-4 text-left">
					<h2>SGD</h2>
				</div>
				<div class="col-md-4 text-center">
					<h2>10,434.07</h2>
				</div>
				<div class="col-md-4 text-center">
					<h2>9,952.12</h2>
				</div>
			</div>
		</div>
	</div>
	<footer>
		<div class="content-footer" style="background-color: #fc7614;bottom: 0px;">
			<div style="padding: 16px;color:white;">
				<div class="row">
					<div class="col-md-3">
						<h3>
							<label for="" id="jam"></label>
							<label for="" id="">:</label>
							<label for="" id="menit"></label>
							<label for="" id="">:</label>
							<label for="" id="detik"></label>
						</h3>
					</div>
					<div class="col-md-9 text-right">
						<h3><?php echo date('d F Y') ?></h3>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>


	<script>
		function waktu() {
			var waktu = new Date();
			setTimeout("waktu()", 1000);
			document.getElementById("jam").innerHTML = waktu.getHours();
			document.getElementById("menit").innerHTML = waktu.getMinutes();
			document.getElementById("detik").innerHTML = waktu.getSeconds();
		}

		$(function() {
			window.setTimeout("waktu()", 1000);
			setTimeout(function() {
				window.location.href = "3"
			}, 10000);
		});
	</script>
</body>

</html>

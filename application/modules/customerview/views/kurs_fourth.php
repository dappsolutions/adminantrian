<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Kurs</title>


	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/lib/animate/animate.min.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/bootstrap.css">


	<style>
		body{
			background-color: #00529c;
		}

		#title_sukubunga{
			font-size: 50px;	
		}

		.title-unitkerja{
			font-size: 40px;
			font-weight: bold;
			display:none;
		}

	</style>
</head>

<body style="">
	<div style="top: 0px;">
		<div class="content-header" style="color: white;">
			<div style="padding: 16px;top:0px;">
				<div class="row">
					<div class="col-md-12 text-center">
						<label style="" class="title-unitkerja"><?php echo strtoupper($unit_kerja['uker']) ?></label>
						<br>
						<label style="" class="title-unitkerja"><?php echo strtoupper($unit_kerja['almt']) ?></label>
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<label for="" id="title_sukubunga">SUKU BUNGA SIMPANAN</label>
					</div>
				</div>
			</div>
		</div>
		

		<div class="content-video" style="border: 4px solid white;border-radius: 3px;height: 20%;margin-left: 8px;margin-right: 8px;position:relative;">
			<video autoplay id="video" src="<?php echo base_url() ?>files/berkas/video/<?php echo $video['video'] ?>" width="800" height="600" loop muted  style="width: 100%;height: 500px;">
				Your browser does not support the video tag.
			</video>
			<!-- <button onclick="playVideo(this)">PLAY</button> -->
		</div>
		<br>
		<div class="content-title" style="background-color: white;border-bottom: 1px solid #ccc;color:#00529c">
			<div class="row">
				<div class="col-md-12 text-center">
					<h1 style="font-weight: bold;"><?php echo $display['ket'] ?></h1>
				</div>
			</div>
		</div>
		<div class="content-title" style="background-color: white;color: #00529c;">
			<div class="row">
				<div class="col-md-7 text-center">
					<h2 style="font-weight: bold;">NOMINAL</h2>
				</div>
				<div class="col-md-5 text-center">
					<h2 style="font-weight: bold;">% / Tahun</h2>
				</div>
			</div>
		</div>
		<br>
		<div class="content-kurs" style="padding: 16px;color: white;">
			<?php if(!empty($bungax)){ ?>
				<?php foreach ($bungax as $key => $value) {?>
					<div class="row">
						<div class="col-md-7 text-left wow fadeInUp" data-wow-delay="0.6s">
							<h2><?php echo $value['nama'] ?></h2>
						</div>
						<div class="col-md-5 text-center">
							<h2><?php echo $value['bunga'] ?></h2>
						</div>
					</div>
					<hr style="background-color: white;">
				<?php } ?>
			<?php } ?>
		</div>
		<br>
		<div class="content-title" style="background-color: white;border-bottom: 1px solid #ccc;color: #00529c;">
			<div class="row">
				<div class="col-md-12 text-center">
					<h2 style="font-weight: bold;">KURS</h2>
				</div>
			</div>
		</div>
		<div class="content-title" style="background-color: white;color: #00529c;">
			<div class="row">
				<div class="col-md-7 text-center">
					<h2 style="font-weight: bold;">JUAL</h2>
				</div>
				<div class="col-md-5 text-center">
					<h2 style="font-weight: bold;">BELI</h2>
				</div>
			</div>
		</div>
		<br>
		<div class="content-kurs" style="padding: 16px;color: white;">
			<?php if(!empty($kurs)){ ?>
				<?php foreach ($kurs as $key => $value) {?>
						<div class="row">
						<div class="col-md-4 text-left">
							<h2><?php echo $value['negara'] ?></h2>
						</div>
						<div class="col-md-4 text-center">
							<h2><?php echo str_replace('.', ',', $value['jual']) ?></h2>
						</div>
						<div class="col-md-4 text-center">
							<h2><?php echo str_replace('.', ',', $value['beli']) ?></h2>
						</div>
					</div>
					<hr style="background-color: white;">
				<?php } ?>
			<?php } ?>	
		</div>
	</div>
	<footer>
		<div class="content-footer" style="background-color: #fc7614;bottom: 0px;">
			<div style="padding: 16px;color:white;">
				<div class="row">
					<div class="col-md-3">
						<h3>
							<label for="" id="jam"></label>
							<label for="" id="">:</label>
							<label for="" id="menit"></label>
							<label for="" id="">:</label>
							<label for="" id="detik"></label>
						</h3>
					</div>
					<div class="col-md-9 text-right">
						<h3><?php echo date('d F Y') ?></h3>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script src="<?php echo base_url() ?>assets/js/url.js"></script>
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/bootstrap.js"></script>


	<script>
		function waktu() {
			var waktu = new Date();
			setTimeout("waktu()", 1000);
			document.getElementById("jam").innerHTML = waktu.getHours();
			document.getElementById("menit").innerHTML = waktu.getMinutes();
			document.getElementById("detik").innerHTML = waktu.getSeconds();
		}
		

		function setAnimate(){
			$(`.title-unitkerja`).fadeIn('slow');
			$(`#title_sukubunga`).fadeIn('slow');

			$(`div.content-title`).fadeOut('slow');
			$(`div.content-title`).fadeIn('slow');
			$(`div.content-kurs`).fadeOut('slow');
			$(`div.content-kurs`).fadeIn('slow');
		}

		$(function() {
			var video = $('#video');
			window.setTimeout("waktu()", 2000);
			video[0].muted = false;


			//set animasi
			setAnimate();


			//interval kalo
			window.setInterval(function() {
				//
					$.ajax({
						type: 'POST',
						// data: params,
						dataType: 'json',
						url: url.base_url("customerView") + "getChangeVideo",
						error: function () {
							toastr.error("Program Error");
						},

						beforeSend: function () {
							// message.loadingProses("Proses Pengambilan Formulir...");
						},

						success: function (resp) {
							//add
								var video = $('#video');
								let link_url = `<?php echo base_url() ?>files/berkas/video/${resp.video.video}`;
								// console.log(link_url);
								video.attr('src', link_url);
						}
					});
			}, <?php echo $interval['waktu'] ?>);

		});
	</script>
</body>

</html>

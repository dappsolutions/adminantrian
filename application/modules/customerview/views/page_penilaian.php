<input type="hidden" id="customer-id" value="">
<input type="hidden" id="antrian-id" value="">

<div class="row">
	<div class="col-md-12">
		<div class="box padding-16">
			<div class="box-body">
				<div class="row">
					<div class="col-md-12">
						<h4>Form Penilaian Pelanggan</h4>
					</div>
				</div>
				<hr>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class='col-md-6 text-bold'>
								Scan Barcode
							</div>
							<div class='col-md-6'>
								<input type='text' name='' id='scan_barcode' autofocus class='form-control' value='' onkeyup="CustomerView.getDataCustomer(this, event)" />
							</div>
						</div>
						<br />


						<div class="row">
							<div class='col-md-6 text-bold'>
								Nama
							</div>
							<div class='col-md-6'>
								<input type='text' readonly name='' id='nama' class='form-control required' value='<?php echo isset($nama) ? $nama : '' ?>' error="Nama" placeholder="Nama" />
							</div>
						</div>
						<br />

						<div class="row">
							<div class='col-md-6 text-bold'>
								KTP (Kartu Indentitas)
							</div>
							<div class='col-md-6'>
								<input type='text' readonly name='' id='ktp' class='form-control required' value='<?php echo isset($ktp) ? $ktp : '' ?>' error="KTP" placeholder="KTP (Kartu Indentitas)" />
							</div>
						</div>
						<br />

						<div class="row">
							<div class='col-md-6 text-bold'>
								No. Telp
							</div>
							<div class='col-md-6'>
								<input type='text' readonly name='' id='no_tlp' class='form-control required' value='<?php echo isset($no_tlp) ? $no_tlp : '' ?>' error="No Telp" placeholder="No. Telp" />
							</div>
						</div>
						<br />

						<div class="row">
							<div class='col-md-6 text-bold'>
								Alamat
							</div>
							<div class='col-md-6'>
								<textarea id="alamat" readonly class="form-control required" error="Alamat"></textarea>
							</div>
						</div>
						<br />

						<div class="row">
							<div class='col-md-6 text-bold'>
								Keperluan
							</div>
							<div class='col-md-6'>
								<textarea id="keperluan" readonly class="form-control required" error="Keperluan"></textarea>
							</div>
						</div>
						<br />
						<div class="row">
							<div class='col-md-12 text-right'>
								<button class="btn btn-success" onclick="CustomerView.prosesSimpanPenilaian(this)">PROSES</button>
								<button class="btn" onclick="CustomerView.cancel()">BATAL</button>
							</div>
						</div>
						<br />
					</div>


					<div class="col-md-6">
						<div class="row" style="border: 1px solid #ccc;padding:8px !important">
							<div class="col-md-12">
								<?php if (!empty($pertanyaan)) { ?>
									<?php $no = 1; ?>
									<?php foreach ($pertanyaan as $key => $value) { ?>
										<div class="row data_pertanyaan" data_id="<?php echo $value['id'] ?>">
											<div class="col-md-12">
												<div class="row">
													<div class="col-md-12">
														<?php echo $no++ ?>. <?php echo $value['pertanyaan'] ?>
													</div>
													<div class="col-md-12 text-right">
														<?php if (!empty($penilaian)) { ?>
															<?php foreach ($penilaian as $k => $v) { ?>
																<input type="radio" class="data_kategori" pertanyaan="<?php echo $value['id'] ?>" name="<?php echo $no ?>_radio" data_id="<?php echo $v['id'] ?>"> <?php echo $v['kategori'] ?>
															<?php } ?>
														<?php } ?>
													</div>
												</div>

											</div>
										</div>
										<br>
									<?php } ?>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

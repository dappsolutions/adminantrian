<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($id) ? $id : '' ?>' />

<div class="content">
	<div class="animated fadeIn">
		<div class="padding-16">
			<div class="">
				<h4>Silakan Scan Barcode, untuk proses selanjutnya..</h4>
				<hr>

				<div class="row">
					<div class='col-md-6 text-bold'>
						Scan Barcode
					</div>
					<div class='col-md-6'>
						<input type='text' name='' id='scan_barcode' autofocus class='form-control' value='' onkeyup="CustomerView.getDataCustomer(this, event)" />
					</div>
				</div>
				<br />

				<input type="hidden" id="customer-id" value="">
				<input type="hidden" id="antrian-id" value="">

				<div class="row">
					<div class='col-md-6 text-bold'>
						Nama
					</div>
					<div class='col-md-6'>
						<input type='text' readonly name='' id='nama' class='form-control required' value='<?php echo isset($nama) ? $nama : '' ?>' error="Nama" placeholder="Nama" />
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						KTP (Kartu Indentitas)
					</div>
					<div class='col-md-6'>
						<input type='text' readonly name='' id='ktp' class='form-control required' value='<?php echo isset($ktp) ? $ktp : '' ?>' error="KTP" placeholder="KTP (Kartu Indentitas)" />
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						No. Telp
					</div>
					<div class='col-md-6'>
						<input type='text' readonly name='' id='no_tlp' class='form-control required' value='<?php echo isset($no_tlp) ? $no_tlp : '' ?>' error="No Telp" placeholder="No. Telp" />
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						Alamat
					</div>
					<div class='col-md-6'>
						<textarea id="alamat" readonly class="form-control required" error="Alamat"></textarea>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						Keperluan
					</div>
					<div class='col-md-6'>
						<textarea id="keperluan" readonly class="form-control required" error="Keperluan"></textarea>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						Kategori Penilaian
					</div>
					<div class='col-md-6'>
						<select name="" id="kategori" class="form-control required" error="Kategori Penilaian">
							<?php if (!empty($list_penilaian)) { ?>
								<?php foreach ($list_penilaian as $key => $value) { ?>
									<option value="<?php echo $value['id'] ?>"><?php echo $value['kategori'] ?></option>
								<?php } ?>
							<?php } ?>
						</select>
					</div>
				</div>
				<br />

				<div class="row">
					<div class='col-md-6 text-bold'>
						Keterangan
					</div>
					<div class='col-md-6'>
						<textarea id="keterangan" class="form-control"></textarea>
					</div>
				</div>
				<br />

				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-success" onclick="CustomerView.prosesSimpanPenilaian(this)">Proses</button>
						&nbsp;
						<button id="" class="btn btn-danger" onclick="CustomerView.batal()">Batal</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
	$(document).on('shown.bs.modal', '.modal', function() {
		$(this).find('input:visible:first').focus();
	});
</script>

<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>POLSEK PENGADUAN DAN PELAYANAN</title>
	<!-- Tell the browser to be responsive to screen width -->
	<link rel="shortcut icon" type="image/png" href="<?php echo base_url() ?>assets/images/logo_polda.png" />
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/dist/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/square/blue.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main_app.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">

	<style>
		.hover-content:hover {
			cursor: pointer;
			background-color: #dd4b39;
			color: white;
		}

		.choosed {
			background-color: #dd4b39;
			color: white;
		}
	</style>
</head>

<body class="hold-transition login-page" style="background-color: #259dd4;">
	<div class="loader">
	</div>
	<div class="container">
		<input type="hidden" id="jenis" value="<?php echo $jenis ?>">
		<br>
		<div class="row">
			<div class="col-md-3 text-right">
				<img src="<?php echo base_url() ?>assets/images/logo_polri.png" alt="" height="100" width="150">
			</div>
			<div class="col-md-6 text-center">
				<h3 style="color:yellow;font-weight:bold;">SENTRA PENGADUAN PELAYANAN TERPADU</h3>
				<h1 style="color:white;font-weight: bold;">POLRES TRENGGALEK</h1>
			</div>
			<div class="col-md-3 text-left">
				<img src="<?php echo base_url() ?>assets/images/logo_polda.png" alt="" height="100" width="200">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 text-center">
				<h3 style="color:yellow;font-weight: bold;">SILAKAN MASUKKAN DATA DIRI ANDA</h3>
			</div>
		</div>
		<br>
		<div class="box padding-16">
			<div class="box-body">
				<div class="row">
					<div class="col-md-9">
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="">
										<tr>
											<td>
												<label for="" style="font-size: 20px;">IDENTITAS</label>
											</td>
											<?php for ($i = 0; $i < 15; $i++) { ?>
												<td>&nbsp;</td>
											<?php } ?>
											<td class="identitas">
												<button class="btn hover-content" data_id="0" onclick="chooseContent(this)">KTP</button>
												<button class="btn hover-content" data_id="1" onclick="chooseContent(this)">KK</button>
												<button class="btn hover-content" data_id="2" onclick="chooseContent(this)">SIM</button>
												<button class="btn hover-content" data_id="3" onclick="chooseContent(this)">KARTU PELAJAR</button>
												<button class="btn hover-content" data_id="4" onclick="chooseContent(this)">PASPOR</button>
											</td>
										</tr>
										<tr>
											<td colspan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<label for="" style="font-size: 20px;">NO IDENTITAS</label>
											</td>
											<?php for ($i = 0; $i < 15; $i++) { ?>
												<td>&nbsp;</td>
											<?php } ?>
											<td>
												<input type="text" id="no_identitas" value="" class="form-control required" error="No Identitas">
											</td>
										</tr>
										<tr>
											<td colspan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<label for="" style="font-size: 20px;">NAMA</label>
											</td>
											<?php for ($i = 0; $i < 15; $i++) { ?>
												<td>&nbsp;</td>
											<?php } ?>
											<td>
												<input type="text" id="nama" value="" class="form-control required" error="Nama">
											</td>
										</tr>
										<tr>
											<td colspan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<label for="" style="font-size: 20px;">ALAMAT</label>
											</td>
											<?php for ($i = 0; $i < 15; $i++) { ?>
												<td>&nbsp;</td>
											<?php } ?>
											<td>
												<input type="text" id="alamat" value="" class="form-control required" error="Alamat">
											</td>
										</tr>
										<tr>
											<td colspan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<label for="" style="font-size: 20px;">UMUR</label>
											</td>
											<?php for ($i = 0; $i < 15; $i++) { ?>
												<td>&nbsp;</td>
											<?php } ?>
											<td>
												<input type="text" id="umur" value="" class="form-control required" error="UMUR">
											</td>
										</tr>
										<tr>
											<td colspan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<label for="" style="font-size: 20px;">NO HP</label>
											</td>
											<?php for ($i = 0; $i < 15; $i++) { ?>
												<td>&nbsp;</td>
											<?php } ?>
											<td>
												<input type="text" id="no_hp" value="" class="form-control required" error="NO HP">
											</td>
										</tr>
										<tr>
											<td colspan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<label for="" style="font-size: 20px;">J. KELAMIN</label>
											</td>
											<?php for ($i = 0; $i < 15; $i++) { ?>
												<td>&nbsp;</td>
											<?php } ?>
											<td class="jk">
												<button class="btn hover-content" data_id="0" onclick="chooseContent(this)">LAKI - LAKI</button>
												<button class="btn hover-content" data_id="1" onclick="chooseContent(this)">PEREMPUAN</button>
											</td>
										</tr>
										<tr>
											<td colspan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<label for="" style="font-size: 20px;">PENDIDIKAN</label>
											</td>
											<?php for ($i = 0; $i < 15; $i++) { ?>
												<td>&nbsp;</td>
											<?php } ?>
											<td class="pendidikan">
												<button class="btn hover-content" data_id="0" onclick="chooseContent(this)">SD</button>
												<button class="btn hover-content" data_id="1" onclick="chooseContent(this)">SMP</button>
												<button class="btn hover-content" data_id="2" onclick="chooseContent(this)">SMU</button>
												<button class="btn hover-content" data_id="3" onclick="chooseContent(this)">DIPLOMA</button>
												<button class="btn hover-content" data_id="4" onclick="chooseContent(this)">S1</button>
												<button class="btn hover-content" data_id="5" onclick="chooseContent(this)">S2</button>
												<button class="btn hover-content" data_id="6" onclick="chooseContent(this)">S3</button>
											</td>
										</tr>
										<tr>
											<td colspan="20">&nbsp;</td>
										</tr>
										<tr>
											<td>
												<label for="" style="font-size: 20px;">PEKERJAAN</label>
											</td>
											<?php for ($i = 0; $i < 15; $i++) { ?>
												<td>&nbsp;</td>
											<?php } ?>
											<td class="pekerjaan">
												<button class="btn hover-content" data_id="0" onclick="chooseContent(this)">PNS/TNI/POLRI</button>
												<button class="btn hover-content" data_id="1" onclick="chooseContent(this)">PEGAWAI SWASTA</button>
												<button class="btn hover-content" data_id="2" onclick="chooseContent(this)">WIRASWASTA</button>
												<button class="btn hover-content" data_id="3" onclick="chooseContent(this)">PELAJAR/MAHASISWA</button>
												<button class="btn hover-content" data_id="4" onclick="chooseContent(this)">LAINNYA</button>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-6 text-left">
								<button class="btn btn-danger" onclick="batal()">BATAL</button>
							</div>
							<div class="col-md-6 text-right">
								<button class="btn btn-success" onclick="simpanData(this)">SIMPAN</button>
							</div>
						</div>
					</div>
					<div class="col-md-3 text-left">
						<img src="<?php echo base_url() ?>assets/images/logo_polisi.png" alt="" width="200" height="200">
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div style="margin-top: -12px;text-align: right;">
					<h1 style="text-shadow: 2px 2px #dd4b39;color:white;">
						<label for="" id="jam"></label>:<label for="" id="menit"></label>:<label for="" id="detik"></label>
					</h1>
				</div>
			</div>
		</div>
		<br>
		</section>
	</div>

	<!-- jQuery 3 -->
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/validation.js"></script>
	<script src="<?php echo base_url() ?>assets/js/url.js"></script>
	<script src="<?php echo base_url() ?>assets/js/message.js"></script>
	<script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
	<script>
		function waktu() {
			var waktu = new Date();
			setTimeout("waktu()", 1000);
			document.getElementById("jam").innerHTML = waktu.getHours();
			document.getElementById("menit").innerHTML = waktu.getMinutes();
			document.getElementById("detik").innerHTML = waktu.getSeconds();
		}

		function chooseContent(elm) {
			var td = $(elm).closest('td');
			td.find('.btn').removeClass('choosed');
			$(elm).addClass('choosed');
		}

		function batal() {
			window.location.href = url.base_url("customerview") + "show";
		}

		function simpanData(elm) {
			var params = {};
			params.identitas = $.trim($('td.identitas').find('.choosed').attr('data_id'));
			params.no_identitas = $.trim($('input#no_identitas').val());
			params.nama = $.trim($('input#nama').val());
			params.alamat = $.trim($('input#alamat').val());
			params.umur = $.trim($('input#umur').val());
			params.no_hp = $.trim($('input#no_hp').val());
			params.jk = $.trim($('td.jk').find('.choosed').attr('data_id'));
			params.pendidikan = $.trim($('td.pendidikan').find('.choosed').attr('data_id'));
			params.pekerjaan = $.trim($('td.pekerjaan').find('.choosed').attr('data_id'));
			params.jenis = $('#jenis').val();

			if (validation.run()) {
				$.ajax({
					type: 'POST',
					data: params,
					dataType: 'json',
					url: url.base_url("customerview") + "simpanData",
					error: function() {
						toastr.error("Program Error");
						message.closeLoading();
					},

					beforeSend: function() {
						message.loadingProses("Proses Simpan Data...");
					},

					success: function(resp) {
						message.closeLoading();
						message.closeDialog();
						if (resp.is_valid) {
							toastr.success("Proses Simpan Berhasil");
							setTimeout(function() {
								window.location.reload();
							}, 3000);
						} else {
							toastr.error("Proses Simpan Gagal");
						}
					}
				});
			}
		}

		$(function() {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' /* optional */
			});

			window.setTimeout("waktu()", 1000);
		});
	</script>
</body>

</html>

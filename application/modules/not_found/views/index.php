<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>BRI | Log in</title>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/bower_components/Ionicons/css/ionicons.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/dist/css/AdminLTE.min.css">
	<!-- iCheck -->
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/square/blue.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/main_app.css">
	<link rel="stylesheet" href="<?php echo base_url() ?>assets/css/toastr.min.css">
</head>

<body class="hold-transition login-page">
	<br />
	<div class="row">
		<div class="col-md-12">
			<section class="content-header text-center">
				<h1>
					404 Error Page
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<div class="error-page">
					<h2 class="headline text-yellow"> 404</h2>

					<div class="error-content">
						<h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

						<p>
							We could not find the page you were looking for.
							Meanwhile, you may <a href="<?php echo base_url() ?>">return to login</a> or try using the search form.
						</p>

						<form class="search-form">
							<div class="input-group">
								<input type="text" name="search" class="form-control" placeholder="Search">

								<div class="input-group-btn">
									<button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i>
									</button>
								</div>
							</div>
							<!-- /.input-group -->
						</form>
					</div>
					<!-- /.error-content -->
				</div>
				<!-- /.error-page -->
			</section>
		</div>
	</div>
	<!-- /.login-box -->

	<!-- jQuery 3 -->
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/jquery/dist/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="<?php echo base_url() ?>assets/admin_lte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script src="<?php echo base_url() ?>assets/admin_lte/plugins/iCheck/icheck.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/validation.js"></script>
	<script src="<?php echo base_url() ?>assets/js/url.js"></script>
	<script src="<?php echo base_url() ?>assets/js/message.js"></script>
	<script src="<?php echo base_url() ?>assets/js/toastr.min.js"></script>
	<script src="<?php echo base_url() ?>assets/js/controllers/login.js"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass: 'icheckbox_square-blue',
				radioClass: 'iradio_square-blue',
				increaseArea: '20%' /* optional */
			});
		});
	</script>
</body>

</html>
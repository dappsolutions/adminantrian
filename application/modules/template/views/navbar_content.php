<header class="main-header">
	<!-- Logo -->
	<a href="<?php echo base_url() . 'dashboard' ?>" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini">
			<img src="<?php echo base_url() ?>assets/images/logo/Bank-BRI.png">
		</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg">
			<h4 style="margin-top: 16px;">BRI</h4>
			<!-- <img src="<?php echo base_url() ?>assets/images/logo/logo_splash.png"> -->
		</span>
	</a>
	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- Messages: style can be found in dropdown.less-->
				<!-- Notifications: style can be found in dropdown.less -->
				<li class="dropdown notifications-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-bell-o"></i>
						<span class="label label-warning"><?php echo '-' ?></span>
					</a>
					<ul class="dropdown-menu">
						<li class="header">You have <?php echo $total_notif ?> notifications</li>
						<?php echo $this->load->view('notification_view'); ?>
						<!--<li class="footer"><a href="#">View all</a></li>-->
					</ul>
				</li>
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<img src="<?php echo base_url() ?>assets/images/images.png" class="user-image" alt="User Image">
						<span class="hidden-xs"><?php echo ucfirst($this->session->userdata('username')) ?></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<img src="<?php echo base_url() ?>assets/images/images.png" class="img-circle" alt="User Image">

							<p>
								<?php echo ucfirst($this->session->userdata('username')) ?> - <?php echo $this->session->userdata('hak_akses') ?>
								<small>Member since <?php echo ucfirst($this->session->userdata('user_created')) ?></small>
							</p>
						</li>
						<!-- Menu Body -->
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" onclick="Template.changePassword(this, event)" class="btn btn-default btn-flat">Ganti Password</a>
							</div>
							<div class="pull-right">
								<a href="<?php echo base_url() . 'login/sign_out' ?>" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
				</li>
				<!-- Control Sidebar Toggle Button -->
				<li>
					<a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
				</li>
			</ul>
		</div>
	</nav>
</header>
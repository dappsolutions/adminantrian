<?php if ($this->session->userdata('hak_akses') == "admin") { ?>
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="active"><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>


		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Data</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'unitkerja' ?>"><i class="fa fa-file-text-o"></i> Unit Kerja </a></li>
				<!-- <li><a href="<?php echo base_url() . 'customer' ?>"><i class="fa fa-file-text-o"></i> Customer </a></li> -->
				<li><a href="<?php echo base_url() . 'jenissukubunga' ?>"><i class="fa fa-file-text-o"></i> Jenis Suku Bunga </a></li>
				<li><a href="<?php echo base_url() . 'sukubunga' ?>"><i class="fa fa-file-text-o"></i> Suku Bunga </a></li>
				<li><a href="<?php echo base_url() . 'kurs' ?>"><i class="fa fa-file-text-o"></i> Kurs</a></li>
				<li><a href="<?php echo base_url() . 'intervaldisplay' ?>"><i class="fa fa-file-text-o"></i> Interval Display</a></li>
				<li><a href="<?php echo base_url() . 'intervalinfo' ?>"><i class="fa fa-file-text-o"></i> Interval Info</a></li>
				<li><a href="<?php echo base_url() . 'shutdown' ?>"><i class="fa fa-file-text-o"></i> Shutdown</a></li>
			</ul>
		</li>

		<!-- <li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Laporan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'report_kepuasan' ?>"><i class="fa fa-file-text-o"></i> Kepuasan Pelanggan </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Grafik</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'grafik_kepuasan' ?>"><i class="fa fa-file-text-o"></i> Kepuasan Pelanggan </a></li>
			</ul>
		</li> -->

		<li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
	</ul>

<?php } ?>

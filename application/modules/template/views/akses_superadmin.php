<?php if ($this->session->userdata('hak_akses') == "superadmin") { ?>
	<ul class="sidebar-menu" data-widget="tree">
		<li class="header">MAIN NAVIGATION</li>
		<li class="active"><a href="<?php echo base_url() . 'dashboard' ?>"><i class="fa fa-folder-o"></i> <span>Dashboard</span></a></li>


		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Data</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'antrian' ?>"><i class="fa fa-file-text-o"></i> Antrian </a></li>
				<!-- <li><a href="<?php echo base_url() . 'customer' ?>"><i class="fa fa-file-text-o"></i> Customer </a></li> -->
				<li><a href="<?php echo base_url() . 'questions' ?>"><i class="fa fa-file-text-o"></i> Pertanyaan </a></li>
				<li><a href="<?php echo base_url() . 'user' ?>"><i class="fa fa-file-text-o"></i> Pengguna </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Laporan</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'report_kepuasan' ?>"><i class="fa fa-file-text-o"></i> Kepuasan Pelanggan </a></li>
			</ul>
		</li>

		<li class="treeview">
			<a href="#">
				<i class="fa fa-folder"></i>
				<span>Grafik</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
				<li><a href="<?php echo base_url() . 'grafik_kepuasan' ?>"><i class="fa fa-file-text-o"></i> Kepuasan Pelanggan </a></li>
			</ul>
		</li>

		<li><a href="<?php echo base_url() . 'login/sign_out' ?>"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
	</ul>

<?php } ?>

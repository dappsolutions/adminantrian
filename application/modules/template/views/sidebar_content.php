<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<img src="<?php echo base_url() ?>assets/images/images.png" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
				<p><?php echo ucfirst($this->session->userdata('username')) ?></p>
				<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- /.search form -->
		<?php echo $this->load->view('akses_superadmin'); ?>
		<?php echo $this->load->view('akses_admin'); ?>
		<?php echo $this->load->view('akses_teller_or_cs'); ?>
	</section>
</aside>

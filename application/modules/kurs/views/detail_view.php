<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Kurs</u>
					</div>
				</div>
				<hr />

				<div class="row">
					<div class='col-md-3'>
						Negara
					</div>
					<div class='col-md-3'>
						<b><?php echo $negara ?></b>
					</div>
				</div>
				<br />
				
				<div class="row">
					<div class='col-md-3'>
						Jual
					</div>
					<div class='col-md-3'>
						<b><?php echo $jual ?></b>
					</div>
				</div>
				<br />
			
				<div class="row">
					<div class='col-md-3'>
						Beli
					</div>
					<div class='col-md-3'>
						<b><?php echo $beli ?></b>
					</div>
				</div>
				<br />

				<hr />
				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-danger-baru" onclick="Kurs.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

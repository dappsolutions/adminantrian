<input type='hidden' name='' id='id' class='form-control' value='<?php echo isset($nox) ? $nox : '' ?>' />
<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class='row'>
					<div class='col-md-12'>
						<u>Data Kurs</u>
					</div>
				</div>
				<hr />

				<div class="row">
					<div class='col-md-3'>
						Negara
					</div>
					<div class='col-md-3'>
						<input id="negara" class="form-control required" error="Negara" value="<?php echo isset($negara) ? $negara : '' ?>"/>
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3'>
						Beli
					</div>
					<div class='col-md-3'>
						<input id="beli" class="form-control required" error="Beli" value="<?php echo isset($beli) ? $beli : '' ?>"/>
					</div>
				</div>
				<br />
				<div class="row">
					<div class='col-md-3'>
						Jual
					</div>
					<div class='col-md-3'>
						<input id="jual" class="form-control required" error="Jual" value="<?php echo isset($jual) ? $jual : '' ?>"/>
					</div>
				</div>
				<br />
				<hr />

				<div class='row'>
					<div class='col-md-12 text-right'>
						<button id="" class="btn btn-success" onclick="Kurs.simpan('<?php echo isset($nox) ? $nox : '' ?>')">Simpan</button>
						&nbsp;
						<button id="" class="btn btn-baru" onclick="Kurs.back()">Kembali</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

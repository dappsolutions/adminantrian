<div class="content">
	<div class="animated fadeIn">
		<div class="box padding-16">
			<div class="box-body box-block">
				<div class="row">
					<div class="col-md-12">
						<div class="input-group">
							<input type="text" class="form-control" onkeyup="Shutdown.search(this, event)" id="keyword" placeholder="Pencarian">
							<span class="input-group-addon"><i class="fa fa-search"></i></span>
						</div>
					</div>
				</div>
				<br />
				<div class='row'>
					<div class='col-md-12'>
						<?php if (isset($keyword)) { ?>
							<?php if ($keyword != '') { ?>
								Cari Data : "<b><?php echo $keyword; ?></b>"
							<?php } ?>
						<?php } ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-12">
						<div class='table-responsive'>
							<table class="table table-striped table-bordered table-list-draft">
								<thead>
									<tr class="bg-primary-light text-white">
										<th>No</th>
										<th>Matikan</th>
										<!-- <th></th> -->
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<?php if (!empty($content)) { ?>
										<?php $no = $pagination['last_no'] + 1; ?>
										<?php foreach ($content as $value) { ?>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $value['matikan'] == '0' ? 'Mati' : 'Hidup' ?></td>
												<td class="text-center">
													<!-- <i class="fa fa-trash grey-text hover" onclick="Shutdown.delete('<?php echo $value['id'] ?>')"></i>
													&nbsp; -->
													<!-- <i class="fa fa-pencil grey-text  hover" onclick="Shutdown.ubah('<?php echo $value['id'] ?>')"></i>
													&nbsp; -->
													<!-- <i class="fa fa-file-text grey-text  hover" onclick="Shutdown.detail('<?php echo $value['id'] ?>')"></i> -->
													<?php if($value['matikan'] == 0){ ?>
														<i class="fa fa-check text-success  hover" onclick="Shutdown.hidupkan('<?php echo $value['id'] ?>')"></i>
													<?php }else{ ?>
														<i class="fa fa-close text-danger  hover" onclick="Shutdown.matikan('<?php echo $value['id'] ?>')"></i>
													<?php } ?>
												</td>
											</tr>
										<?php } ?>
									<?php } else { ?>
										<tr>
											<td colspan="6" class="text-center">Tidak ada data ditemukan</td>
										</tr>
									<?php } ?>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div class="box-footer clearfix">
				<ul class="pagination pagination-sm no-margin pull-right">
					<?php echo $pagination['links'] ?>
				</ul>
			</div>
		</div>

		<!-- <a href="#" class="float" onclick="Shutdown.add()">
			<i class="fa fa-plus my-float fa-lg"></i>
		</a> -->
	</div>
</div>

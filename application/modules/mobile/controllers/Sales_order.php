<?php

class Sales_order extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'Sales_order';
	}

	public function getTableName()
	{
		return 'order';
	}

	public function index()
	{
		echo 'Sales_order';
	}

	public function getPostDataHeader()
	{
		$data['no_order'] = Modules::run('no_generator/generateNoOrder');
		$data['pembeli'] = $_POST['pembeli'];
		$data['createdby'] = $_POST['user'];
		$data['tanggal_faktur'] = date('Y-m-d H:i:s');
		$data['createddate'] = date('Y-m-d');
		$data['potongan'] = 3;
		$data['total'] = str_replace('.', '', $_POST['total']);
		return $data;
	}

	public function prosesSimpan()
	{
		$is_valid = "0";

		$data_item = json_decode($_POST['data_item']);
		// echo '<pre>';
		// print_r($data_item);
		// die;
		$this->db->trans_begin();
		try {
			$post_data = $this->getPostDataHeader();
			$this->db->insert($this->getTableName(), $post_data);
			// echo '<pre>';
			// echo $this->db->last_query();die;
			$id = $this->db->insert_id();

			//invoice product item    
			if (!empty($data_item)) {
				foreach ($data_item as $value) {
					$post_item['order'] = $id;
					$post_item['product_satuan'] = $value->product_satuan;
					$post_item['qty'] = $value->qty;
					$post_item['sub_total'] = intval($value->sub_total);
					$order_product = Modules::run('database/_insert', 'order_product', $post_item);

					//insert potongan
					$post_pot_item['order_product'] = $order_product;
					$post_pot_item['potongan'] = $value->potongan;
					$post_pot_item['nilai'] = $value->nilai_potongan;

					Modules::run('database/_insert', 'order_pot_product', $post_pot_item);
					//insert into product_log_stock
					// $post_log_stok['product_satuan'] = $value->product_satuan;
					// $post_log_stok['status'] = 'ORDER';
					// $post_log_stok['qty'] = $value->qty;
					// $post_log_stok['keterangan'] = 'Order Pelanggan';
					// $log_stock = Modules::run('database/_insert', 'product_log_stock', $post_log_stok);
				}
			}



			//    echo '<pre>';
			//    print_r($data_bank);die;
			//invoice status
			$post_status['order'] = $id;
			// $post_status['user'] = $_POST['user'];
			$post_status['status'] = 'DRAFT';
			$order_status = Modules::run('database/_insert', 'order_status', $post_status);

			$this->db->trans_commit();
			$is_valid = "1";
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid, 'id' => $id));
	}

	public function prosesConfirm()
	{
		$is_valid = "0";

		$this->db->trans_begin();
		try {
			$post_status['order'] = $_POST['order'];
			$post_status['status'] = $_POST['status'];
			$order_status = Modules::run('database/_insert', 'order_status', $post_status);

			$this->db->trans_commit();
			$is_valid = "1";
		} catch (Exception $ex) {
			$this->db->trans_rollback();
		}

		echo json_encode(array('is_valid' => $is_valid));
	}


	public function searchProduk()
	{
		$keyword = trim($_POST['keyword']);
		$data = Modules::run('database/get', array(
			'table' => 'product p',
			'field' => array(
				'p.*', 's.nama_satuan',
				'ps.harga', 'ps.id as product_satuan'
			),
			'join' => array(
				array('product_satuan ps', 'p.id = ps.product'),
				array('satuan s', 's.id = ps.satuan'),
			),
			'where' => "(p.deleted is null or p.deleted = 0 and ps.deleted = 0) 
			and (p.kode_product like '%" . $keyword . "%' or p.product like '%" . $keyword . "%') "
		));
		
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['val'] = $value['product'] . ' - ' . $value['nama_satuan'] . ' - ' . number_format($value['harga']);
				$value['harga_str'] = number_format($value['harga']);
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}

	public function searchCustomer(){
		$keyword = trim($_POST['keyword']);
		// $keyword = "sum";
		$data = Modules::run('database/get', array(
						'table' => 'pembeli p',
						'field' => array('p.*'),
						'like' => array(
							array("p.nama", $keyword)
						),
						'inside_brackets'=> true,
						'is_or_like'=> true,
						'where' => "p.deleted is null or p.deleted = 0"
		));

		$result = array();
		if(!empty($data)){
						foreach ($data->result_array() as $value) {
										array_push($result, $value);
						}
		}

		echo json_encode(array(
						'data'=> $result
		));
}
}

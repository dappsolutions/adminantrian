<?php

class List_sales_order extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'List_sales_order';
	}

	public function getTableName()
	{
		return 'order';
	}

	public function index()
	{
		echo 'List_sales_order';
	}

	public function getListSalesOrder()
	{
		$user = isset($_POST['user']) ? $_POST['user'] : '';
		$now = date('Y-m-d');
		// $user = '1';
		// echo $now;die;
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' o',
			'field' => array(
				'o.*', 'p.nama as nama_pembeli',
				'isa.status', 'pro.product as nama_product',
				'sa.nama_satuan', 'ps.harga', 'isp.qty', "isp.id as order_product"
			),
			'join' => array(
				array('pembeli p', 'p.id = o.pembeli'),
				array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = o.id'),
				array('order_status isa', 'isa.id = iss.id'),
				array('(select max(id) id, `order` from order_product group by `order`) ispp', 'ispp.order = o.id'),
				array('order_product isp', 'isp.id = ispp.id'),
				array('product_satuan ps', 'ps.id = isp.product_satuan'),
				array('product pro', 'pro.id = ps.product'),
				array('satuan sa', 'sa.id = ps.satuan'),

			),
			'where' => "o.deleted is null or o.deleted = 0 and (o.createdby = '" . $user . "' or o.createdby is null) and o.tanggal_faktur = '" . $now . "'",
			'orderby' => "o.id desc"
		));

		// echo '<pre>';
		// echo $this->db->last_query();die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['total_str'] = 'Rp, ' . number_format($value['total']);
				$value['produk'] = $value['nama_product'] . ' - '
					. $value['nama_satuan'] . ' - Rp, ' . number_format($value['harga']);
				array_push($result, $value);
			}
		}

		echo json_encode(array(
			'data' => $result
		));
	}

	public function getListItemOrder($order)
	{
		$user = isset($_POST['user']) ? $_POST['user'] : '';
		// $user = '1';
		$data = Modules::run('database/get', array(
			'table' => $this->getTableName() . ' o',
			'field' => array(
				'o.*', 'p.nama as nama_pembeli',
				'isa.status', 'pro.product as nama_product',
				'sa.nama_satuan', 'ps.harga', 'isp.qty',
				"isp.id as order_product", 'isp.sub_total'
			),
			'join' => array(
				array('pembeli p', 'p.id = o.pembeli'),
				array('(select max(id) id, `order` from order_status group by `order`) iss', 'iss.order = o.id'),
				array('order_status isa', 'isa.id = iss.id'),
				array('order_product isp', 'isp.order = o.id'),
				array('product_satuan ps', 'ps.id = isp.product_satuan'),
				array('product pro', 'pro.id = ps.product'),
				array('satuan sa', 'sa.id = ps.satuan'),

			),
			'where' => "o.id = '" . $order . "'",
			'orderby' => "o.id desc"
		));

		// echo '<pre>';
		// echo $this->db->last_query();die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				$value['total_str'] = 'Rp, ' . number_format($value['total']);
				$value['produk'] = $value['nama_product'] . ' - '
					. $value['nama_satuan'] . ' - Rp, ' . number_format($value['harga']);
				$data_potongan = $this->getDetailPotongan($value['order_product']);

				$value['jenis_potongan'] = "";
					$value['nilai_potongan'] = "";
				if(!empty($data_potongan)){
					$value['jenis_potongan'] = $data_potongan['jenis_potongan'];
					$value['nilai_potongan'] = $data_potongan['nilai'];
				}
				array_push($result, $value);
			}
		}

		// echo '<pre>';
		// print_r($result);die;
		echo json_encode(array(
			'data' => $result
		));
	}

	public function getDetailPotongan($order_product)
	{
		$data = Modules::run('database/get', array(
			'table' => 'order_pot_product ipp',
			'field' => array('ipp.*', 'pt.potongan as jenis_potongan'),
			'join' => array(
				array('potongan pt', 'pt.id = ipp.potongan', 'left')
			),
			'where' => "ipp.deleted = 0 and ipp.order_product = '" . $order_product . "'"
		));

		$result = array();
		if (!empty($data)) {
			$result = $data->row_array();
		}


		return $result;
	}
}

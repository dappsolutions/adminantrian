<?php

class Aktifitas extends MX_Controller
{

	public $segment;
	public $limit;
	public $page;
	public $last_no;

	public function __construct()
	{
		parent::__construct();
		$this->limit = 10;
	}

	public function getModuleName()
	{
		return 'Aktifitas';
	}

	public function getTableName()
	{
		return 'pembeli';
	}

	public function index()
	{
		echo 'Aktifitas';
	}

	public function getDay(){
		$day = date('l');
		switch (strtolower($day)) {
			case 'sunday':
				return 7;
				break;			
			case 'monday':
				return 1;
				break;			
			case 'tuesday':
				return 2;
				break;			
			case 'wednesday':
				return 3;
				break;			
			case 'thursday':
				return 4;
				break;			
			case 'friday':
				return 5;
				break;			
			case 'saturday':
				return 6;
				break;			
			default:
				return 1;
				break;
		}
	}

	public function getListAktifitasSales()
	{
		$day = $this->getDay();
		// echo $day;die;
		// $user = "3";
		$user = $_POST['user'];
		
		$sql = "
		select sr.id
	, sr.no_rute
	, sr.minggu
	, pb.nama as nama_pembeli
	, pg.nama as nama_sales
	, h.nama as hari
	, mg.nama as minggu_str
	from sales_rute sr
	join sales_rute_customer src
		on src.sales_rute = sr.id
	join pembeli pb
		on pb.id = src.pembeli
	join sales_rute_user sru
		on sru.sales_rute = sr.id
	join `user` u
		on u.id = sru.`user` 
	join pegawai pg
		on u.pegawai = pg.id
	join sales_rute_hari srh
		on srh.sales_rute = sr.id
	join hari h
		on srh.hari = h.id
	join minggu mg
		on mg.id = sr.minggu
		 where u.id = '" . $user . "'";
		$data = Modules::run('database/get_custom', $sql);

		// echo '<pre>';
		// echo $this->db->last_query();die;
		$result = array();
		if (!empty($data)) {
			foreach ($data->result_array() as $value) {
				array_push($result, $value);
			}
		}

		// echo '<pre>';
		// print_r($result);die;
		echo json_encode(array(
			'data' => $result
		));
	}
}

var Pengadaan = {
 module: function () {
  return 'pengadaan';
 },

 add: function () {
  window.location.href = url.base_url(Pengadaan.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Pengadaan.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Pengadaan.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Pengadaan.module()) + "index";
   }
  }
 },

 getPostProcItem: function () {
  var data = [];
  var tb_product = $('table#tb_product').find('tbody').find('tr');
  var temp_product = "";
  $.each(tb_product, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    if (!$(this).hasClass('deleted')) {
		 var data_id = $(this).attr('data_id');

		 let product = $(this).hasClass('children') ? temp_product : $(this).find('td:eq(0)').find('select').val();
		 let satuan = $(this).find('td:eq(1)').find('select').val();
		 let konversi = $(this).find('td:eq(1)').find('select').find(`option[value=${satuan}]`).attr('konversi');
     data.push({
      'id': data_id,
			'product': product,
			'konversi': konversi,
      'satuan': $(this).find('td:eq(1)').find('select').val(),
      'harga': $(this).find('td:eq(2)').find('input').val(),
      'jumlah': $(this).find('td:eq(3)').find('input').val(),
      'sub_total': $(this).find('td:eq(4)').find('label').text(),
     });

     temp_product = product;
    }
   }
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'vendor': $('#vendor').val(),
   'tanggal': $('#tanggal').val(),
   'keterangan': $('#keterangan').val(),
   'total': $('label#total').text(),
   'proc_item': Pengadaan.getPostProcItem()
  };

  return data;
 },

 simpan: function (id) {
  var data = Pengadaan.getPostData();
//  console.log(data);
//  return;
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Pengadaan.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Pengadaan.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Pengadaan.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Pengadaan.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#jumlah').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Pengadaan.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Pengadaan.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setSelect2: function () {
  $('select#vendor').select2();
 },

 setDate: function () {
  $('input#tanggal').datepicker({
   dateFormat: 'yy-mm-dd',
   todayHighlight: true,
  });
 },

 addItem: function (elm, e) {
  e.preventDefault();
  var tr = $(elm).closest('tbody').find('tr:last');

  var index = tr.index();
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    index: index
   },
   async: false,
   url: url.base_url(Pengadaan.module()) + "addItem",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    var tr = $(elm).closest('tbody').find('tr:last');
    var newTr = tr.clone();
    tr.html(resp);
    tr.addClass('item-' + index);
    tr.addClass('_data_item');
    tr.attr('urutan', index);
    tr.after(newTr);
   }
  });
 },

 getListSatuan: function (elm, product) {
  var tr = $(elm).closest('tr');

  $.ajax({
   type: 'POST',
   data: {
    product: product
   },
   dataType: 'html',
   async: false,
   url: url.base_url(Pengadaan.module()) + "getListSatuanData",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    tr.find('td:eq(1)').html(resp);
   }
  });
 },

 hitungSubTotal: function (elm, jenis) {
  var tr = $(elm).closest('tr');
  var harga = parseInt(tr.find('td:eq(2)').find('input').val());

  var jumlah = parseInt(tr.find('td:eq(3)').find('input').val());
  var sub_total_content = tr.find('td:eq(4)');
  var sub_total = (jumlah * harga);
  sub_total_content.find('label#sub_total').text(sub_total);


  if (jenis == 'product') {
   //get satuan
   var product = $(elm).val();
   Pengadaan.getListSatuan(elm, product);
  }
  Pengadaan.hitungTotal();
 },

 addSatuanContent: function (elm) {
  var tr_current = $(elm).closest('tr');
  var urutan = tr_current.attr('urutan');

  var tr_last = $(elm).closest('tbody').find('tr:last');
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    tr_index: tr_last.index(),
    urutan: urutan
   },
   async: false,
   url: url.base_url(Pengadaan.module()) + "addSatuanContent",
   error: function () {
    toastr.error("Gagal");
   },

   beforeSend: function () {

   },

   success: function (resp) {
    var newTr = tr_current.clone();
    newTr.html(resp);
    newTr.find('td:eq(1)').find('select').select2();
    newTr.addClass('children');
    tr_current.after(newTr);
   }
  });
 },

 removeSatuanContent: function (elm) {
  $(elm).closest('div.select_content').remove();
 },

 deleteItem: function (elm) {
  var data_id = $(elm).closest('tr').attr('data_id');
  if (data_id != '') {
   var tr = $('table#tb_product').find('tbody').find('tr[data_id="' + data_id + '"]');
   tr.addClass('hide');
   tr.addClass('deleted');
  } else {
   $(elm).closest('tr').remove();
  }

  Pengadaan.hitungTotal();
 },

 hitungTotal: function () {
  var tb_product = $('table#tb_product').find('tbody').find('tr');
  var total = 0;
  $.each(tb_product, function () {
   var td = $(this).find('td');
   if (td.length > 1) {
    if (!$(this).hasClass('deleted')) {
     var sub_total = $(this).find('td:eq(4)').text().toString();
//     sub_total = sub_total.replace(',', '');
     sub_total = sub_total.replace(/,/g, '');
     sub_total = parseInt(sub_total);
     total += sub_total;
    }
   }
  });

  $('label#total').text(total);
  $('label#total').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 cetak: function (id) {
  window.open(url.base_url(Pengadaan.module()) + "printFaktur/" + id);
 },
};

$(function () {
 Pengadaan.setDate();
 Pengadaan.setSelect2();
 Pengadaan.setThousandSparator();
});

var SendNotif = {
 module: function () {
  return 'send_notif';
 },

 add: function () {
  window.location.href = url.base_url(SendNotif.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(SendNotif.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(SendNotif.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(SendNotif.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'pesan': $('#pesan').val(),
   'email': $('#email').val(),
   'no_hp': $('#no_hp').val()
  };

  return data;
 },

 kirimNotif: function (id) {
  var data = SendNotif.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(SendNotif.module()) + "kirimNotif",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.success('Notif Berhasil Disimpan');
      toastr.error(resp.message);

      var reload = function () {
       window.location.reload();
      };

      setTimeout(reload(), 1000);
     }
     message.closeLoading();
    }
   });
  }
 },

 send: function (elm, id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   data: {
    email: $(elm).attr('email'),
    no_hp: $(elm).attr('no_hp')
   },
   async: false,
   url: url.base_url(SendNotif.module()) + "sendView/" + id,
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 }

};
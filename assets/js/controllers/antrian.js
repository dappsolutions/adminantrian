var Antrian = {
 module: function () {
  return 'antrian';
 },

 add: function () {
  window.location.href = url.base_url(Antrian.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Antrian.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Antrian.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Antrian.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'username': $('#username').val(),
   'password': $('#password').val(),
   'priveledge': $('#akses').val(),
   'pegawai': $('#pegawai').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = Antrian.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Antrian.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Antrian.module()) + "detail" + '/' + resp.pengguna;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Antrian.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Antrian.module()) + "detail/" + id;
 },

 checkAksesAktif: function (elm) {
  var akses = $(elm).val();
  if (akses != 1) {
   $.each($(elm).find('option'), function () {
    if ($(this).is(':selected')) {
     $(this).removeAttr('selected');
    }
   });

   $(elm).find('option:eq(0)').prop("selected");
   var msg = "<p>Silakan Upgrade Sistem Anda..</p>";
   msg += "<p>Mohon segera kontak <b><a href='http://www.support@greenholetech.com'>support@greenholetech.com</a></b></p>";
   bootbox.dialog({
    message: msg,
    size: 'large'
   });
  }
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Antrian.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Antrian.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },
 
 setSelect2: function(){
  $('select#pegawai').select2();
 }
};

$(function(){
 Antrian.setSelect2();
});

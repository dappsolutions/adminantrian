var Intervalinfo = {
 module: function () {
  return 'intervalinfo';
 },

 add: function () {
  window.location.href = url.base_url(Intervalinfo.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Intervalinfo.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Intervalinfo.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Intervalinfo.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'interval': $('#interval').val(),
  };

  return data;
 },

 simpan: function (id) {
  var data = Intervalinfo.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    url: url.base_url(Intervalinfo.module()) + "simpan",
    error: function () {
     toastr.error("Program Error");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Intervalinfo.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Intervalinfo.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Intervalinfo.module()) + "detail/" + id;
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   url: url.base_url(Intervalinfo.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Intervalinfo.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 setUserAktifNonAktif: function (id, is_active) {
  if(is_active == 1){
   is_active = 0;
  }else{
   is_active = 1;
  }
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Intervalinfo.module()) + "setUserAktifNonAktif" + '/' + id + '/' + is_active,
   error: function () {
    toastr.error("Program Error");
    message.closeLoading();
   },

   beforeSend: function () {
    message.loadingProses('Proses...');
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Diproses");
     var reload = function(){
      window.location.reload();
     };
     
     setTimeout(reload(), 1000);
    }else{
     toastr.error("Gagal Diproses");
    }
    
    message.closeLoading();
   }
  });
 }
};

var Produk = {
 module: function () {
  return "produk";
 },

 setDate: function () {
  $('#expired').datepicker({
   dateFormat: 'yy-mm-dd',
   changeMonth: true,
   changeYear: true
  });
 },

 add: function () {
  window.location.href = url.base_url(Produk.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Produk.module()) + "index";
 },

 addDetail: function (elm) {
  var tr = $(elm).closest('tbody').find('tr:last');
  var newTr = tr.clone();
  var no = parseInt($.trim(newTr.find('td:eq(0)').text()));
  var no = no + 1;
  newTr.find('td:eq(0)').html(no.toString());
  newTr.find('input').val("0");
  $(newTr.find('input')).divide({
   delimiter: '.',
   divideThousand: true
  });
  newTr.find('td:eq(3)').html(0);
  newTr.find('td:eq(4)').html('<i class="mdi mdi-minus mdi-24px" onclick="Produk.removeDetail(this)"></i>');
  tr.after(newTr);
 },

 removeDetail: function (elm) {
  $(elm).closest('tr').remove();
 },

 getPostDetailAnsuran: function () {
  var data = [];
  $.each($('#list_ansuran').find('tbody').find('tr'), function () {
   data.push({
    'id': $.trim($(this).attr('id')),
    'harga': $(this).find('#harga_angsuran').val(),
    'ansuran': $(this).find('#tenor').val(),
    'harga_total': $.trim($(this).find('td:eq(3)').text())
   });
  });

  return data;
 },

 getPostDetailImage: function () {
  var data = [];
  var text = $('input.text_image');
  $.each(text, function () {
   data.push({
    'image': $(this).val()
   });
  });

  return data;
 },

 getPostDetailImageRemove: function () {
  var data = [];
  var removeData = $('.image_remove');
  $.each(removeData, function () {
   data.push({
    'id': $(this).attr('id')
   });
  });

  return data;
 },

 getPostData: function () {
  var data = {
   'tipe_product': $('#tipe').val(),
   'kategori_product': $('#kategori').val(),
   'product': $('#product').val(),
   'keterangan': $('#keterangan').val(),
   'kode_product': $('#kode_product').val(),
//   'harga_cash': $('#harga_cash').val(),
//   'harga_kredit': $('#harga_kredit').val(),
//   'detail_ansuran': Produk.getPostDetailAnsuran(),
   'detail_image_remove': Produk.getPostDetailImageRemove(),
   'detail_image': Produk.getPostDetailImage()
  };

  return data;
 },

 simpan: function (id) {
  var data = Produk.getPostData();

  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Produk.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Produk.module()) + "detail" + '/' + resp.product;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Produk.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Produk.module()) + "index";
   }
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Produk.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Produk.module()) + "detail/" + id;
 },

 removeImage: function (elm) {
  $(elm).closest('.col-md-4').addClass('image_remove');
  $(elm).closest('.col-md-4').addClass('hide');
 },

 listFotoProduk: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Produk.module()) + "listFotoProduk/" + id,
   success: function (resp) {
    bootbox.dialog({
     message: resp,
     size: 'large'
    });
   }
  });
 },

 setThousandSparator: function () {
  $('#harga_cash').divide({
   delimiter: '.',
   divideThousand: true
  });
  $('#harga_kredit').divide({
   delimiter: '.',
   divideThousand: true
  });

  $('.harga_angsuran').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Produk.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Produk.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },

 getHargaTotal: function (elm, e) {
  var harga = $(elm).closest('tr').find('input#harga_angsuran').val();
  harga = isNaN(parseInt(harga)) ? 0 : harga;
  var data_ansuran = $(elm).closest('tr').find('select#tenor').find('option');
  var total_ansuran = 0;
  $.each(data_ansuran, function () {
   if ($(this).is(':selected')) {
    total_ansuran = $(this).attr('total_ansuran');
   }
  });

  var total_harga = harga * total_ansuran;
  console.log("total harga : "+total_harga);
  $(elm).closest('tr').find('td:eq(3)').html(total_harga);
  
  $(elm).closest('tr').find('td:eq(3)').divide({
   delimiter: '.',
   divideThousand: true
  });
 }
};


function uploadImage() {
 var button = $('.images .pic');
 var uploader = $('<input type="file" accept="image/*" />');
 var images = $('.images');
 button.on('click', function () {
  uploader.click();
 });

 uploader.on('change', function () {
  var reader = new FileReader();
  reader.onload = function (event) {
   images.prepend('<div class="img" style="background-image: url(\'' + event.target.result + '\');" rel="' + event.target.result + '"><span>remove</span></div>');
   images.prepend('<input type="hidden" class="text_image" value = "' + event.target.result + '"/>');
  };
  reader.readAsDataURL(uploader[0].files[0]);
 });

 images.on('click', '.img', function () {
  $(this).remove();
 });
}

$(function () {
 Produk.setDate();
 uploadImage();

 Produk.setThousandSparator();
});
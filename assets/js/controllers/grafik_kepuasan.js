var GrafikKepuasan = {
	module: function () {
		return "grafik_kepuasan";
	},

	setGrafik: function () {
		var bar = new Morris.Bar({
			element: 'bar-chart',
			resize: true,
			data: JSON.parse($('input#data_kepuasan').val()),
			barColors: ['#dd4b39', '#f39d30', '#26c0ef', '#49a65b'],
			xkey: 'y',
			ykeys: ['a', 'b', 'c', 'd'],
			ymin: 0,
			// stacked: true,
			labels: ['Tidak Puas', 'Kurang Puas', 'Puas', 'Sangat Puas'],
			hideHover: 'auto'
		});
	},

	changeMonth: function (elm) {
		var year = $("#tahun").val();
		var month = $("#month").val();
		window.location.href = url.base_url(GrafikKepuasan.module()) + "index?year=" + year + "&month=" + month;
	},

	changeYear: function (elm) {
		var year = $(elm).val();
		window.location.href = url.base_url(GrafikKepuasan.module()) + "index?year=" + year;
	},

	setDate: function () {
		$('input#tgl').datepicker({
			dateFormat: 'yy-mm-dd'
		});
	},

	setGrafikByDate: function (elm) {
		var tgl = $(elm).val();
		var year = $('#tahun').val();
		window.location.href = url.base_url(GrafikKepuasan.module()) + "index?year=" + year + "&tgl=" + tgl;
	}
};

$(function () {
	GrafikKepuasan.setGrafik();
	// GrafikKepuasan.setDate();
});

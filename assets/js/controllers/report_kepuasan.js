var ReportKepuasan = {
	setCollapse: function () {
		$('a.sidebar-toggle').trigger('click');
	},
	module: function () {
		return 'report_kepuasan';
	},
	inputAntrian: function (elm, e) {
		var no_antrian = $(elm).val().toString().toUpperCase();
		$(elm).val(no_antrian);
	},

	addToAntrian: function (elm) {
		var numb = $.trim($(elm).text());
		var inputAntrian = $('#input-antrian').val();
		var concatAntrian = inputAntrian + numb;
		$('#input-antrian').val(concatAntrian);
	},

	prosesNextAntrian: function (elm) {
		var current_antrian = $.trim($('#current-number').attr('data_id'));
		var params = {};
		params.current_antrian = current_antrian;
		$.ajax({
			type: 'POST',
			data: params,
			dataType: 'json',
			url: url.base_url(ReportKepuasan.module()) + "prosesNextAntrian",
			error: function () {
				toastr.error("Program Error");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Next Antrian...");
			},

			success: function (resp) {
				message.closeLoading();
				message.closeDialog();
				if (resp.is_valid) {
					toastr.success("Proses Berhasil");
					setTimeout(function(){
						window.location.reload();
					}, 2000);
				} else {
					toastr.error("Proses Gagal");
				}
			}
		});
	}
};


$(function () {
	ReportKepuasan.setCollapse();
});

var Satuan = {
 module: function () {
  return 'satuan';
 },

 add: function () {
  window.location.href = url.base_url(Satuan.module()) + "add";
 },

 back: function () {
  window.location.href = url.base_url(Satuan.module()) + "index";
 },

 search: function (elm, e) {
  if (e.keyCode == 13) {
   var keyWord = $(elm).val();
   if (keyWord != '') {
    window.location.href = url.base_url(Satuan.module()) + "search" + '/' + keyWord;
   } else {
    window.location.href = url.base_url(Satuan.module()) + "index";
   }
  }
 },

 getPostData: function () {
  var data = {
   'id': $('#id').val(),
   'product': $('#product').val(),
   'satuan': $('#satuan').val(),
   'harga': $('#harga').val(),
   'harga_beli': $('#harga_beli').val(),
   'ket_harga': $('#keterangan').val(),
   'qty': $('#qty').val(),
//   'level': $('#level').val()
  };

  return data;
 },

 simpan: function (id) {
  var data = Satuan.getPostData();
  var formData = new FormData();
  formData.append('data', JSON.stringify(data));
  formData.append("id", id);

  if (validation.run()) {
   $.ajax({
    type: 'POST',
    data: formData,
    dataType: 'json',
    processData: false,
    contentType: false,
    async: false,
    url: url.base_url(Satuan.module()) + "simpan",
    error: function () {
     toastr.error("Gagal");
     message.closeLoading();
    },

    beforeSend: function () {
     message.loadingProses("Proses Simpan...");
    },

    success: function (resp) {
     if (resp.is_valid) {
      toastr.success("Berhasil Disimpan");
      var reload = function () {
       window.location.href = url.base_url(Satuan.module()) + "detail" + '/' + resp.id;
      };

      setTimeout(reload(), 1000);
     } else {
      toastr.error("Gagal Disimpan");
     }
     message.closeLoading();
    }
   });
  }
 },

 ubah: function (id) {
  window.location.href = url.base_url(Satuan.module()) + "ubah/" + id;
 },

 detail: function (id) {
  window.location.href = url.base_url(Satuan.module()) + "detail/" + id;
 },

 setThousandSparator: function () {
  $('#harga').divide({
   delimiter: '.',
   divideThousand: true
  });
  $('#harga_beli').divide({
   delimiter: '.',
   divideThousand: true
  });
 },

 delete: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'json',
   async: false,
   url: url.base_url(Satuan.module()) + "delete/" + id,

   error: function () {
    toastr.error("Gagal Dihapus");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dihapus");
     var reload = function () {
      window.location.href = url.base_url(Satuan.module()) + "index";
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dihapus");
    }
   }
  });
 },
 
 execUbahHargaJual: function (elm) {
	 var product_satuan = $('#product_satuan').val();
	 var harga = $('#harga_jual_ubah').val();
  $.ajax({
   type: 'POST',
   dataType: 'json',
	 async: false,
	 data:{
		product_satuan: product_satuan,
		harga: harga
	 },
   url: url.base_url(Satuan.module()) + "execUbahHargaJual",

   error: function () {
    toastr.error("Gagal Dirubah");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dirubah");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dirubah");
    }
   }
  });
 },
 
 execUbahHargaJualGrosir: function (elm) {
	 var product_satuan = $('#product_satuan').val();
	 var harga = $('#harga_jual_ubah').val();
  $.ajax({
   type: 'POST',
   dataType: 'json',
	 async: false,
	 data:{
		product_satuan: product_satuan,
		harga: harga
	 },
   url: url.base_url(Satuan.module()) + "execUbahHargaJualGrosir",

   error: function () {
    toastr.error("Gagal Dirubah");
   },

   success: function (resp) {
    if (resp.is_valid) {
     toastr.success("Berhasil Dirubah");
     var reload = function () {
      window.location.reload();
     };

     setTimeout(reload(), 1000);
    } else {
     toastr.error("Gagal Dirubah");
    }
   }
  });
 },
 
 ubahHargaJual: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Satuan.module()) + "ubahHargaJual/" + id,

   error: function () {
    toastr.error("Gagal");
   },

   success: function (resp) {
    bootbox.dialog({
			message: resp
		});
   }
  });
 },
 
 ubahHargaGrosir: function (id) {
  $.ajax({
   type: 'POST',
   dataType: 'html',
   async: false,
   url: url.base_url(Satuan.module()) + "ubahHargaGrosir/" + id,

   error: function () {
    toastr.error("Gagal");
   },

   success: function (resp) {
    bootbox.dialog({
			message: resp
		});
   }
  });
 },
 
 setSelect2: function(){
  $("#product").select2();
  $('select#satuan').select2();
 }
};

$(function () {
 Satuan.setSelect2();
 Satuan.setThousandSparator();
});

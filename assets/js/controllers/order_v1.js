var Order = {
	module: function () {
		return 'order';
	},

	add: function () {
		window.location.href = url.base_url(Order.module()) + "add";
	},

	back: function () {
		window.location.href = url.base_url(Order.module()) + "index";
	},

	search: function (elm, e) {
		if (e.keyCode == 13) {
			var keyWord = $(elm).val();
			if (keyWord != '') {
				window.location.href = url.base_url(Order.module()) + "search" + '/' + keyWord;
			} else {
				window.location.href = url.base_url(Order.module()) + "index";
			}
		}
	},

	getProductItem: function () {
		var tr_data = $('table#tb_product').find('tbody').find('tr');
		var data = [];
		$.each(tr_data, function () {
			var td = $(this).find('td');
			if (td.length > 3) {
				var product_satuan = $(this).find('td:eq(0)').find('select').val();
				var product = $(this).find('td:eq(0)').find(`select`).find(`option[value='${product_satuan}']`).attr('produk_id');
				var stok = $(this).find('td:eq(0)').find(`select`).find(`option[value='${product_satuan}']`).attr('stok');
				var konversi = $(this).find('td:eq(0)').find(`select`).find(`option[value='${product_satuan}']`).attr('konversi');
				var qty = $(this).find('td:eq(1)').find('input').val();
				var sub_total = $.trim($(this).find('td:eq(3)').text());
				var id = $(this).attr('data_id');

				var bank = "";
				//    if (metode == 2) {
				//     bank = $(this).closest('tr').next().attr('data_bank');
				//    }

				data.push({
					'id': id,
					'product_satuan': product_satuan,
					'product': product,
					'stok': stok,
					'konversi': konversi,
					//     'pajak': pajak,
					//     'metode': metode,
					'qty': qty,
					'sub_total': sub_total,
					//     'bank': bank,
					'potongan_item': Order.getPostPotonganItem($(this)),
					'deleted': $(this).hasClass('deleted') ? '1' : '0'
				});
			}
		});

		return data;
	},

	getProductItemConfirm: function () {
		var tr_data = $('table#tb_product').find('tbody').find('tr');
		var data = [];
		$.each(tr_data, function () {
			var td = $(this).find('td');
			var product_satuan = $(this).attr('product_satuan');
			var qty = $(this).attr('qty');
			var konversi = $(this).attr('konversi');
			var product_id = $(this).attr('product_id');

			data.push({
				'id': id,
				'product_satuan': product_satuan,
				'qty': qty,
				'konversi': konversi,
				'product_id': product_id,
				'deleted': $(this).hasClass('deleted') ? '1' : '0'
			});
		});

		return data;
	},

	getPostPotonganItem: function (elm) {
		var index = $(elm).index();
		var class_pot = "potongan-" + index;

		var data = [];
		$.each($('tr.' + class_pot), function () {
			data.push({
				'potongan': $(this).find('td:eq(0)').attr('data_id'),
				'nilai': $(this).find('label#jenis').attr('nilai'),
			});
		});

		return data;
	},

	getPostData: function () {
		var data = {
			'id': $('#id').val(),
			'pembeli': $('#pembeli').val(),
			'metode_bayar': $('#metode_bayar').val(),			
			'no_rekening': $('#no_rekening').val(),
			'total': $('label#total').text(),
			'potongan': $('#potongan_invoice').val(),
			'pot_faktur': $('#nilai_potongan').val(),
			'product_item': Order.getProductItem()
		};

		return data;
	},

	simpan: function (id) {
		var data = Order.getPostData();
		var formData = new FormData();
		formData.append('data', JSON.stringify(data));
		formData.append("id", id);

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: formData,
				dataType: 'json',
				processData: false,
				contentType: false,
				async: false,
				url: url.base_url(Order.module()) + "simpan",
				error: function () {
					toastr.error("Gagal");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Disimpan");
						var reload = function () {
							window.location.href = url.base_url(Order.module()) + "detail" + '/' + resp.id;
						};

						setTimeout(reload(), 1000);
					} else {
						toastr.error(resp.message);
					}
					message.closeLoading();
				}
			});
		}
	},

	ubah: function (id) {
		window.location.href = url.base_url(Order.module()) + "ubah/" + id;
	},

	detail: function (id) {
		window.location.href = url.base_url(Order.module()) + "detail/" + id;
	},

	setThousandSparator: function () {
		$('#jumlah').divide({
			delimiter: '.',
			divideThousand: true
		});
	},

	delete: function (id) {
		$.ajax({
			type: 'POST',
			dataType: 'json',
			async: false,
			url: url.base_url(Order.module()) + "delete/" + id,

			error: function () {
				toastr.error("Gagal Dihapus");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dihapus");
					var reload = function () {
						window.location.href = url.base_url(Order.module()) + "index";
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dihapus");
				}
			}
		});
	},

	setSelect2: function () {
		$("#product").select2();
		$("#pembeli").select2();
		$("#metode").select2();
		$("#pajak").select2();

		var tr_product = $('table#tb_product').find('tbody').find('tr');
		$.each(tr_product, function () {
			var td = $(this).find('td');
			if (td.length > 1) {
				var data_id = $(this).attr('data_id');
				if (data_id != '') {
					var index = $(this).index();
					$(this).find('td:eq(0)').find('select').select2();
					$(this).find('td:eq(1)').find('select').select2();
					$(this).find('td:eq(2)').find('select').select2();
				}
			}
		});
	},

	setDate: function () {
		$('input#tanggal_faktur').datepicker({
			dateFormat: 'yy-mm-dd',
			todayHighlight: true,
		});
		$('input#tanggal_bayar').datepicker({
			dateFormat: 'yy-mm-dd',
			todayHighlight: true,
		});



	},

	addItem: function (elm, e) {
		e.preventDefault();
		var tr = $(elm).closest('tbody').find('tr:last');
		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: {
				index: tr.index()
			},
			async: false,
			url: url.base_url(Order.module()) + "addItem",
			error: function () {
				toastr.error("Gagal");
			},

			beforeSend: function () {

			},

			success: function (resp) {
				var tr = $(elm).closest('tbody').find('tr:last');
				var newTr = tr.clone();
				tr.html(resp);
				tr.after(newTr);
			}
		});
	},

	hitungSubTotal: function (elm, jenis) {
		var tr = $(elm).closest('tr');

		var class_potongan = "potongan-" + (tr.index());
		var option_product = tr.find('td:eq(0)').find('select').find('option');
		var harga = 0;
		// var stock = 0;
		$.each(option_product, function () {
			if ($(this).is(':selected')) {
				harga = $(this).attr('harga');
				stock = $(this).attr('stok');
			}
		});

		//  var option_pajak = tr.find('td:eq(1)').find('select').find('option');
		// var persentase = 0;
		//  $.each(option_pajak, function () {
		//   if ($(this).is(':selected')) {
		//    persentase = $(this).attr('persentase');
		//   }
		//  });

		var jumlah = parseInt(tr.find('td:eq(1)').find('input').val());
		// if (jumlah > stock) {
		//  toastr.error("Melebihi Stok");
		//  tr.find('td:eq(1)').find('input').val('0');
		//  return;
		// }
		var sub_total_content = tr.find('td:eq(3)');

		//  var potong_pajak = (jumlah * harga * persentase) / 100;
		//  var sub_total = (jumlah * harga) - potong_pajak;
		var sub_total = (jumlah * harga);

		var tr_potongan = $('tr.' + class_potongan);
		// console.log("kelas", tr_potongan);
		if (tr_potongan.length > 0) {
			$.each(tr_potongan, function () {
				var jenis = $(this).find('label#jenis').attr('jenis');
				if (jenis == "nominal") {
					sub_total -= parseInt($(this).find('label').attr('nilai'));
				} else {
					var nilai_pot = parseInt($(this).find('label').attr('nilai'));
					var nilai_percent = (nilai_pot * sub_total) / 100;
					sub_total -= nilai_percent;
				}
			});
		}
		console.log("sub total", sub_total);
		sub_total_content.find('label#sub_total').text(sub_total);

		Order.hitungTotal();
	},

	hitungTotal: function () {
		var tb_product = $('table#tb_product').find('tbody').find('tr');
		//  console.log(tb_product);
		var potongan = 0;
		var jenis_potongan = $('#potongan_invoice').val();
		var total = 0;
		$.each(tb_product, function () {
			var td = $(this).find('td');
			if (td.length > 3) {
				if (!$(this).hasClass('deleted')) {
					var sub_total = $(this).find('td:eq(3)').text().toString();
					sub_total = sub_total.replace(',', '');
					sub_total = parseInt(sub_total);
					total += sub_total;
				}
			}
		});

		if (jenis_potongan == "1") {
			var potongan_input = $('#nilai_potongan').val() == '' ? '0' : $('#nilai_potongan').val();
			potongan = parseInt(potongan_input);
			total -= potongan;
		} else {
			if (jenis_potongan == '2') {
				var nilai_pot = $('#nilai_potongan').val() == '' ? '0' : $('#nilai_potongan').val();

				nilai_pot = parseInt(nilai_pot);
				//    console.log(nilai_pot);
				var nilai_percent = (nilai_pot * total) / 100;
				//    console.log(nilai_percent);
				total -= nilai_percent;
			}
		}
		$('label#total').text(total);
		$('label#total').divide({
			delimiter: '.',
			divideThousand: true
		});
	},

	deleteItem: function (elm) {
		var data_id = $(elm).closest('tr').attr('data_id');
		if (data_id != '') {
			var tr = $('table#tb_product').find('tbody').find('tr[data_id="' + data_id + '"]');
			var metode = tr.find('td:eq(2)').find('select').val();
			tr.addClass('hide');
			tr.addClass('deleted');
			if (metode == 2) {
				tr.next().addClass('hide');
				tr.next().addClass('deleted');
			}
		} else {
			$(elm).closest('tr').remove();
		}

		Order.hitungTotal();
	},

	getMetodeBayar: function (elm) {
		var metode = $(elm).val();
		var index = $(elm).closest('tr').index();
		switch (metode) {
			case "2":
				$.ajax({
					type: 'POST',
					data: {
						metode: metode,
						index: index
					},
					dataType: 'html',
					async: false,
					url: url.base_url(Order.module()) + "getMetodeBayar",
					error: function () {
						toastr.error("Gagal");
					},

					beforeSend: function () {

					},

					success: function (resp) {
						bootbox.dialog({
							message: resp
						});
					}
				});
				break;

			default:

				break;
		}
	},

	pilihBank: function (elm) {
		message.closeDialog();
		var index_row = $(elm).attr('index_row');
		var tb_product = $('table#tb_product').find('tbody');
		var tr_product = tb_product.find('tr:eq(' + index_row + ')');

		var nama_bank = $(elm).closest('tr').find('td:eq(0)').text();
		var akun = $(elm).closest('tr').find('td:eq(2)').text();
		var no_rek = $(elm).closest('tr').find('td:eq(1)').text();
		var data_id_bank = $(elm).closest('tr').attr('data_id');
		var tr_bank = '<tr data_bank="' + data_id_bank + '">';
		tr_bank += '<td colspan="7">';
		tr_bank += '[' + nama_bank + '] - [' + akun + '] - [' + no_rek + ']';
		tr_bank += '</td>';
		tr_bank += '</tr>';
		tr_product.after(tr_bank);
	},

	cetak: function (id) {
		window.open(url.base_url(Order.module()) + "printFaktur/" + id);
	},

	bayar: function () {
		window.location.href = url.base_url("payment") + "add";
	},

	cancel: function (id) {
		$.ajax({
			type: 'POST',
			data: {
				order_id: id,
			},
			dataType: 'json',
			async: false,
			url: url.base_url(Order.module()) + "cancelOrder",
			error: function () {
				toastr.error("Gagal");
			},

			beforeSend: function () {

			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dibatalkan");
					var reload = function () {
						window.location.reload();
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dibatalkan");
				}
			}
		});
	},

	getDataPostConfirm: function () {
		var data = {
			'product_item': Order.getProductItemConfirm()
		};

		return data;
	},

	confirm: function (id) {
		$.ajax({
			type: 'POST',
			data: {
				order_id: id
			},
			dataType: 'html',
			// async: false,
			url: url.base_url(Order.module()) + "showKonfirmasiPembayaran",
			error: function () {
				toastr.error("Gagal");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Memuat Data...");
			},

			success: function (resp) {
				message.closeLoading();
				bootbox.dialog({
					message: resp
				});

				$('#bayar').focus();
			}
		});
	},

	execPembayaran: function (elm) {
		var total_harus_bayar = $('label#total').text();
		total_harus_bayar = total_harus_bayar.replace(',', '');
		total_harus_bayar = total_harus_bayar.replace(',', '');
		total_harus_bayar = total_harus_bayar.replace(',', '');
		total_harus_bayar = total_harus_bayar.replace(',', '');
		total_harus_bayar = total_harus_bayar.replace(',', '');
		var metode = $.trim($('#metode_bayar').text());

		var data = Order.getDataPostConfirm();
		var formData = new FormData();
		formData.append('data', JSON.stringify(data));
		formData.append('order_id', $('#order-id').val());
		formData.append('uang_bayar', $('#bayar').val());
		formData.append('total_tagihan', total_harus_bayar);

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: formData,
				dataType: 'json',
				processData: false,
				contentType: false,
				async: false,
				url: url.base_url(Order.module()) + "confirmBayar",
				error: function () {
					toastr.error("Gagal");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Konfirmasi...");
				},

				success: function (resp) {
					if (resp.is_valid) {
						toastr.success("Berhasil Konfirmasi");
						var hutang = 0;
						if (metode != 'KREDIT') {
							bootbox.dialog({
								message: `<div class="row"><div class="col-md-12"><h4>Uang Kembalian : ${resp.total_kembali}</h4></div></div>`
							});
						}else{
							hutang = parseInt(resp.total_kembali);
						}



						setTimeout(function () {
							window.location.href = url.base_url(Order.module()) + "detail" + '/' + resp.id;
						}, 10000);
						Order.validate($('#order-id').val(), hutang, $('#bayar').val());
						// var reload = function () {
						// 	window.location.href = url.base_url(Order.module()) + "detail" + '/' + resp.id;
						// };

						// setTimeout(reload(), 1000);
					} else {
						toastr.error("Gagal Konfirmasi");
					}
					message.closeLoading();
				}
			});
		}
	},

	validate: function (id, hutang, uang_bayar) {
		$.ajax({
			type: 'POST',
			data: {
				order_id: id,
				hutang: hutang,
				uang_bayar: uang_bayar
			},
			dataType: 'json',
			// async: false,
			url: url.base_url(Order.module()) + "validateBayar",
			error: function () {
				toastr.error("Gagal");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Validasi...");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Validasi");
					var reload = function () {
						window.open(url.base_url("faktur_pelanggan") + "detail" + '/' + resp.id);
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error(resp.message);
				}
				message.closeLoading();
			}
		});
	},

	getPotongan: function (elm, e) {
		e.preventDefault();
		var tr = $(elm).closest('tr');

		var index = tr.index();
		//  console.log(index);
		//  return;
		$.ajax({
			type: 'POST',
			dataType: 'html',
			data: {
				index: index
			},
			async: false,
			url: url.base_url(Order.module()) + "getPotongan",
			error: function () {
				toastr.error("Gagal");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Retrieve Potongan...");
			},

			success: function (resp) {
				message.closeLoading();

				bootbox.dialog({
					message: resp
				});
			}
		});
	},

	submitPot: function (elm, jenis) {
		var potongan = $('#potongan').val();
		var nilai_pot = $('#nilai_pot').val() == "" ? '0' : $('#nilai_pot').val();
		var table = $('#tb_product').find('tbody');
		var index_pot = $('input#index_pot').val();

		message.closeDialog();

		var tr = table.find('tr:eq(' + index_pot + ')');
		if (validation.run()) {
			if (potongan == 1) {
				//nominal          
				var newTr = tr.clone();
				newTr.addClass('potongan-' + index_pot);
				var td_html = "<td colspan='3' class='potongan' data_id='" + potongan + "'>";
				td_html += "<label id='jenis' jenis='nominal' nilai='" + nilai_pot + "'>Potongan Nominal : </label> " + nilai_pot;
				td_html += "</td>";
				td_html += "<td>";
				td_html += "</td>";
				td_html += "<td class='text-center'>";
				td_html += '<i class="mdi mdi-delete mdi-18px" onclick="Order.deleteItem(this)"></i>';
				td_html += "</td>";
				newTr.html(td_html);
				tr.after(newTr);
			} else {
				//persentase

				var newTr = tr.clone();
				newTr.addClass('potongan-' + index_pot);
				var td_html = "<td colspan='3' class='potongan' data_id='" + potongan + "'>";
				td_html += "<label id='jenis' jenis='persentase' nilai='" + nilai_pot + "'>Potongan Persentase (%) : </label> " + nilai_pot;
				td_html += "</td>";
				td_html += "<td>";
				td_html += "</td>";
				td_html += "<td class='text-center'>";
				td_html += '<i class="mdi mdi-delete mdi-18px" onclick="Order.deleteItem(this)"></i>';
				td_html += "</td>";
				newTr.html(td_html);
				tr.after(newTr);
			}

			Order.hitungSubTotal(tr, jenis);
		}
	},

	cancelItem: function (elm) {
		var order = $(elm).attr('order');
		var data_id = $(elm).attr('data_id');

		$.ajax({
			type: 'POST',
			data: {
				data_id: data_id,
				order: order
			},
			dataType: 'json',
			async: false,
			url: url.base_url(Order.module()) + "cancelItem",
			error: function () {
				toastr.error("Gagal");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Pembatalan...");
			},

			success: function (resp) {
				if (resp.is_valid) {
					toastr.success("Berhasil Dibatalkan");
					var reload = function () {
						window.location.reload();
					};

					setTimeout(reload(), 1000);
				} else {
					toastr.error("Gagal Dibatalkan");
				}
				message.closeLoading();
			}
		});
	},

	ubahQty: function (elm) {
		var product_item = $(elm).attr('data_id');
		var product_satuan = $(elm).attr('product_satuan');
		var qty = $(elm).attr('qty');
		var harga = $(elm).attr('harga');
		var order = $(elm).attr('order');
		var data_id = $(elm).attr('data_id');

		var html = "<div class='row'>";
		html += "<div class='col-md-12'>";
		html += "<input type='number' id='qty_ubah' max='" + qty + "' min='0' class='form-control required' error='Jumlah' placeholder='Jumlah'/>";
		html += "</div>";
		html += "</div>";
		html += "<br/>";
		html += "<div class='row'>";
		html += "<div class='col-md-12 text-right'>";
		html += "<button data_id='" + data_id + "' order='" + order + "' \n\
product_satuan='" + product_satuan + "' qty_old='" + qty + "' \n\
harga='" + harga + "' order_product='" + product_item + "' \n\
class='btn btn-primary' \n\
onclick='Order.execUbahQty(this)'>Proses</button>";
		html += "</div>";
		html += "</div>";

		bootbox.dialog({
			message: html,
		});
	},

	execUbahQty: function (elm) {
		var order_product = $(elm).attr('order_product');
		var qty_old = $(elm).attr('qty_old');
		var product_satuan = $(elm).attr('product_satuan');
		var harga = $(elm).attr('harga');
		var order = $(elm).attr('order');
		var data_id = $(elm).attr('data_id');
		var qty_ubah = $('input#qty_ubah').val();

		if (validation.run()) {
			if (parseInt(qty_ubah) > parseInt(qty_old)) {
				toastr.error("Melebihi Jumlah Lama");
			} else {
				var total_qty_back = qty_old - qty_ubah;
				var sub_total = qty_ubah * harga;
				$.ajax({
					type: 'POST',
					data: {
						order_product: order_product,
						qty_old: qty_old,
						product_satuan: product_satuan,
						harga: harga,
						qty_ubah: qty_ubah,
						total_qty_back: total_qty_back,
						sub_total: sub_total,
						order: order,
						data_id: data_id
					},
					dataType: 'json',
					async: false,
					url: url.base_url(Order.module()) + "execUbahQty",
					error: function () {
						toastr.error("Gagal");
						message.closeLoading();
					},

					beforeSend: function () {
						message.loadingProses("Proses Simpan...");
					},

					success: function (resp) {
						if (resp.is_valid) {
							toastr.success("Berhasil Disimpan");
							var reload = function () {
								window.location.reload();
							};

							setTimeout(reload(), 1000);
						} else {
							toastr.error(resp.message);
						}
						message.closeLoading();
					}
				});
			}
		}
	},

	checkUangPembayaran: function (elm, e) {
		var uang_bayar = isNaN(parseInt($(elm).val())) ? 0 : parseInt($(elm).val());
		var total_harus_bayar = $('label#total').text();
		var metode = $.trim($('#metode_bayar').text());
		total_harus_bayar = total_harus_bayar.replace(',', '');
		total_harus_bayar = total_harus_bayar.replace(',', '');
		total_harus_bayar = total_harus_bayar.replace(',', '');
		total_harus_bayar = total_harus_bayar.replace(',', '');
		total_harus_bayar = total_harus_bayar.replace(',', '');

		total_harus_bayar = isNaN(parseInt(total_harus_bayar)) ? 0 : parseInt(total_harus_bayar);
		console.log("uang_bayar", uang_bayar);
		console.log("total_harus_bayar", total_harus_bayar);

		//JIKA TIDAK HUTANG
		if (metode != 'KREDIT') {
			if (uang_bayar < total_harus_bayar) {
				toastr.error("Uang Bayar Tidak Boleh Kurang");
			} else {
				toastr.success("Uang Bayar Cukup");
			}
		}
	},

	setMetodeBayar: function(elm){		
		if($(elm).val() == '2'){
			$('div.content-rekening').removeClass('hide');
			$('#no_rekening').val('');
		}else{
			if(!$('div.content-rekening').hasClass('hide')){
				$('div.content-rekening').addClass('hide');
			}			
		}
	}
};

$(function () {
	Order.setDate();
	Order.setSelect2();
	Order.setThousandSparator();
});

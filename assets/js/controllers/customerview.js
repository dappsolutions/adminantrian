var CustomerView = {
	setCollapse: function () {
		$('a.sidebar-toggle').trigger('click');
	},

	module: function () {
		return "customerview";
	},

	getFormCustomer: function (elm) {
		var params = {};
		params.jenis = $(elm).attr('jenis');
		$.ajax({
			type: 'POST',
			data: params,
			dataType: 'html',
			url: url.base_url(CustomerView.module()) + "getFormCustomer",
			error: function () {
				toastr.error("Program Error");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Pengambilan Formulir...");
			},

			success: function (resp) {
				message.closeLoading();
				bootbox.dialog({
					message: resp
				});
			}
		});
	},
	
	getFormCustomerPenilaian: function (elm) {
		var params = {};
		params.jenis = $(elm).attr('jenis');
		$.ajax({
			type: 'POST',
			data: params,
			dataType: 'html',
			url: url.base_url(CustomerView.module()) + "getFormCustomerPenilaian",
			error: function () {
				toastr.error("Program Error");
				message.closeLoading();
			},

			beforeSend: function () {
				message.loadingProses("Proses Pengambilan Data...");
			},

			success: function (resp) {
				message.closeLoading();
				bootbox.dialog({
					message: resp
				});
			}
		});
	},

	cancel: function(){
		window.location.href = url.base_url(CustomerView.module())+"index";
	},
		
	getDataCustomer: function (elm, e) {
		$(elm).val($(elm).val().toUpperCase());
		if(e.keyCode == 13){
			var params = {};
			params.no_antrian = $(elm).val().toUpperCase();
	
			$.ajax({
				type: 'POST',
				data: params,
				dataType: 'json',
				url: url.base_url(CustomerView.module()) + "getDataCustomer",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},
	
				beforeSend: function () {
					message.loadingProses("Proses Pengambilan Data...");
				},
	
				success: function (resp) {
					message.closeLoading();
					$('#customer-id').val(resp.customer);
					$('#antrian-id').val(resp.id);
					$('#nama').val(resp.nama);
					$('#ktp').val(resp.no_ktp);
					$('#no_tlp').val(resp.no_tlp);
					$('#alamat').val(resp.alamat);
					$('#keperluan').val(resp.keperluan);
				}
			});
		}		
	},


	getPostInput: function (elm) {
		var data = {
			'nama': $('#nama').val(),
			'ktp': $('#ktp').val(),
			'no_tlp': $('#no_tlp').val(),
			'alamat': $('#alamat').val(),
			'keperluan': $('#keperluan').val(),
		};

		return data;
	},

	prosesSimpan: function (elm) {
		var params = {};
		params.data = CustomerView.getPostInput(elm);
		params.jenis = $('#jenis').val();

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: params,
				dataType: 'json',
				url: url.base_url(CustomerView.module()) + "prosesSimpan",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan Formulir...");
				},

				success: function (resp) {
					message.closeLoading();
					message.closeDialog();
					if (resp.is_valid) {
						toastr.success("Proses Simpan Berhasil");

						var link_redirect = url.base_url(CustomerView.module()) + `cetakFormulirAntrian?no_antrian=${resp.no_antrian}&jenis=${params.jenis}`;

						var w = screen.width - 300,
							h = 500;
						var left = (screen.width / 2) - (w / 2);
						var top = (screen.height / 2) - (h / 2);

						window.open(
							link_redirect,
							"_blank",
							"toolbar=no, scrollbars=yes, resizable=yes, top=" + top + ", left=" + left + "," +
							" width=" + w + ", height=" + h
						);
					} else {
						toastr.error("Proses Simpan Gagal");
					}
				}
			});
		}
	},
	
	getPostInputPertanyaan: function(){
		var data = [];
		var data_pertanyaan = $('div.data_pertanyaan');
		$.each(data_pertanyaan, function(){
			var params = {};
			var data_kategori = $(this).find('input.data_kategori');
			params.kategori = "";
			$.each(data_kategori, function(){
				if($(this).is(':checked')){
					params.kategori = $(this).attr('data_id');
				}
			});

			params.questions = $(this).attr('data_id');
			data.push(params);
		});

		return data;
	},

	prosesSimpanPenilaian: function (elm) {
		var params = {};
		params.customer = $('#customer-id').val();
		params.antrian = $('#antrian-id').val();
		params.kategori = $('#kategori').val();
		params.keterangan = $('#keterangan').val();
		params.questions = CustomerView.getPostInputPertanyaan();

		if (validation.run()) {
			$.ajax({
				type: 'POST',
				data: params,
				dataType: 'json',
				url: url.base_url(CustomerView.module()) + "prosesSimpanPenilaian",
				error: function () {
					toastr.error("Program Error");
					message.closeLoading();
				},

				beforeSend: function () {
					message.loadingProses("Proses Simpan Penilaian...");
				},

				success: function (resp) {
					message.closeLoading();
					message.closeDialog();
					if (resp.is_valid) {
						toastr.success("Proses Simpan Berhasil");
						setTimeout(function(){
							window.location.href = url.base_url(CustomerView.module())+"index";
						}, 3000);
					} else {
						toastr.error("Proses Simpan Gagal");
					}
				}
			});
		}
	},

	batal: function () {
		message.closeDialog();
	},

	gotoPagePenilaian: function(){
		window.location.href = url.base_url(CustomerView.module())+"penilaianview";
	}
};

$(function () {
	CustomerView.setCollapse();
});
